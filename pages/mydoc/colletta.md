---
title: "Colletta"
tags: [ adhesius, cola animal, cola calenta ]
summary: "És un material adhesiu històricament molt usat per teles, pintures i paper d'origen italià"
sidebar: mydoc_sidebar
permalink: colletta.html
---

## Recepta de Victori Vivancos

- 1 Kgr de cola de conill (o cola de peix)
- 1 litre d'agua
- 50 ml. fel de bou
- 1 litre de vinagre (el más claro)
-  0'50 gr. de fenol

### Preparació
«Se hidratan las dos colas por separado durante al menos 24 horas, por saturación en agua. Transcurrido este período de tiempo, eliminar el agua sobrante. Calentar hasta su disolución al baño María. Añadir el resto de componentes excepto el fenol, que se incorporará cuando todo esté bien mezclado. Verter en bandejas, con un máximo de dos dedos de grosor, para que evapore correctamente el agua y dejar enfriar. En estado gel, cortar en cuadraditos y dar vueltas, para que se separen y se sequen correctamente por todos los lados. Repetir el proceso durante varios días.»



Recepta. [Vivancos, Victoria. Restauradora. Catedrática de la Universitat Politécnica de València(http://victoriavivancos.blogspot.com/2009/02/preparacion-de-coletta-italiana.html)