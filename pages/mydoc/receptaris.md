---
title: "Receptaris"
tags: []
summary: "Històricament els receptaris han sigut la font de coneixement per elaborar productes abans de la revolució industrial. En aquesta secció s'esmenten uns pocs que són considerats referencials."
sidebar: mydoc_sidebar
permalink: receptaris.html
---


## El papir X de Leiden y el papir de Stockolm.
Són dos papirs grecs del segle III. Las millors introduccions als dos papirs son Caley 1926 y 1927.

[wiki Papir X ](https://es.qwe.wiki/wiki/Leyden_papyrus_X)

[receptes, Papir X. Anglès](https://en.wikisource.org/wiki/Leyden_Papyrus_X)

[Imatge del Papir X](https://upload.wikimedia.org/wikipedia/commons/4/4d/Egitto_ellenistico%2C_papiro_con_testo_magico_in_greco_%28papiro_X_di_leida%29%2C_da_tebe%2C_200-300_ac_ca.jpg)

[+ info p.Stockol](https://www.wdl.org/es/item/14299/)

[Papir Stockolm o Papyrus Graecus Holmiensis](https://www.wdl.org/es/item/14299/view/1/1/)


## El manuscrit de Lucca o ad tingenda musiva o Compositiones variae
Composicions per tenyir mosàis, pells i altres coses, daurat de ferro minerals, crisografia, preparació de coles i altres receptes

[Imatge](https://archive.org/details/aclassicaltechn00burngoog/mode/2up)

[txt llibre Thomson](https://archive.org/stream/aclassicaltechn00burngoog/aclassicaltechn00burngoog_djvu.txt)

## Mappae Clavicula y De coloribus et mixtionibus 
Heraclius, también escrito Eraclius. Descriu la manufactura de laques i pigments minerals, vidrieres, daurats, crisografia, pintura a l'oli, tremp, etc. i ofereis indicacions sobre mescles, vernissos, i tints (especialment per cuir) i artesania en altres materials com gemmes, marfil, cristall, vidre, ceràmica...

[+wiki](https://es.qwe.wiki/wiki/Mappae_clavicula)

## De Clarea
Escrit per un autor conegut com l'Anònim Bernense,
Tractat dels segles XI-XII de la clara d'ou. Hi ha un estudi per Thomson en Technical Studies in the Field of Fine Arts (1948), 1 (1&2), 8-19 & 70-81.

[+wiki](https://it.wikipedia.org/wiki/De_Clarea)

## Theophilus Protospatharius  
Estudis sobre el manuscrit
- Dodwell, C.R. Theophilus, de diversis artibus / The Various Arts, Medieval Texts. London & Edingurgh: Thomas Nelson and Sons, 1961, rep. Oxford 1986. Edición y traducción al inglés.
- Hawthorne, J.G. and Smith, C.S., Theophilus’ On Divers Arts, corrected edn., 1963, Univ. of Chicago Press, New York-Dover, 1979. Traducció anglesa amb notes i estudi.
- Brepohl, E., Theophilus Presbyter und das Mittelalterliche Kunsthandwerk, 2 vols., Köln, Weimar, Wien: Böhlau Verlag, 1999. Reimpresió de l'edició de Dodwel amb una traducció alemana y 154 làmines de reconstrucció de tècniques i 118 il·lustratives d'obras d'art.

[+ wiki](https://es.qwe.wiki/wiki/Theophilus_Protospatharius)

## Codex Matritensis
Manual del 1130 amb receptes de pigments, vernissos, tintas i adhesivos.  Reedició en J.M. Burman, Recipes from Codex Matritensis A.16 (ahora 19). University of Cincinnati Studies Series II, Vol. VIII, part. 1, 1912.

[J.M. Burman on line](https://babel.hathitrust.org/cgi/pt?id=hvd.32044072042179&view=1up&seq=3)


## Audemar, De coloribus faciendis, S. XIII-XIV
També conegut com Pere(o Pierre) de St. Omer, o Petrus de Sancto Audemaro. 
Conté receptes per fabricacion de pigments minerals, laques, tintes negres, adhesius, gomes, etc. La millor edició es la de Merrifield (1849).

## Liber de coloribus illuminatorum sive pictorum
La millor edició es la de Tompson del 1926. Receptes de preparació de pigments, tremp. Similar al de Teòfil.

## De arte illuminandi, o Còdex Napolità
Preparació de pigments, daurat, coles, pintura. Com referència veure Thompson y Hamilton (1933) y Brunello (1975).

## Els tres tractats de Archerius, Alcierius o Alcherius
Compilats en Itàlia cap al 1400 i recollits en la compilació de Le Begue. Pintura: preparació, mescla, fresc, oli, vernís, tremp, preparació de panells, pergamí o paper, dibuix amb punta metàl·lica, daurat, pintura de miniatures, pigments, or, i altres tècniques similars. Hi ha una traducció anglesa inglesa Merrifield 1849.

## Le Begue
Publicada per Sra. Merrifield (1849). Es una col·lecció realizada en 1413 sobre tècniques de il·luminació i altres tècniques d'altres fonts, dos glossaris sobre el color i terminologia tècnica, receptes sobre or, [crisografia](https://www.enciclopedia.cat/ec-gdlc-e00149261.xml), guix, laques, tintes, adhesius i formes d'esborrar llètres.

## Cenino Cennini, Il libro dell’arte
Juntament amb Teòfil és la font més indispensable. Redactat sobre el 1390 és un tractat molt ampli i sistemàtic sobre tècniques de dibuix, pigments, pinzells, pintures, treball sobre teles, mosàic, vidre, etc. La millor edició en anglès continua siguent la de Thompson, The Craftsman’s Handbook, 1933.

## Ambrogio, Ricepte d’Affare più Colori.
Tractat italià de Ambrosio di St. Pietro, de Siena, escrit sobre 1462. Es concentra en escriptura, incloent receptes per crisografia, pigments, tremp, tinta, preparació del pergamí, eliminació de taques de greix i escriptures antigues en el pergamí. El manuscrit va ser editat per Thompons en 1933.

### Fonts
[http://coloribusmediiaevi.blogspot.com/2009/04/principales-recetarios-publicados.html](http://coloribusmediiaevi.blogspot.com/2009/04/principales-recetarios-publicados.html)

[Blog](https://www.antoniosanchezbarriga.com/2008/10/)

