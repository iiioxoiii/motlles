---
title: "Pigment pur"
tags: [materials de restauracio, pigment, color]
summary: "És una substància colorant que pot ser d'origen natural o artificial, orgànic o inorgànic."
sidebar: mydoc_sidebar
permalink: pigment_pur.html
---

És una matèria innorgànica finament triturada i amb molt color.

## Pigments naturals i transformats

Els d'origen natural són els més antics perquè l'home els trobava a la natura en forma de petits fragments terrosos.
Són en general anomenats terres, ocres, sienes, terres ombra natural, terres verdes i cretes blanques.
Encara es fan servir i porten nom del seu origen. 

Més tard va aprendre a fabricar colors artificials per transformació. És pensa que les transformacions van
començar per l'atzar. Per exemple està documentat el cas del blanc de plom (carbonat bàsic de plom “cas
calvayaible”). Un incendi en un vaixell que transportava plom va veure que s'havia transformat en un “vermell saturno” o un havia adoptat un color ataronjat-vermell.

## Pigments sintètics

Són materials obtinguts per processos industrials, moltes vegades obtinguts dels [hidrocarburs](https://ca.wikipedia.org/wiki/Hidrocarbur) i molts són tòxics. Per exemple el [verd maragda](https://es.wikipedia.org/wiki/Verde_de_Par%C3%ADs).

### Orgànics naturals: naturals i sintètics

Els naturals són els tradicional coneguts com els colorants. Se'n coneixent milers. 

## Aplicació

Requerix un aglutinant per fer el fixat.

## Característiques

- Té cos propi. 
- És insoluble.
- És molt resistent a la llum i pràcticament no decolora.
- Els pigments purs decanten.

## Granulometria

Cada pigment té el seu gra idoni. Si es passen del punt perd propietats de color. 
Antigament els pigments (com que eren molts a mà) la granulometria no era homogènia. Avui si que ho són.

## Detalls dels pigments

- Els colors són més o menys resistents a la llum segons ho indica el fabricant. 
- La llum sempre efectes nocius al pigment. Es produeixen reaccions químiques com decoloracions o transparències.
- També poden alterar el color les capes de preparació sobre les quals es pinta.
- La calor, el fred, l'aire, els gasos, la humitat, etc, afecten la constitució molecular del pigment produint canvis de
tonalitat.
- També hi han pigments incompatibles per [l'aglutinant](/aglutinant.html) o incompatibles entre ells. Si hi han incompatibilitats estan indicades als pots. Per exemple: 
-- El [groc crom](https://ca.wikipedia.org/wiki/Groc_de_crom) verdeja amb l'oli.
-- El [blau prússia](https://ca.wikipedia.org/wiki/Blau_de_Pr%C3%BAssia) en contacte amb la [caseïna](https://ca.wikipedia.org/wiki/Case%C3%AFna) es torna inestable.
-- La pintura al fresc fa tornar el [blau prússia](https://ca.wikipedia.org/wiki/Blau_de_Pr%C3%BAssia) en marró fosc.

## Pigments per pintures al fresc
Hi ha pigments específics per pintures al fresc.

- Blancs: Blac de calç (crema de calç)
- Grocs
..* Ocres naturals
..* Terra siena natural
..* Ocre òxid de ferro
..*Groc de níquel-titani
- Blaus 
..* Blau de cobalt
..* Blau ultramar
- Vermells
..* Terres roges
..* Roig òxid de ferro
- Verds
..* Òxid de crom hidratat
..* Òxid de crom opac
..* Terres verdes
- Negres
..* Negre de magnesi
..* Negre d'òxid de ferro
..* Negre de vinyes

*Codina Esteve, Rosa.«Procediments Pictòrics,(Textos docents; 189. Text-guia)».edicions Universitat de Barcelona. ISBN 84-8338-189-3*

## Adulteració del pigments
- S'afegeixen càrregues o pigments inerts per motius econòmics ja que augmenta el pes. En restauració no es fan
servir pigments adulterats.
- Es fan servir com càrreges carbonat càlcic i tota mena d'argiles blanques. 
- Es poden afegir colorants sintètics per donar major intensitat de color.




[Falles i innovació](https://valenciaplaza.com/el-reinado-del-corcho-blanco-llega-a-su-finasi-se-fabricaran-las-fallas-del-manana)