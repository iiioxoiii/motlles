---
title: "Material per cremar"
keywords: 
tags: [falla, Àngela Grau González]
sidebar: mydoc_sidebar
permalink: material_per_cremar.html
summary: Projecte personal de l'Àngela Grau Gonález i extret del Treball final de Grau de Belles Arts en la Facultat de Belles Arts Sant Carles durant el curs 2017-2018 i titulat «Material para Quemar»
---

## Disseny

Primer es realitzen esbossos.

## Abans de començar el modelat

L'argila, composta d'òxid silici, òxid d'alumini, feldespats i d'altres impureses minerals és fa plàtica quant s'afegeix aigua. Hi han molts tipud d'argila però la més comú és la de fang vermell que conté òxid de ferro que és el que li confereix el color vermell.

Per fer el model es farà serviar argila per la seva plasticitat, la posibilitat de reciclat mitjançant la rehidratació i l'amassat i pel seu baix cost el fan el més idoni per modelar.

La bona argila es aquella que no queda enganxada a la mà però consistent per manipular-se. Per la seva consevació cal guardar-la en un recipient tancat per evitar l'assecat i conservar l'aigua.

Durant el modelat cal evitar que no s'assequi. Si s'asseca, la peça s'encongeix i s'esquerda. Per evitar-ho cal col·locar un drap humit, de qualsevol material absorvent y una bolsa per envolicar la peça. 

### Bastiment

A l'hora de planificar el modelat cal preveure fag perquè la seva plasticitat provoca també que el bloc sigui inestable. Per tant s'agafarà una peanya quadrada, d'algun taulell, al qual s'encolarà un llistò de fusta perpendicular 

## El modelat

En el modelat es fa servir:
- Un filferro per tallar el fang
- Palets de fusta de difernents formes i mides
- Buidadors
- Brotxa o pinzell
- Regle
- Espàtula
- Pulveritzador
- Un drap de cotò
- Un plàstic per evitar l'assecat de la peça

Es important no perdre de vista tots els angles de la figura per evitar irregularitats i sorpreses. Cal treballar en totes les perspectives sense centrar-se en el frontal i, per garantir-ho es treballarà sobre un trípode d'escultor, ajustant l'altura i girar la peça per poder observar tots els punts de vista durant el modelat.

## Motlle

Un cop acabat l'original en el fang es divideix el cap en dues parts: davant i darrera, separant-les amb una tira de «llandetes», cada dos, amb una altra doblegada a mode de pinça. A la part de les orelles es posa fang per evitar espifiar-la.

Per la preparació de l'escaiola agafarem una palangana amb poca quantitat d'aigua en el que anirem espolvorejant l'escaiola de forma homogènia per la superfície sense fer grumolls. Un cop l'aigua està completament saturada deixarem reposar l'escaiola uns moments abans de remoure amb una mà en una sola direcció per activar el fragüat. Hem de ser ràpids perquè aquest procès te un temps limitat d'actuació. I per afegir les capes prepararem encayola el número de vegades que sigui necessari fins donar-li la superfície consistència al motlle.

En aquest respecte deixarem a remull l'espart.

La primera lletada d'escaiola serà més líquida. 

En la segona ja es pot fer servir l'espart barrejat amb l'escaiola prèviament escorregut.

Finalment, en una tercera fase es cubreix amb guix dens.

### Impermeabilitzant

Abans de fer l'altra part del motlle s'aplica impermeabilitzat en la part de les orelles per evitar el contacte escaiola amb escaiola. Es farà servir una barbotina a partir de fang en cru, sec i pulveritzat, que rehumidificat en aigua ens servirà com desmotllant.

### Fraguat

Pot variar degut a les condicions climàtiques, però podrem assegurar-nos que està llest, tocant la superfície del motlle i comprovant que no hi ha calor. A les hores, amb falques de fusta obrirem el motlle, donant cops on estan les lladetes.

## Positivat en cartró

Els motlles que únicament hagin de ser positivats en cartrò, i com excepció a la norma, no cal fer claus de registre en el pla de la junta, ja que es positiven les dues meitats, amb el motlle obert i no hi ha perill de desplaçaments.

És farà servir engrut i el cartró falla, tant el vermell (o rosa) com el gris, que marca el canvi de gruix i de capa.

### Preparació del catró

- S'estripa el cartró a bocins amb les mans i no amb tisores per no deixar trams rectes al positiu.
- Es posa en aigua per estovar-lo.
- Després es «mata» el cartró (la «rabia»), tractant de obrir les fibres a cops fins aixafar-lo i fer-lo més dúctil. D'aquesta manera l'engrut penetra millor.

### L'engrut

Per la preparació de l'engrut es necessita:

- 100gr de farina.
- 600ml d'aigua.
- 5gr de sulfat de coure.

Es porten a ebullicio 500ml i es dissol la farina en l'aigua restant. Quant l'aigua està bullint es treu del foc i es mescla amb la mescla freda de la farina i el sulfat de coure per fer de fungicida.

Abans de començar s'escòrre el cartró i s'extent l'engrut a la part rugosa que és la que quedarà a l'interior del motlle. 

Es cubreix una primera capa de bocins petits, quan més petits millor, de cartró fi o gris, perquè s'adapta millor a la superfície i agafa el registre. Depenent de la consistència dessitjada s'aplica una altra capa de cartró gris encolada aquesta vegada pels dos costats o passarem a fer servir el vermell, de major gruix.

En procurarà que sogresurti pels costats del motlle. Posteriorment el cartó solbrant es doblegarà cap a dins per delimitar els contorns i fer-los forts. Una vegada secs els dos costats, es treuen del motlle i es puleixen les imperfeccions i superfícies de contacte, en la que es col·locarà cola de fuster per juntar les dues meitats.

Després es graparà tot el perímetre de la unió i es deixarà assecar durant 24h.

Quan està assecat es treuen les grapes per evitar l'oxidació i es reforça la unió amb tires de paper de diari i engrut més diluit. Aquesta acció s'anomena «tiretejar». 

Es deixa assecar 24 hores més.


## Retocs

És prepara la «pasteta» per remodelar els detalls i defectes.

### La pasteta

La «pasteta» serveix per tapar petites imperfeccions.

Per la preparació de la pasteta és necessita:

- Paper higiènic
- Cola blanca 
- Engrut
- Oli de Lli (unes gotes)

Es mescla paper higiènic mullat amb cola blanca, una mica d'engrut i unes gotes d'oli de lli.

Un cop feta la pasteta és prepara la primera capa de preparació 

### Primera capa

#### Ingredients
- 50% de gotelé
- 50% d'aigua
- i un raig de cola blanca.

En un recipient es mescla una proposció de gotelé, una proporció d'aigua i una mica de cola blanca. Es remou fins aconseguir una densitat d'un iogur líquit. Si es molt espessa afegit una mica d'aigua. S'aplicarà amb pinzell o brotxa. Es deixa assecar 24h.

### Segona capa
#### Indredients
- Panet i aigua en una proporció de 1/1.
- Gotelé.

Afegir en la mesla de panet aigualit el gotelé fins assolir la textura de iogur líquit. S'aplicarà amb pinzell o brotxa. Es deixa assecar 24h.

### Tercera capa
Igual que la segona però amb una proporció inferior de gotelé. La desnitat serà més densa com una papilla.
S'asseca més ràpid i cal anar més ràpid. Es deixa assecar 24h.

{% include note.html content="Es poden donar més capes si és necessari disminuïnt progressivament la proposció de gotelé" %}

## Pintura

Per pintar la peça es fa servir pintura plàstica.

### Tacar la peça
És el nom de la primera capa amb pinzell. Ha de quedar homogènia per després aplicar la pintura amb pistola.

Els retocs es fan amb llàpissos de colors.

## Vernís

Es pot realitzar un vernissat per tota la superfície amb acabat mate o brillant segons es vulgui.

## Cabell

Es realitza amb fibres d'espart decolorat amb llegiu i adherit amb pistola calenta. 


