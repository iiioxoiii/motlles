---
title: "Toxicitat Aguda"
tags: [riscos laborals, etiquetat]
summary: "Símbol d'advertència de gran perillositat per contacte, inhalació i/o ingesta"
sidebar: riscos_laborals_sidebar
permalink: perill_toxic.html
---

{% include precaution.html content="S'ha d'evitar tot contacte directe"%}


![Pictograma Toxicitat Aguda](https://i.pinimg.com/1200x/08/d9/84/08d984aec4c8ba5bfea3538028eecd1c.jpg)


## Efectes 

El simbol significa que el producte genera efectes nocius per la salut, amb conseqüències inmediates, encara que es tracti de petites dosis o en barreja amb altres substàncies. 

- Cancerígena.
- Afecta a la fertilitat.
- Causa mutacions.
- És un sensibilitzant respiratòri, pot provocar al·lègies, asma o dificultats respiratòries si és inhalat.
- Tòxic en determinats òrgans.
- Perill per aspiració, que pot ser mortal o molt nociu si s'ingereix o penetra per alguna via.

## Classificació

Els productes tòxics es classifiquen en 4 categories de perill diferent. A cada categoria els hi correspon un pictograma, una frase d’advertència i una indicació de perill concreta. 

{% include precaution.html content="Totes quatre categories determinen graus de toxicitat. El grau 4, significa que es menor que el 3 2 o el 1 però no significa que deixi de ser tòxic." %}

## Divisions «Toxicitat Aguda»

A cada divisió s’associa una frase H (d’indicació de perill) diferent. Les frases han de figurar en un dels idiomes oficials al país on es comercialitza la substància o barreja i que estigui publicat al reglament.

Divisió | Frase | Paraula
--- | --- | --- |
Toxicitat aguda (oral) Cat.1 Cat.2| H300: Mortal en caso de ingestión | Peligro
Toxicitat aguda (oral) Cat.1 Cat.2| H310: Mortal en caso de contacto con la piel | Peligro
Toxicitat aguda (oral) Cat.1 Cat.2| H330: Mortal en caso de inhalación | Peligro


## Indicacions

1. Les indicacions han d’aparèixer en un idioma oficial on es comercialitza el producte i ha de ser un idioma en el qual hagi estat redactat el Reglament.
2. A més a més, han de figurar les frases P (Consells de Prudència). Aquestes frases les ha de triar el fabricant, importador o formulador de la llista que proposa el Reglament per a cada classe de perill. En total, no han d’haver-hi més de 6 frases P per a cada substància o barreja.

A l’Annex IV del Reglament trobareu el significat d’aquestes frases, que també està recollida a la NTP 878 de l’INSHT.

Alguns consells de prudència (frases P) per als productes d’aquesta classe són:
- P262 Evitar el contacto con los ojos, la piel o la ropa.
- P264 Lavarse concienzudamente tras la manipulación.
- P270 No comer, beber ni fumar durante su utilización.

En alguns casos, el fabricant o proveïdor a d’acabar de completar aquestes frases indicant per exemple amb què és necessari rentar-se o donant alguna informació addicional.


## Napo i la Toxicitat Aguda

<div class="row">
  <iframe width="660" height="415" src="https://www.youtube-nocookie.com/embed/iuuqXq3ht2A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</div>

