---
title: "Argila"
tags: [materials plàstics]
summary: "L'argila, composta d'òxid silici, òxid d'alumini, feldespats i d'altres impureses minerals és fa plàtica quant s'afegeix aigua. Hi han molts tipud d'argila però la més comú és la de fang vermell, que ademés conté òxid de ferro que li dona el color vermell."
sidebar: mydoc_sidebar
permalink: argiles.html
---

Les partícules d'argila, inferiors als 4 micròmetres de diàmetre, poden conformar un sòl homogeni (també anomenat argila) o bé formar part d'un sòl heterogeni (i en aquest cas l'argila és la fracció més fina del sòl). Els sòls argilosos són altament impermeables. Les roques formades a partir de l'argila són les argilites.

[+Wikipedia](http://wwww.ca.wikipedia.org/wiki/Argila#Usos_de_l'argila_per_l'home)
