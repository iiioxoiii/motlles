---
title: "Lanolina"
tags: [materials de restauracio, cera, cera animal ]
summary: "És una cera obtinguda de les ovelles"
sidebar: mydoc_sidebar
permalink: lanolina.html
---

Lanolina (paraula derivada del llatí lāna=llana + oleum=oli) és una substància greixosa groguenca secretada per les glàndules sebàcies dels animals amb llana als quals els serveix per no quedar completament xops quan es mullen, encara que la majoria prové de la llana de l'ovella domèstica i és un subproducte de la neteja de la llana.Es fa servir . Algunes races d'ovella tenen tanta lanolina que s'obté fàcilment esprement la seva llana entre corrons.

## Composició

La lanolina és una cera i per tant una barreja [d'èsters](https://ca.wikipedia.org/wiki/%C3%88ster) i [àcids grassos](https://ca.wikipedia.org/wiki/%C3%80cid_gras) amb [alcohol](https://ca.wikipedia.org/wiki/Alcohol). 

- S'han identificat en la lanolina més de 180 àcids i 80 tipus d'alcohols. 
- El punt de fusió és a uns 36°C a 41°C.
- És insoluble en aigua però hi forma emulsions estables.
- Conté 1/3 part d'aigua.

## Presentació

En forma de crema o pomada. 

## Caracterítiques

- Un cop assecada és insoluble en aigua.
- És soluble amb dissolvents orgànics.
- Té el punt de fusió de 36 a 41o.
- Es fa servir en farmàcia i cosmètica i restauració (per tractaments de pells i s'havia fet servir com a capa
de protecció de metalls),com a cera hidròfuga, en productes netejadors del calçat ...