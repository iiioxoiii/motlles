---
title: "Etiquetado"
tags: [riscos laborals, etiquetat]
summary: "Normativa de etiquetado del Reglamento (CE) no1272/2008 del Parlamento Europeo y del Consejo, de 16 de diciembrede 2008, sobre clasificación, etiquetado y envasado de sustancias y mezclas, y por el que semodifican y derogan las Directivas 67/548/CEE y 1999/45/CE y se modifica el Reglamento(CE) nº1907/2006"
sidebar: riscos_laborals_sidebar
permalink: reglamento_etiquetado.html
---


## Dimensiones y confección de los elementos de la etiqueta

1. Los pictogramas de peligro establecidos en el Anexo V llevarán un símbolo negro sobre un fondo blanco, con un marco rojo lo suficientemente ancho para ser claramente visible.

2. Los pictogramas de peligro tendrán forma de cuadrado apoyado en un vértice. Cada pictograma deberá cubrir al menos una quinceava parte de la superficie de la etiqueta armonizada y la superficie mínima en ningún caso será menor de 1 cm2.

## Dimensiones de las etiquetas

Capacidad del envase | Dimensiones (en milímetros)
--- | --- | ---
Hasta 3 litros | Si es posible, al menos 52 × 74
Superior a 3 litros, pero sin exceder los 50 litros | Al menos 74 × 105
Superior a 50 litros, pero sin exceder los 500 litros | Al menos 105 × 148
Superior a 500 litros | Al menos 148 × 210

## Exenciones a los requisitos de etiquetado en casos particulares

De conformidad con el artículo 23, se aplicarán las siguientes exenciones:

### Botellas de gas transportables

Para las botellas de gas transportables, se permitirá recurrir a una de las siguientes alternativas siempre que sean botellas con una capacidad de agua menor o igual a 150 litros:

1. El formato y las dimensiones deben ajustarse a las prescripciones de la edición actual de la norma ISO 7225, relativa a «Botellas de gas — Etiquetas de precaución». En este caso, la etiqueta podrá llevar el nombre genérico o el nombre comercial o industrial de la sustancia o mezcla, siempre que las sustancias peligrosas que la compongan figuren de forma clara e indeleble en el cuerpo de la botella de gas.

2. La información especificada en el artículo 17 podrá incluirse en un disco duradero o en una etiqueta que atraiga la atención sobre la botella.

### Botellas de gas propano, butano o gas licuado de petróleo (GLP)

- Si el gas propano, butano o el gas licuado de petróleo, o una mezcla que contenga estas sustancias clasificadas de conformidad con los criterios del presente anexo, se comercializan en botellas recargables cerradas o en cartuchos no recargables a los que se aplica la norma EN 417 por tratarse de gases combustibles que sólo se liberan para la combustión (EN 417, edición actual, relativa a «Cartuchos metálicos para gases licuados de petróleo, no recargables, con o sin válvula, destinados a alimentar aparatos portátiles; construcción, inspección, ensayo y marcado»), estas botellas o cartuchos llevarán únicamente la etiqueta con el pictograma apropiado y las indicaciones de peligro y los consejos de prudencia correspondientes a la inflamabilidad.

- No es necesario incluir en la etiqueta información relativa a los efectos sobre la salud humana o el medio ambiente. En su lugar, el proveedor proporcionará la información a los distribuidores o a los usuarios intermedios mediante la ficha de datos de seguridad.

- Deberá transmitirse suficiente información al consumidor para que éste pueda tomar todas las medidas necesarias en relación con la salud y la seguridad.

### Aerosoles y recipientes con dispositivo nebulizador sellado que contienen sustancias o mezclas clasificadas como peligrosas por aspiración

Por lo que respecta a la aplicación de la sección 3.10.4, no es necesario etiquetar para este peligro las sustancias o mezclas clasificadas con arreglo a los criterios de las secciones 3.10.2 y 3.10.3 cuando se comercializan como aerosoles o en recipientes con dispositivo nebulizador sellado.

### Metales en forma masiva, aleaciones, mezclas que contienen polímeros y mezclas que contienen elastómeros

De conformidad con el presente anexo, **no es precisa** una etiqueta para metales en forma masiva, aleaciones, mezclas que contienen polímeros y mezclas que contienen elastómeros **si no presentan un peligro para la salud humana por inhalación, ingestión o contacto con la piel, ni para el medio ambiente acuático en la forma en que se comercializan**, aunque estén clasificadas como peligrosas con arreglo a los criterios del presente anexo.

{% include note.html content="En su lugar, el proveedor proporcionará la información a los distribuidores o a los usuarios intermedios mediante la ficha de datos de seguridad." %}

### Explosivos comercializados con objeto de producir un efecto explosivo o pirotécnico

Los explosivos, a los que se refiere la sección 2.1, comercializados con objeto de producir un efecto explosivo o un efecto pirotécnico se etiquetarán y envasarán únicamente de conformidad con los requisitos para explosivos.

## Solicitud de utilización de una denominación química alternativa

Las solicitudes de utilización de una denominación química alternativa, al amparo del artículo 24, sólo podrán concederse cuando:

1. la sustancia no tenga asignado un límite de exposición laboral comunitario, y

2. el fabricante, importador o usuario intermedio pueda demostrar que la utilización de la química alternativa cumple con la necesidad de aportar información suficiente para que puedan tomarse las debidas medidas relativas a la salud y a la seguridad laboral y con la necesidad de garantizar que puedan controlarse los riesgos derivados de la manipulación de la mezcla de que se trate; y

3. la sustancia esté clasificada exclusivamente en una o más de las categorías de peligro siguientes:

-- cualquiera de las categorías de peligro a que se refiere la parte 2 del presente anexo;
-- toxicidad aguda, categoría 4;
-- irritación o corrosión cutáneas, categoría 2;
-- lesiones oculares graves o irritación ocular, categoría 2;
-- toxicidad específica en determinados órganos — exposición única, categorías 2 o 3;
-- toxicidad específica en determinados órganos — exposiciones repetidas, categoría 2;
-- peligroso para el medio ambiente acuático –crónico, categorías 3 o 4.

### Elección de la denominación o las denominaciones químicas para las mezclas destinadas al sector de la perfumería

Si se trata de sustancias presentes en la naturaleza, se podrá utilizar una o varias nombres químicos del tipo «aceite esencial de …» o «extracto de …» en lugar de los denominaciones químicas de los componentes del aceite esencial o del extracto con arreglo al artículo 18, apartado 3, letra b).

## Exenciones a los requisitos de etiquetado y envasado

Cuando sea de aplicación el artículo 29, apartado 1, los elementos que deben figurar en la etiqueta según el artículo 17 podrán indicarse:

a) en etiquetas desplegables; o
b) en etiquetas colgadas; o
c) en un envase exterior.

{% include important.html content="En la etiqueta de todos los envases interiores figurarán, como mínimo, los pictogramas de peligro, el identificador de producto a que se refiere el artículo 18 y el nombre y el número de teléfono del proveedor de la sustancia o mezcla" %}

## Etiquetado de los envases cuyo contenido no excede de 125 ml

Las indicaciones de peligro y los consejos de prudencia correspondientes a las categorías de peligro que se enumeran seguidamente podrán excluirse de los elementos de la etiqueta requeridos conforme a lo dispuesto en el artículo 17 cuando **el contenido del envase no exceda de 125 ml, y la sustancia o mezcla esté clasificada en una o más de las categorías de peligro siguientes**

1. gases comburentes de categoría 1;
2. gases a presión;
3. líquidos inflamables de categorías 2 o 3;
4. slidos inflamables de categorías 1 o 2;
5. sustancias o mezclas que reaccionan espontáneamente, de los tipos C a F;
6. sustancias o mezclas que experimentan calentamiento espontáneo de categoría 2;
7. sustancias y mezclas que, en contacto con el agua, desprenden gases inflamables de categorías 1, 2 o 3;
8. líquidos comburentes de categorías 2 o 3;
9. sólidos comburentes de categorías 2 o 3;
10. peróxidos orgánicos de los tipos C a F;
11. toxicidad aguda de categoría 4, si la sustancia o mezcla no se suministra al público en general;
12. irritación cutánea de categoría 2;
13. irritación ocular de categoría 2;
14. toxicidad específica en determinados órganos — exposición única, de categorías 2 y 3, si la sustancia o mezcla no se suministra al público en general;
15. toxicidad específica en determinados órganos — exposiciones repetidas, de categoría 2, si la sustancia o mezcla no se suministra al público en general;
16. peligroso para el medio ambiente acuático — agudo de categoría 1;
17. peligroso para el medio ambiente acuático — crónico de categorías 1 o 2.

## Etiquetado aerosoles de menos de 125 ml

En especial a los generadores de aerosoles cuando el contenido del envase no exceda de 125 ml, y la sustancia o mezcla esté clasificada en una o más de las categorías de peligro siguientes:

1. gases inflamables de categoría 2;

2. toxicidad para la reproducción — efectos adversos sobre la lactancia o a través de ella.

3. peligroso para el medio ambiente acuático — crónico de categorías 3 o 4.

El pictograma, la indicación de peligro y el consejo de prudencia correspondientes a las categorías de peligro enumeradas a continuación podrán omitirse de los elementos que han de figurar en la etiqueta conforme a lo dispuesto en el artículo 17 cuando el contenido del envase no exceda de 125 ml, y la sustancia o mezcla esté clasificada en la catergoria de corrosivos para metales o más categorías.


## Etiquetado de envases solubles de un solo uso

Los elementos que han de figurar en la etiqueta conforme a lo dispuesto en el artículo 17 podrán omitirse de los envases solubles de un solo uso cuando:

1. el contenido de cada envase soluble no exceda de 25 ml;

2. el contenido del envase soluble esté clasificado exclusivamente en una o varias de las categorías de peligro indicadas en la sección 1.5.2.1.1.b) y

3. el envase soluble esté contenido dentro de un envase exterior que cumpla plenamente las disposiciones del artículo 17.
El apartado 1.5.2.2 no se aplicará a las sustancias o mezclas comprendidas en el ámbito de aplicación de las Directivas 91/414/CEE o 98/8/CE.

