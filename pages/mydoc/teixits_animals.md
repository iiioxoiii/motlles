---
title: "Teixits animals"
tags: []
summary: "Són aquelles fibres d'origen animal que poden ser transformades en filats i en conseqüència ser teixides, segons diversos sistemes de tramat o lligat."
sidebar: mydoc_sidebar
permalink: teixits_animals.html
---

Pel seu origen poden ser llanes i pels que s'obtenen de la pell de diferents mamífers. La seva composició es basa en la [queratina](https://ca.wikipedia.org/wiki/Ceratina) que és una varietat d'alguminoide compost per hidrògen, oxígen, nitrògen i sofre. Alguns exemples serien la llana d'ovella, pel de cabra del qual s'obté el [mohair](https://ca.wikipedia.org/wiki/Moher_(llana)) o la [caixmir](https://ca.wikipedia.org/wiki/Llana_caixmir), el pel de camell, pel d'alpaca, pel de cavall, pel humà. 

A part hi haurien les sedes que s'obtenen de secrecions de certs cuc en forma de llargs filaments continuus. Poden ser les sedes autèntiques o de morera, la seda silvestre que s'obté de diferentes papallones, la seda d'aranyes que són secrecions d'aranyes tropicals, la seda marina que s'obté d'alguns cetàcis. 

{% include note.html content="Per a la identificació: si es cremen fan flama i desprén olor de cabell cremant. Les cendres són fosques." %}
 
## La Llana

### Origen

- S’obté del borrissol de l'ovella. L'ovella és **ovis aries**.
- Les llanes de millor qualitat s’obtenen d’animals de 3 a 6 anys. 
- Una ovella produeix d’1 a 3 kg de llana fina o de 2 a 6 de llana gruixuda depèn de la raça.

### Història

Els primers teixits de llana pertanyen a l’edat de bronze i provenen dels països escandinaus. Els romans van ser els primers en criar sistemàticament ovelles seleccionades per produir llana.

### Composició

De constitució proteica a base de queratina, formada químicament per: 
Carboni 50%, Oxigen 25%, Nitrogen 15%, Hidrogen 6%, Sofre 4% aprox. 

{% include note.html content="De la llana s’obtenia tradicionalment la lanolina." %}

### Propietats

- Es atacada per alcalins i àcids. 
- Pot absorbir fins a un 40% del seu pes en aigua sense notar-se humitat al tacte.
- Com a teixit encongeix molt i li perjudica la calor. 
- Les fibres son elàstiques.

{% include note.html content="És la fibra més higroscòpica de totes." %}


### Aplicacions

- Teixits de vestuari de ritual, barrets.
- El feltre.

## La Seda

### Origen

S' obté del capoll de les erugues d’alguns lepidòpters que alguns lepidòpters es construeixen quan es transformen en crisàlides. És una secreció de les glàndules sericigenes dels cucs. En concret, es tracta de la femella [**bombyx mori**](https://ca.wikipedia.org/wiki/Cuc_de_seda) que és una papallona nocturna blanca. S’alimenta de les fulles de la morera blanca, viu de 3 a 4 dies i pon de 300 a 500 ous. 3500 cucs mengen 1000kg de fulles de morera per a produir de 2.5 a 3.5 kg de seda. Un cuc treballant sense parar pot produir un fil de 3 a 4 km de llarg. 

La industria de la seda es va iniciar cap a 2700 anys aC a la Xina. En el segle III dC el secret Xinès va ser divulgat a altres països com India, Japó i Persia. A Europa va ser introduïda a través de [Bizanci](https://ca.wikipedia.org/wiki/Bizanci) l’any 552 Després va passar a Grècia i països de dominació àrab.

### Composició

És una fibra proteica constituïda per [fibroïna](https://ca.wikipedia.org/wiki/Fibro%C3%AFna), [sericina](https://ca.wikipedia.org/wiki/Sericina) i altres proteïnes, cera, greix i substàncies colorants.

### Propietats

- És la fibra natural més elàstica i la més resistent
- Son filaments continus de gran finor i llargària
- Teixit noble molt sensible a la llum

### Aplicacions

- Suport de pintures xineses (de les que es pintaven amb tintes)
- També suport de pintures barrejat amb lli en època renaixentista (utilitzat per [Dürer](https://ca.wikipedia.org/wiki/Albrecht_D%C3%BCrer), [Mantegna](https://ca.wikipedia.org/wiki/Andrea_Mantegna)
- També suport per aquarel·les.
- Vestuari de ritual i ventalls, para-sols, etc.
