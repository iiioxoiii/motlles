---
title: "Perill d'Explosió"
tags: [riscos laborals, etiquetat]
summary: "Els explosius són substàncies i preparats sòlids, líquids, pastosos o gelatinosos que,fins i tot en absència d’oxigen atmosfèric, poden reaccionar de forma exotèrmica amb ràpida formació de gasos i que, en determinades condicions d’assaig, detonen, deflagren ràpidament o, sota l’efecte de la calor, en el cas de confinament parcial,exploten."
sidebar: riscos_laborals_sidebar
permalink: perill_explosio.html
---

{% include precaution.html content="S'ha d'evitar el calentament" %}

![Pictograma Explossió](http://dimasur.es/479-thickbox_default/etiquetas-pictogramas-explosivos.jpg)

Els explosius són substàncies o barreges sòlides o líquides que, de forma espontània, per reacció química, pot desprendre gasos a una temperatura, pressió i velocitat tals que poden ocasionar danys al seu entorn. 

En aquesta definició queden incloses les substàncies pirotècniques, encara que no desprenguin gasos.

## Divisions 

A cada divisió s’associa una frase H (d’indicació de perill) diferent. Les frases han de figurar en un dels idiomes oficials al país on es comercialitza lasubstància o barreja i que estigui publicat al reglament.

Divisió | Frase | Paraula
--- | --- | --- |
Divisió 1.1 | H201: Explosivo, peligro de explosión en masa | Peligro
Divisió 1.2 | H202:Explosivo, grave peligro de proyección | Peligro
Divisió 1.3 | H203:Explosivo, peligro de incendio, de onda expansiva ode proyección. | Peligro
Divisió 1.4 | H204:Peligro de incendio o de proyección. | Atención
Divisió 1.5 | H205:Peligro de explosión en masa en caso de incendio. | Peligro
Divisió 1.6 | Sense indicacions de perill. Sense paraula d’advertència


## Substancies explosives

Les substàncies auto-reactives i els peròxids en categories A i B

Tipus | Frase | Paraula 
--- | --- | --- |
Tipus A | H240 *(Peligro de explosión en caso de calentamiento)* | *Peligro* |
Tipus B | H241 *(Peligro de incendio o explosión en caso de calentamiento)* | *Peligro* | 
Tipus C i D |  H242 *(Peligro de incendio en caso de calentamiento)* | *Peligro* |
Tipus E i F | H242 *(Peligro de incendio en caso de calentamiento)* | *Atención* |
Tipus G | No té assignats elements a l’etiqueta. |  |

### Substancies i barreges que reaccionen esporàdicament

Les substàncies tèrmicament inestables, líquides o sòlides (auto-reactives), que poden experimentar una descomposició exotèrmica intensa fins i tot en absència d’oxigen (aire). En aquesta definició no s’inclouen els explosius, ni els comburents ni elsperòxids orgànics.

### Peròxids

Substàncies o barreges orgàniques sòlides o líquides que contenen l’estructura bivalent -O-O-, i poden considerar-se derivades del peròxid d’hidrogen (H2O2) enel qual un o tots dos àtoms d’hidrogen s’han substituït per radicals orgànics. Els peròxids orgànics són substàncies o barreges tèrmicament inestables, que poden patir una descomposició exotèrmica autoaccelerada. A més a més, poden teniruna o vàries de les propietats següents:

- Ser susceptibles d’experimentar una descomposició explosiva.
- Cremar ràpidament
- Ésser sensibles als xocs o als fregaments
- Reaccionar perillosament amb altras substàncies.


## Napo i el Perill d'Explosió

<div class="row text-center">
  <iframe width="660" height="415" src="https://www.youtube-nocookie.com/embed/iuuqXq3ht2A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</div>

