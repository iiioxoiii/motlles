---
title: "Plàstics"
tags: [plastics]
summary: "En la producció ha disminuït pesos, sorolls, lubrificacions, funcions aïllants, etc"
sidebar: mydoc_sidebar
permalink: plastics.html
---

Són un important número de materials que adquireixen la forma sòlida en el seu estat final, però a que determinat estat de la seva
fabricació, es transformen en un líquid d'alta viscositat, capaços de ser configurats, generalment sota temperatura i pressió, en
formes diverses.

- S'adaptem molt al procés de disseny de producte i al procés de fabricació.
- Es poden assolir aspectes visuals molt diversos.
- El manteniment del material és gairebé nul (els productes són més perdurables).
- Pes reduït.

## Propietats

### Lleugeresa

Tenen en general baixes densistats que oscilen entre el 0,8 gr/cm3(PMP) i el 2,2 gr/cm3 (PTFE). La gran majoria però està entre les densitats
compreses de 0,9-1,6 grm/cm3.

Això determina lleugeresa en les peces fabricades en plàstic i fa que sigui determinant en la fabricació d'elements en el quals el pes sigui
determinant.

Amb l'adicció d'agents espumants es poden aconseguir materials plàstics amb una alta porositat i densitats tant baixes com 0,01gr/cm3

(falta quadre comparatiu de densitats amb metalls)

### Flexibilitat

El plàstic és un material que poseeix un mòdul d'elasticitat baix, és a dir, **no és poden considerar flexibles**. En comparació a d'altres materials
es pot concloure que tenen una flexibilitat de 10 a 15 vegades inferior als dels metalls.

Aquesta poca flexibilitat es compensa amb dissenys de peces més acurats.

(falta quadre moduls d'elasticitat N/mm2)

### Transformacions

Solen tenir una temperatura de fusió baixa, entre els 15 i els 300ºC, el que significa constos baixos de fusió i injecció en motlles. La injecció
de plàstic és un procediment que es pot portar a terme molt ràpid. I cal recordar que un cop conformada la peça no cal afegir tractament posterior
per la conservació o per modificar l'estructura.

### Aïllants

Són altament aïllants tèrmics i d'electricitat. Els plàstics són 1000 vegades menys conductors de calor que els metalls corrents i els fan molt aptes
per aïllants tèrmics.

(falta quadre conductivitat tèrmica W/m x K)

Elèctricament poden ser 1000 trillons de vegades menys conductors que el metall menys conductor. Per això són el material més usat en electricitat
per l'aïllament elèctric.

(falta quadre aïllament electric W /ºC x m)

### Transparència

Permet usar-los per propòsits similars al vidre però a un cost inferior de producció: menys energia de producció, menys pes i menys fragilitat que el vidre.
El vidre però és més dur i menys rallable.

### Acabats

Les peces poden estar fetes un cop sortides del motlle i no calen tractaments preparatoris per adherir pigments. Es poden tenyir afegint
pigments en el seu estat líquid i tenir uns acabats sense manteniment.

### Estabilitat química

Tenen una corrossitat baixa no com els metàl·lics. Normalment són resistents als àcids i bases minerals i als ambients salins. Dissolen fàcilment amb dissolvents
orgànics que de manera controlada poden fer-se servir en usos industrials per lligar pintures.

### Permeabilitat als gasos

La majoria de plàstics de densitats mitjanes-altes actuen com barrera de líquids malgrat deixin passar algun gas. En els filtres es busca que els
plàstics siguin permeables o no permeables en el cas de plàstics per envasos.

### Reciclatge

Alguns plàstics poden ser reciclats totalment però altres no. La problemàtica augmenta quan els objectes de rebuig estan compostos per diferents tipus
de plàstics i no es poden serpar. Per això el reciclatge és difícil i s'acaba produïnt una eliminació del material, en el millor dels casos, per incineració.

### Contaminació

És molt poc contaminant en termes energètics i poc contaminant per no degradar-se. Però al no poder-se reciclar per una gestió de residus insuficient provoca
molts problemes medioambientals i afecta a tot l'ecosistema.

## Història

## Obtenció

S'obtenen de diverses matèries primeres. La més important és el **petroli**. De la fusta i del cotó s'obté la cel·lulosa. Alguns altres plàstics
també s'obtenen del carbó i del gas natural.

## Composició

Estan compostots per àtom de carboni (C) en combinació amb altres elements, fonamentalment hidrògen (H), oxígen (O), nitrògen (N), sofre (S) o clor (Cl).
S'estudien doncs des de la **química orgànica**.

Els materials  plàstics obtinguts no tenen, ni molt menys les característiques dessitjades d'utilització, igual com els metalls purs.
Per obtenir els materials finals cal afegir adhitius i passar per un tractament de preparació (granulació, granejat) que els prepara
pel fer-los servir posteriorment.

# Tipus

 Hi han uns 40 grups principals dels quals cadascun pot donar dotzenens de variants. 

## L'Etilé com mol·lècula del polímer

La matèria primera de tots els plàstics és **l'etilé**

## Classificació

- Els [termoplàstics](https://iiioxoiii.gitlab.io/motlles/termoplastics.html)
- [Materials termoestables](https://iiioxoiii.gitlab.io/motlles/termoplastics.html)

## Formes comercials

- Perfils comercials: Passamans, barres de secció quadrada, rodona, hexagonal i extrussions de perfils amb formes diverses.
- Tubs: Seccions quadrades, rodones, extruïts en formes diverses.
- Xapa: Llises, massises, panell d'abella o «sàndwich».
- Materials per extrussions: en presentació de petites boletes o filaments per ser fos.

