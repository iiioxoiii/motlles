---
title: "Teixits vegetals"
tags: []
summary: "Són aquelles fibres d'origen vegetals que poden ser transformades en filats i en conseqüència ser teixides, segons diversos sistemes de tramat o lligat."
sidebar: mydoc_sidebar
permalink: teixits_vegetals.html
---

## Composició 

Estan compostes principalment de cel·lulosa. També porten impureses amb diferentes proporcions com són resines, gomes, greixos, ceres, lignina i col·lorants. Aquestes s'han d'eliminar per obtenir fibres netes.

## Obtenció

Les fibres vegetals pel seu orígen poden ser obtingudes de llavors, com és el cas del cotò, obtingudes de tiges, com el cànnam, obtingudes de fulles com el sisal, obtingudes de fruits com el coco, i encara hi han d'altres com els fils de paper o cauxú també d'origen vegetal.

## Identificació 

A part del microscòpi, si es cremen fan molta flama amb molta llum i tenen olor característica del paper cremat, deixen poques cendes i són de color blanc.
 

## El cotó (de llavors)

És la fíbra més important de totes les fíbres naturals.

### Obtenció 

S'obté de les llavors de l'arbust anomenat gossypium de la família de les malviàcies. Existèixen moltes varietats amb diferents creuaments. 

És una planta de clíma tropical i subtropical, Necessita calor i humitat. El cotí creix en forma de pels damunt les llavors del fruit. Quant més llargs són aquest filaments de millor qualitat serà el cotò obtingut i els teixits. El cotó es cultiva a EEUU, India, Xina, Perú i Brazil. 

### Història 

Els primers teixits de cotó que coneixem provenen de l'India 3000 a.C. Després es va estendre a Egipte i a la Xina. Va arribar a Europa en el segle IX-X a través d'Espanya on fou introduït pels àrabs. AL 1768 fou inventada la màquina de filar i al 1787 la primer tel·ler mecànic. Al segle XIV arriba a Alemanya i després a Anglaterra. Al segle XIX el cotó fa fer baixar l'utilització del llí en pintures donat que era més econòmic.

### Composició 

91% de cel·lulosa, 7% d'aigua, 0,4% de cera i grasses i 0,5% de plasma. La resta són minerals.

### Propietats 

- És molt higroscòpic atacat pels àcids.
- No dissol en dissolvents orgànics.
- Encongeix amb l'aigua.
- Pateix biodeterioramet com quanlsevol material orgànic.

### Aplicacions 

- Ha sigut molt important com a matèria prima en la fabricació fet a mà i com a suport de moltes obres d'art.
- També com a suport de pintures al damunt tèxtil amb teixit de cotò.
- Molt emprat en l'elaboració de vestits de ritual i tapissos. El cotofluix hdròfil.

{% include note.html content="En el cas de les pintures caldria d'evitar el seu us com a suport. En tot cas per fer servir només en pintures petites." %} 

 
 
## El Lli (de tiges)

El teixit de lli s'obté de la planta [linum usitatissimum](https://ca.wikipedia.org/wiki/Lli) que és un planta hervàcea que creix de 40cm a 1m d'altura, d'arrels fibrosses i tija buida, d'uns 3 cm de diàmetre. Creix en climes temperats i fa flors blanques o blavoses. Dels residus de la seva elaboració s'obté [l'estopa](https://ca.wikipedia.org/wiki/Estopa_(material)) que és la part més basta i gruixuda que s'aprofita per fer cordes i cordills. Els principals productor són Egipte, EEUU, Austràlia. En la història,  els teixits de lli més antics daten de fa 7000 anys perqué es feien servir a Egipte per embolcallar les mòmies. Fins al sergle XVIII el lli era la fibra tèxtil més important a Europa. Fins al 1300 les manofactures egípcies foren les més importants. Després es difón a Europa. Al segle XIX el lli fou desplançat pel cotò.

### Composició

De 76% a 88% de cel.lulosa, però porta moltes impureses (del 15 al 20%). Un 3% és lignina i un 10% aigua.


### Propietats

- Molt higroscòpic. Capaç d'absorvir el 20% del seu pes en aigua sense tenir tacte humit. 
- Es atacat pel àcids. 
- La seva resistència a la rotura és quasi el doble a la del cotò.
- Té un tàcte fred i és un bon conductor de la calor.

### Aplicacions

- Materia prima en la fabricació del paper fet a mà. 
- Ideal per teixits de suport de pintures. Endrapats de taules. Vestits i drapatges de ritual. Estoballes.
- Existeixen teles mixtes la qual cosa s'ha fet per rebaixar el preu de la tela. 
- De les diferents varietats de lli també es pot obtenir l'oli de llinosa (veure [olis](/olis.html)). 
- L'estopa, com a subproducte del lli, s'ha emprat per a la realització de pintura sobre taula, per a reomplir el orificis dels nusos de la fusta, i per les escletxes dels diferents taulons que conformaven un retaule.
 
## El Cànem

S'obté de les tijes del cannabis sativa de clima atemperat. La planta té de 2 a 3 metres d'alçada i també té el subproducte que és l'estopa. 
 
### Història

Despres de la India se sap que 3000 a.C es feia servir a la Xina després de passar per la Ïndia i Rússia.

### Propietats

- Es molt resistent als estreps, més que el lli.
- També a la rotura i al desgast.
- La fibra és més tosca i gruixuda que el lli.
- Molt higroscòpica pot etenir un 30% del seu pes en aigua sense que es noti humida al tacte. 
- Té la desventatge que s'obtenen teixits més foscos que el del lli. 
- Tampoc és tant elàstic i es deforma més que el lli. 
- És més difícil de recuperar la forma.

### Composició

De 70% a 80% cel·lulosa,2-6% lignina, 4-8% pectina, 2-4% cutina, 0,5-1,5% ceres, 1,5-2,5% cendres.

### Aplicacions

- Va ser la fibra mes utilitzada a part del lli per a pintures de suport en tela fins a la meitat del segle XIX.
- Va ser utilitzada pels pintors Venecians com Tintoretto,...
- Amb un tramat especial anomenat espiguilla o espina de peix.
- També s’obté un oli per preparar sabons.
- Fins fa pocs anys les xarxes dels pescadors eren de cànem, ara són de nylon.
 
## El Jute 
 
Prové de la India i s’obté de dos tipus de plantes de la família de les tiliàcies CORCHORUS. Madeix de 2 a 3 metres d’alçada i es de clima tropical (humitat i calor). Hi ha la *capsularis* i *olitorus*.

### Història

Va ser utilitzat a la India per la fabricació de cordes, estores i teixits ordinaris com els sacs. Fou Introduït a Europa al segle XVIII i després passà a Amèrica. 

{% include note.html content="especte al lli i al cànem té menys cel·lulosa." %}

### Propietats

- Molt sensible als àcids.
- Pot absorbir un 24% d’humitat del seu pes sense que es noti humit.
- El teixit obtingut és dur i fosc.
- Els fils que s’obtenen son molt llargs.

### Aplicacions

- S’ha utilitzat com a suport de pintures de gran mida.
- S’ha fet servir en art contemporani.(ex. art pobre).
- També apareixen a arqueologia com a cordes i cordills.

## Linòleum

Suport per gravat que existeix des de fa uns 100 anys i està compost per un teixit de jute cobert per una pasta feta d’oli de llinosa i colofònia, serradures, càrregues minerals i pigments. També pot portar suro en pols.

[wiki](https://ca.wikipedia.org/wiki/Lin%C3%B2leum)

 
