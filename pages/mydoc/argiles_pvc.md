---
title: "Argiles Polimèriques"
tags: [materials plàstics]
sidebar: mydoc_sidebar
permalink: argiles_pvc.html
summary: "Les argiles polimèriques són materials termoplàstics sense aigua."

---

Són materials **termoplàstics** basats en la resina de [clorur de polivinil](https://ca.wikipedia.org/wiki/Clorur_de_polivinil) amb additius que regulen el grau de plasticitat.
