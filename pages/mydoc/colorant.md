---
title: "Colorant"
tags: [materials de restauracio, colorant, color]
summary: "És una substància tintòria que pot ser d'origen orgànic vegetal o animal."
sidebar: mydoc_sidebar
permalink: colorant.html
---

## Característiques
- No té cos propi.
- Normalment és soluble.
- No té resistència a la llum i decolora amb facilitat.

## Aplicacions

Serveix per tenyir minerals porosos (paper, teixits, etc.).