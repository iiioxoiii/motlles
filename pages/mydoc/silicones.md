---
title: "Silicones"
tags: [materials plàstics, polímers]
summary: És un polímer sintètic 
sidebar: mydoc_sidebar
permalink: silicones.html
---

El terme **silicona** és una denominació de [polímers sintètics](https://ca.wikipedia.org/wiki/Pol%C3%ADmer_sint%C3%A8tic).

Es considera un material inert cosa que fa que es faci servir en molts àmbits industrials i aplicacions mèdiques.

### Propietats

- Gran elasticitat conferida per l'enllaç Si-O-Si ( [Silici](https://es.wikipedia.org/wiki/Silicio) i [Oxigen](https://es.wikipedia.org/wiki/Ox%C3%ADgeno) ).
- Gran resistència a la temperatura ( de -80°C a + 250 °C).
- Escassa absorció de la humitat (higrofugants).
- Estabilitat dimensional.
- Antiadherència.
- Resistent a l'aigua calenta, detergents i altres substàncies agressives.
- No es fon.
- No s'oxida.
- Resistència als rajos ultraviolats, a l'ozó i als químics.
- Durabilitat enfront dels cicles de temperatura.

[+Wikipedia](https://ca.wikipedia.org/wiki/Silicona)

### Restauració

Consolidants per pedra, marbre com façanes, també per capes de protecció.

### Silicones de Platí

- Es poden fabricar en diferents formats, com gel, olis, etc.
- Hi han silicones catalitzades amb platí que cumpleixen amb [la normativa europea per productes que tenen contacte amb aliments](http://www.europarl.europa.eu/RegData/etudes/STUD/2016/581411/EPRS_STU(2016)581411_EN.pdf). 
- L'empresa d'articles de cuina Lekué es propietària de la patent des de 2001.
[+info](https://www.consumer.es/economia-domestica/servicios-y-hogar/silicona-platino-un-material-muy-recurrente-en-la-cocina.html)


