---
title: Cavant Castilene
tags: [cera, termoplàstic]
keywords: massilla, modelat, castilene, Chavant, FormFX, admonitions
last_updated: July 3, 2016
summary: "Massilla termopàstica per modelar."
sidebar: castilene
permalink: motlles_modelat_castilene.html
---

## Cavant Castilene

La cera Castilene de Chavant, se modela como la arcilla pero tiene la apariencia de cera. Castilene no necesita una armadura y pesa un 40% menos que las plastilinas.


## TRABAJAR CON CASTILENE

Para comenzar tiene que ablandar Castilene con una fuente de calor (lámpara de calor, cajas calientes, pistolas de aire caliente, ollas eléctricas, horno, microondas, (TENGA MUCHO CUIDADO EN NO SUPERAR EL CALOR CON EL MICROONDAS O PUEDE TENER PROBLEMAS CON BURBUJAS DE AIRE), agua caliente, o al baño maría) No utilice nunca una llama directa. La temperatura de fusión es de 170º.
Castilene permanecerá maleable mientras trabaja si se mantiene caliente con lámparas de calor. Todas las herramientas y los métodos que normalmente se utilizan para modelar en cera son compatibles con Castilene. Caliente sus herramientas antes de tallar. Enfríe Castilene para lograr la dureza óptima que Usted necesita para conseguir un pulido perfecto.

## Característiques

Es comercialitzen en tres tipus de duresa: dura, intermitja, tova

## Preu

13,5€/Kg


