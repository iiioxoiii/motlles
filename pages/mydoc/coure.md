---
title: "Coure"
tags: [metall, escultura, construcció]
summary: "Element químic de símbol Cu, del grup 11 de la taula periòdica, de número atòmic 29 i massa atòmica 63,546."
sidebar: mydoc_sidebar
permalink: coure.html
---

És un paterial ductil i manejable en fred i es llima de manera defectuosa. Les peces modelables són de baixa qualitat. És difícil de moldejar per retenir gran quantitat de gasos.

## Propietats

- Densitat 8,95gr/cm2
- Un punt de fusió de 1050 a 1085ºC
- Elevada conductivitat tèrmica
- Elevada conductivitat elèctrica
- Elevada resistència a la corrossió
- Facilitat de fabricació
- Resistència mitjana a la tensió
- Propietats de recuit controlables
- Bona soldabilitat.


## Aliatges

El Zinc (Zn) amb una proporció d'entre un 10 i 40% fa disminuir la temperatura de fusió i augmenta les característiques mecàniques (tracció, duresa, etc). A la vegada forma el component
base per la soldadura autògena heterogènia (soldadura de llautó).

### Llautons (Cu Zn)

Són metalls de color groc clar amb una densitat de 8,4 a 8,6 gr/cm3 i un punt de fusió d'uns 950ºC. Aquest aliatges estan formats per coure i entre un 5 i 50% de Zinc.

- Són mal·leables, dúctils i es poden polir fàcilment.
- Resistència a la corrossió
- Bona soldadura amb plom-estany (Pl-Sn)
- Aptitut a la deformació en fred (embutició, recalcado)
- Aptitut al conformat en calent

Els llautons són de resistència mitjana després del recuit i poden ser treballades en fred per augmentar la resistència.

### Bronze (Cu Sn)

Els aliatges de coure-estany formen metalls de color groc ataronjat amb una densitat aproximada de 8,6 gr/cm3. El seu punt de fusió està comprés entre els 900 i els 1000ºC.
Les proporcions d'estany en l'aliatge van entre un 2% i un 25%.

- No són mal·leables, ni dúctils
- Bona resistència a la corrossió
- Bona resistència al desgast
- Molt bona soldabilitat
- Bones característiques mecàniques
- Bona conductivitat però menor que la del coure

Hi han bronzes que també porten una petita quantitat de fòsfor. Aquests bronzes d'anomenen bronzes fosforosos que fa millorar la resistència mecànica i el desgast.

#### Usos del Bronze

En tota mena de maquinària maquinària que hagi de funcionar en ambients explosius,humits i que hagin de suportar fricció. No provoca guspires i no li afecta la humitat.

## Formes comercials

- Perfils comercials: Passamans, barres de secció quadrada, rodona, hexagonal i extrussions de perfils amb formes diverses.
- Tubs: Seccions quadrades, rodones, extruïts en formes diverses.
- Xapa: Llises, perforades o gravats.
- Accessoris: Moltes peces per tuberies. 

