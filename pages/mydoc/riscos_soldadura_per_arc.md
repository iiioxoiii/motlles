---
title: "Riscos Soldadura per Arc"
tags: [EPI]
summary: "Els principals problemes higiènics en la soldadura elèctrica per arc provenen dels contaminants que es generen i les radiacions ultraviolades produïdes en el procés principalment però també de gasos."
sidebar: mydoc_sidebar
permalink: riscos_soldadura_per_arc.html
---

Cada tipus de soldadura genera una sèrie de riscos higiènics que s'han de tenir en compte a l'hora de realitzar soldadures i d'escollir els equips de protecció.

## Radiació Ultraviolada

{% include precaution.html content="Un gran problema de les soldadures des del punt de vista higiènic són les radiacions ultraviolades, que es poden generar en els processos de soldadura per arc i que poden ocasionar cremades a la pell i problemes als ulls després de llargues exposicions." %}

## Gasos per Reaccions

A causa de la gran complexitat de components que recobreixen els elèctrodes ide les altes temperatures que s’originen, es forma una gran quantitat de reaccions molt complexes que donen lloc a multitud de compostos. La quantitat i composició dels fums i gasos que es generen, des del punt de vista higiènic, estan en funció del tipus de revestiment dels elèctrodes (àcids, bàsics, rútils, oxidants, etc.). 

La velocitat de generació d’aquests contaminats està en funció de **la tensió** i **intensitat de la soldadura**, **del diàmetre dels elèctrodes** i del **tipus de recobriment**. 

{% include note.html content="Els elèctrodes que menys fums produeixen són els de recobriments àcids i els que provoquen més fums són els de recobriments bàsics." %}

A més de la contaminació generada pels revestiments dels elèctrodes, existeix la contaminació deguda a la volatilització dels metalls que concorren en el procés. Si els metalls o aliatges que s’han de soldar estan recoberts de vernissos o altres productes, la calor pot modificar el producte cobrint, emetent vapors o fums d’efecte més elevat com més gran sigui l’augment de temperatura i més gran sigui la superfície afectada.

{% include precaution.html content="En treballs en espais confinats, una acumulació d’aquests vapors pot provocar greus problemes en els pulmons. Igualment es pot formar ozó, que en concentracions elevades provoca irritacions en ulls i pulmons." %}


### Control de contaminats i minimització de riscos

Per establir el **control** de contaminants i **minimitzar els riscos**, és a dir, per no sobrepassar els nivells d'exposició establerts en absorció de contaminants i exposició a les radiacions cal establir una avaluació. En soldadura seràn principalment **la ventilació per dilució** i **extracció localitzada**.

### Ventilació general per dilució

En general es farà servir la ventilació general per concentracions de tòxics i velocitats de generació baixes.

### Extracció localitzada

L'extracció localitzada capta els contaminants prop del focus (zona de emissió) i evita que es propaguin a l'ambient. En funció de la concentació serà necessari fixar un cabal d'aspiració mínim que garanteixi la captació de la major part dels contaminants generats.

- **Sistemes fixos**. S'empren quan el lloc de soldadura és fix, i està constituït per taules que duen incorporada una campana d'extracció, braços mòbils, etc.
- **Sistemes mòbils**. S'utilitzen qual cal desplaçar-se durant el treball fora del camp d'acció, per exemple quan es volen soldar peces de grandària gran i per això no és possible utilitzar taules fixes. En aquest cas es recorre a l'us de boques d'aspiració desplaçables.

{% include note.html content="La capacitat d'extracció disminueix en augmentar la distància entre la boca d'aspiració i el punt de soldadura. Es poden esmentar altres sistemes com l'extracció incorporada a la pistola de soldadura o la pantalla de soldador." %}

## Radiacions

{% include important.html content="Les **radiacions** generades en les operacions de soldadura poden ocasionar greus danys a les persones que s'hi veuen exposades." %}

- **Proteccions soldador.** Per protegir el soldador s'utilitzen generalment equips de protecció individual dels ulls.
- **Proteccions altres persones.** Per protegir a les persones que s'hi vulguin veure exposades de forma indirecta s'opta per aïllar o tancar el procés mitjançant **panells** per evitar-ne la propagació a prop. 

## Proteccions Individuals

| EPI | Norma | Característiques 
| --- | --- | --- | 
| Pantalla | UNE-EN 175 | A de tenir marcat 
| Filtres | UNE-EN 169 | 
| Cobrefiltres | UNE-166 | Es fan servir per prolongar la vida dels filtres.
| Guans | UNE-EN 12477 | Dades UNE-EN 420: el nom, la marca registrada o fabricant, denominació del guant, talla, data caducitat si les prestacions protectores poden veure's afectades significativament per l'envelliment.
| Màscarilles | UNE-EN 414: | Obligatòries en llocs confinats. CE Categoria III . Dades UNE-EN 414: nom, marca registrada, número UNE; any i mes de caducitat, la frase «vea instrucciones de utilización», tipus, clase i còdi de color. Filtres convinats A2 (gasos orgànics, marrò), B2 (gasos inorgànics,gris), P3 (partícules, blanc). 
| Mandrils, Polaines, Mànigues | UNE-EN 420 | Dades: nom, marca registrada, denominació, nom compercial, talla, normes aplicables, variació dimensional (si és superior al 3%), icones de rentat i manteniment, nº màxim de cicles de neteja.
| Roba |  | Cotò ignífug 

## La roba de treball

- Mànigues llargues.
- Punys ajustats als canells
- Collarí per protegir el coll.
- Pantalons sense plecs.
- Sense butxaques o que es puguin tancar.
- No es pot tenir tacada de greix o substàcies inflamables.
- Estar seca i no suada.

## Riscos afegits

També hi han associats a la soldadura altres riscos associats com **la presència d'electricitat**, **altes temperatures** i **projecció de partícules.**

{% include important.html content="Encara que en les operacions de soldadura elèctrica manual per arc s'utilitzen tensions baixes, la intensitat pot ser relativament perillosa segons les condicions especials de resitència del operari, la humitat de l'ambient, la suor de la roba, etc" %}

Els accidents afegits més comuns són:

- **Contacte elèctric directe** per deficiències en cables o connexions de les màquines o instal·lació.
- **Contacte elèctric indirecte** per derivacions de les carcasses.
- **Contacte elèctric directe** per circuit de soldadura quan està en buit a partir de 50V.

### Prevenció riscos elèctrics 

{% include important.html content="Mai es deixarà la pinça portaelectròde sobre la taula." %}

- Abans de començar a soldar comprovar la **toma de terra**.
- Els cables de soldadura han de tenir la secció adequada.
- Els cables llargs ha d'estar protegits contra projeccions incandescents, olis, greixos.
- Assegurar-se de la presa de terra esta correctament connectada.
- La zona de treball a d'estar seca.
- Quan es paralitzen els treballs cal desconnectar les màquines de la xarxa.
- En cas de que no se soldi es tallarà el suministrament elèctric de la màquina.

### Prevenció riscos explosió i incendis

Si hi han materials combustibles o inflamables pot haver-hi un perill d'incendi per les partícules projectades durant el procés de soldadura.

- Cal colocar màmpares envoltant els llocs de soldadura perquè detinguin les partícules incandescents que es produeixen durant les operacions de soldadura.
- Retirar el material inflamable o compustible a prop del lloc on s'hagi de soldar.
- En cas de soldar en canonades o recipients que hagin contingut productes inflamables cal netejar-los i comprovar amb un exposímetre, la concentració de gasos a l'ambient. No es podrà soldar fins tenir la certesa que no es despendran una altra vegada gasos que puguin arribar a concentracions explosives.

### Prevenció projecció de partícules

Cal preveure el despreniment de partícules incandescents o no en diferents direccions, velocitats i energies. Les partícules poden incidir tant sobre l'operari, persones que estiguin a prop o equips utilitzats. 



  


