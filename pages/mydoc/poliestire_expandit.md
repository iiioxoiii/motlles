---
title: "Poliestirè Expandit"
tags: [materials plàstics, polímers, escuma]
summary: "És el conegut com suro blanc, porexpan o poriexpan"
sidebar: mydoc_sidebar
permalink: poliestire_expandit.html
---

S'obté pel mateix procés de polimerització que el PS cristall però afegint-hi iso-pentà com a agent d'expansió. Un cop expandit és un material ideal per a un bon aïllament térmic, tant del fred com de la calor.

{% include note.html content="El poliestirè expandit es fa servir per la construcció de falles pel seu baix cost des dels anys 50." %}

## Propietats

El poliestirè expandit (EPS) és un material plàstic escumat, derivat del [poliestirè] i utilitzat en el sector de l'envasat i la construcció. La seva qualitat més destacada és la seva higiene, ja que no constitueix substrat nutritiu per microorganismes. És a dir, ni es podreix, ni es rovella ni es descompon, cosa que el converteix en un material idoni per a la venda de productes frescos. En els supermercats, es troba fàcilment en forma de safates a les seccions de peixateria, carnisseria, fruites i verdures.

Altres característiques destacables del poliestirè expandit (EPS) són la seva lleugeresa, resistència a la humitat i capacitat d'absorció dels impactes. Aquesta última peculiaritat el converteix en un excel·lent condicionador de productes fràgils o delicats com electrodomèstics, components elèctrics... També es fa servir per a la construcció de taules de surf, encara que normalment aquestes empren poliuretà, el poliestirè és més lleuger, cosa que comporta major flotabilitat i velocitat però menor flexibilitat.

Una altra de les aplicacions del poliestirè expandit és la d'aïllant tèrmic en el sector de la construcció, utilitzant-se com a tal en façanes, cobertes, sòls, etc. En aquest tipus d'aplicacions, el poliestirè expandit competeix amb l'escuma rígida de poliuretà, la qual té també propietats aïllants. A Espanya la Norma Bàsica de l'Edificació NBE-CT79 classifica en cinc grups diferents al poliestirè expandit, segons la densitat i conductivitat tèrmica que se'ls hagi atorgat en la seva fabricació. Aquests valors varien entre els 10 i 25 kg./m³ de densitat i els 0,06 i 0,03 W/m°C de conductivitat tèrmica, encara que només serveixen de referència, doncs depenent del fabricant aquests poden ser majors o menors.

[poliestirè]: (https://ca.wikipedia.org/wiki/Poliestir%C3%A8)
