---
title: "Termoestables"
tags: [materials, polímers]
summary: ""
sidebar: mydoc_sidebar
permalink: termoestables.html
---

## Propietats

Abans de la seva elaboració, no estan reticulats, normalment estan en estat líquit i s'endureixen a la seva forma definitiva pel l'escalfament
o addicció d'enduridors. A partir d'aleshores son especialment resistents a atacs químics i a l'escalfor. I a partir d'aquí ja no es poden
conformar plàsticament una altra vegada.

{% include important.html content="Un cop refredats ja no es poden reconformar." %}

Són més durs i fràgils que els termoplàstics.

{% include important.html content="No són soldables." %}

## Tipus de materials termoestables

-  [resines fenòliques](https://ca.wikipedia.org/wiki/Polietil%C3%A8)(PF)
-  [resines amíniques]()(??)
-  [resines de poliuretà](https://ca.wikipedia.org/wiki/Poliuret%C3%A0)(PUR)
-  [resites epoxi](https://ca.wikipedia.org/wiki/Ep%C3%B2xid)(EP)
-  [resines de poliéster insaturades]()(UP)

