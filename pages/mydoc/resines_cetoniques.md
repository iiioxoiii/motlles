---
title: "Resines cetòniques"
tags: [materials plàstics, polímers, resines]
summary: "Són materials termoestables, transparents"
sidebar: mydoc_sidebar
permalink: resines_cetoniques.html
---

Dissolen en toluè, xilè, WS i alguns altres dissolvents.Fan films transparents que quasi no engrogueixen i es fan servir sobretot en la fabricació de vernissos que han substituït als vernissos fets amb resines naturals. Son fàcilment reversibles i no atrauen la pols. Amb presència d’humitat no son òptimes.

