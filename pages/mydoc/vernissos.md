---
title: "Vernissos"
tags: [vernissos]
summary: "En general vernís és la protecció que es dona a les pintures en general damunt tela o fusta. S'anomena fixatiu quant és el cas d'un suport de document gràfic. Capes de protecció en objectes volumètrics en metalls, pedra, ceràmica..."
sidebar: mydoc_sidebar
permalink: vernissos.html
---
 

En general s'entén com vernís la capa de protecció que es dona a les pintures i objectes per protegir-los dels canvis climàtics, de la pols, dels fregaments i brutícies de tota mena. La capa de vernís crea una barrera que és la primera que rep l'impacte de qualsevol agressió de l'exterior. Per això és molt important envernissar, i sempre esperant el temps necessari que pot ser llarg abans d'aplicar la primera o segona capa.
 
 
## Eines d'aplicació
 
A pinzell, esprai o vaporitzador, a monyeca, i alguna vegada per submersió.
 
## Classificació  

Existeixen vernissos d'origen natural i d'origen sintètic. Poden actuar d'aglutinants com la tècnica de retoc al vernís i també poden actuar d'adhesius. Assequen per evaporació del dissolvent, per oxidació. Poden ser mats, sanitats o brillants. Els setinats és una mescla de brillant i mats, i els mats són vernissos brillants els quals se'ls afegeix cera. Aquesta normalment queda decantada. Per això cal escalfar-los i agitar-los.
 
### Característiques d'un bon vernís
 
1. Transparent
2. No ha d'engroguir ni ser trencadís
3. No ha dissoldre els colors
4. Cal vigilar la tècnica que cal envernissar
5. Te que assecar força ràpid (per no absorbir la pols)
6. No ha de canviar l'estètica de l'obra
7. A de ser reversible.
 
## Tipus de vernissos
 
### Vernissos a l'oli
Estan fets amb olis secants, normalment llinosa o també el de anus. Triguen molt es secar-se, per això antigament se'ls aplicava un secant com el blanc de plom i s'aplicaven a monyeca i en calent. Avui en dia no es fan servir ja que engrogueixen molt, enfosqueixen i es clivellen.
 
### Vernissos amb resines
Hi han vernissos amb resines toves dissoltes majorment en essència de trementina o espirituosa. El més emprat es el de Dammar que continua fent-se servir. El màstec s'havia emprat moltíssim. S'esbalaeix en presencia d'humitat i es clivella.
Aquest vernissos són reversibles durant uns anys. Després costen de retirar.
 
### Vernissos de resines dures
Tenen resines dissoltes en olis calents. Són massa durs, enfosqueixen moltíssim, són totalment irreversibles i es clivellen. S'ha d'evitar el seu us.
 
### Vernissos amb resines sintètiques
Són els vernissos més nous. Es fan servir des de fa uns 30 anys en l'art. Es basen en les resines poliacríliques majorment, polivinil-iques, o policicloexanones. En general dissoltes en dissolvents orgànics.
 
### Vernissos al bàlsam
Com la trementina de Venècia. Avui en dia no es fan servir. Enfosqueixen i clivellen. Rubens en feia servir aquest tipus de vernís.
 
### Vernissos a l'alcohol o etanol.
Són vernissos fets amb resines naturals però amb alcohol etílic. Que és el cas de la sandàraca, la goma laca i el màstec. El més antic és del segle XVI fet amb benjuí. També els fixatius van amb etanol. En general no són gaire bons:  Assequen molt ràpid, fan capes trencadisses (no són elàstics), es clivellen, es poden esbalair. En canvi es fan servir com impermeabilitzants o aïllants en llocs intermedis.
 
### Vernissos a la cera
Es present en forma de cremes o pastes i s'apliquen normalment a monyeca. La seva composició són ceres i dissolvents. El dissolvent acostuma ser trementina o white spirit i la cera acostuma a cer cera d'abella blanca mesclada amb cera carnauva. Després de penetrar la cera si ho polim podem obtenir brillantor. Normalment l'acabat és setinat. Són difícilment reversibles i atreuen la pols. S'han fet servir molt per metall i acabats amb fusta en mobles.
 
## Consideracions
 
La problemàtica que es pot presentar si la peça original no fos envernissada:
- Retirar el vernís anterior si cal tornar a envernissar .
- Cal que estigui molt malament el fet de treure el vernís .
- El vernís pot arribar a formar pàtina, per això cal respectar el vernís sempre que es pugui.
- Cal primer netejar la pintura per veure si el vernís està en mal estat.
- Cal vigilar els vernissos acolorits
 
L'envernissat és la última intervenció que fa el restaurador a una pintura o escultura policromada normalment abans s'haurà netejat la pintura. Cal vigilar que no quedin restes de pels de cotó fluix si no quedarien fixades per sempre més.
 
### Procediment

{% include important.html content="No es pot envernissar sobre envernissat" %}

#### Neteja 
- Cal eliminar la pols amb un plomall i al l'habitació de treball no ha d'haver-hi ni humitat ni presència de pols. L'objecte cal que estigui temperat (que a de portar hores en aquella habitació ja que un canvi brusc de temperatura pot provocar condensació (presencia d'humitat en formes de gotetes d'aigua sobre la peça).

- Sempre es millor donar capes primes diluïdes que no pas una de gruixuda. Si cales dues capes s'ha d'esperar sempre que la capa anterior estigui ven seca. La capa de vernís ha d'estar ven repartida i tenir el mateix gruix en totes les parts. 

{% include note.html content="Un objecte pot absorbir de diferent manera a diferents zones. Sovint s'ha d'insistir en algunes zones més que en altres." %}
 
- Un cop envernissat molt important observar a la llum rasant que no quedin pels de la paletina.

### Manera de fer-ho

{% include important.html content="És molt important que les capes pictòriques estiguin ben seques." %}

-  Amb la paletina agafem el vernís, escorrem la paletina i repartim des de el centre cap a totes les parts en direccions creuades. Cal tenir en compte que no es pot dos cops seguits començar pel mateix lloc per què després de passar la paletina pel pot la descàrrega és molt important quant toca a la superfície. 
- Si envernissem amb espai l'objecte cal que estigui en vertical.
- Cal comprovar amb la llum rasant que no quedin regalims.
 
{% include note.html content="En general les millors paletines són les de cua de cavall, poni o de teixó." %}
 
## Problemes que pot presentar el vernís

{% include note.html content="Per saber l'estat d'un vernís primer cal netejar la superfície d'un vernís."  %}

Amb els anys els vernissos s'oxiden i s'enfosqueixen, engrogueixen o es poden esbalair. Això fa que el dibuix, les formes i el color de la pintura es desdibuixin. Això canvia la lectura de l'obra. Amb el temps també els vernissos clivellen i provoca que la brutícia es vagi acumulant entre les separacions de la capa de vernís.També els vernissos poden patir bio-deteriorament o atacs biològics molt corrents com els excrements de mosca.
 
Els vernissos reben els fregaments, fums, vapors. 

 
## Retirar vernís

Per retirar un vernís és fa amb dissolvents. També es pot fer mecànicament en casos extrems amb polsims abrasius o amb làser. Antigament es retiraven mecànicament amb abrasius que gastaven la capa pictòrica
 
## Assecat 

Si després d'envernissar passen els dies i sembla que no ha assecat del tot pot ser per:

- El dissolvent o la trementina estigui envellida: Tendeixen a resinar-se amb el temps.
- S'ha fet servir aiguarràs.
- La pintura no esta prou seca.
- El dissolvent per retirar un vernís groguenc hagi re-estovat la pintura i la pintura no estigui seca.

Si al cap de 2 mesos no a assecat cal enretirar de nou la capa de vernís.
