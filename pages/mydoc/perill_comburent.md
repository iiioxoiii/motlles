---
title: "Comburent"
tags: [riscos laborals, etiquetat]
summary: "Es denomina comburent a la substància que participa en la combustió. El comburent oxida el combustible i aquest redueix el comburent. Així doncs el comburent és l'agent oxidant (es redueix) mentre que el combustible és el reductor (s'oxida)."
sidebar: riscos_laborals_sidebar
permalink: perill_comburent.html
---

{% include precaution.html content="S'ha d'evitar el calentament" %}

![Pictograma Comburent](https://pictogramasdeseguridad.com/wp-content/uploads/2013/07/Pictograma-Comburente-actualizado.gif)

# Divisions 

A cada divisió s’associa una frase H (d’indicació de perill) diferent. Les frases han de figurar en un dels idiomes oficials al país on es comercialitza la substància o barreja i que estigui publicat al reglament.

Divisió | Frase | Paraula
--- | --- | --- |
Gasos Comburents Cat.1 | H270:  Puede provocar o agravar un incendio | Perill
Líquids Comburents Cat.1 | H271: Puede provocar un incendio o una explosión, muy comburente | Perill
Sòlids Comburents Cat.1 | H271: Puede provocar un incendio o una explosión, muy comburente | Perill
Líquids Comburents Cat.2 | H272: Puede agravar un incendio | Perill
Sòlids Comburents Cat.2 | H272: Puede agravar un incendio | Perill
Líquids Comburents Cat.3 | H272: Puede agravar un incendio | Atención
Sòlids Comburents Cat.3 | H272: Puede agravar un incendio | Atención

