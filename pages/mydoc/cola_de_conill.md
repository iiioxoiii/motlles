---
title: "Cola de Conill"
tags: [ adhesius, cola animal, panet ]
summary: "És una cola calenta. Pot ser feta també de llebre i antigament era de gat. És la cola animal més emprada històricament i actualment també per la seva elasticitat."
sidebar: mydoc_sidebar
permalink: cola_de_conill.html
---
 
## Aplicacions

-Imprimació de suports, adhesiu de fusta o aglutinant de pigments.
- És l'aglutinant de la tècnica del **tremp a la cola** que és una tècnica aquosa de pintura. 
- Històricament s'ha fet servir per fer imprimacions (és la capa anterior a la de preparació i serveix perquè les capes de preparació s'agafin millor. Per suports de tela, fusta policromada, escultures, taules i retaules).
- També es fa servir per la preparació d'estucs i massilles. [veure Panet](/panet.html)
- També con adhesiu en relligats i fixació de capes pictòriques.
- Com totes les coles orgàniques, un cop hidratada acaba per descompondre's.
- En cremar-se fan olor de cabell o de plomes socarrimades.

## Preparació

### Recepta «Antoni Pedrola»

| Tècnica o material | Proporció |
| --- | --- |
| fusta | 150 g/l (~3 penques) |
| tela | 100 g/l (~2 penques) |
| tremp | 50 g/l (~1 penca) |

Aquestes proporcions són orientatives i depenen de la qualitat de les penques o l'època de l'any de la preparació. 

1. Posar les penques de cola en remull 24h fins que estiguin inflades i toves però mai han de perdre la forma.
2. Es porta a ebullició aigua i s'assegura l'esterilització
3. Es treu l'aigua del foc i s'adjunta la cola estovada remenant fins la seva dissolució.

{% include note.html content="No s'ha de deixar bullir la cola al foc perquè perd fortalessa"%}

Es pot mesurar la seva adherència humitejant-nos el palmell d'una la mà i en contacte amb l'altra.

La cola queda endurida quan es refreda.

En cas de tornar-la a fer servir cal escalfar-la al bany maria.

Cal conservarla en un un lloc fresc. 
 
### Additius

Es pot afegir algun desinfectant per conservar-la més temps:
- Essència de mirbà o nitrobenzè 1%
- Petaclorofenol 0.5 %

Pedrola, Antoni[«Materials, procediments i tècniques pictòriques»](https://books.google.es/books?id=DmFWzbjdrt0C&lpg=PA87&ots=5Ty7p5_0nf&dq=tremp%20a%20la%20cola&hl=ca&pg=PP1#v=onepage&q=tremp%20a%20la%20cola&f=false).Col·lecció Materials Docents. Uiversitat de Barcelona 