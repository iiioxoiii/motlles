---
title: "Ouricuri"
tags: [materials de restauracio, cera, cera vegetal ]
summary: "És una cera d'origen vegetal."
sidebar: mydoc_sidebar
permalink: cera_ouricuri.html
---

S'obté de diferents plantes que creixen al sud dels EEUU (Texas i nord de Mèxic). La planta s'anomena eufòrbia cerífera. 
Aquesta cera conté una part de resines de la pròpia planta i és de color gris-groguenc fins al marró. 

Es coneix des de principis de s. XX (1909).
- Té el punt de fusió de 66ºC fins a 77ºC. 
- Dissol en trementina, acetona i olis
 
 
