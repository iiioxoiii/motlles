---
title: "Compra de productes (FDS)"
tags: [riscos laborals, etiquetat]
summary: "Símbol d'advertència de gran perillositat per contacte, inhalació i/o ingesta"
sidebar: riscos_laborals_sidebar
permalink: compra_de_productes.html
---

{% include note.html content="Cada vegada que es compra un nou producte s’hauria de sol·licitar al fabricant la fitxa de dades de seguretat(FDS)." %} 


## Què és la FDS?

Coneguda popularment com a **fitxa de seguretat** , la FDS és un document d’obligat ús pera totes les substàncies i preparats químics que es comercialitzin en l’àmbit de la Comunitat Europea. S’ha de lliurar la primera vegada que se serveixi un productequímic perillós. 

Les FDS s’elaboren amb uns criteris que es poden resumir en els tres punts següents:

1. Han de proporcionar dades que permetin identificar **el producte químic** i **el fabricant o subministrador**, així com **un número de telèfon** en el qual ferconsultes d’emergència.
2. Han d’informar sobre els riscos i perills del producte respecte a:
- inflamabilitat
- estabilitat i reactivitat
- toxicitat
- possibles lesions per inhalació, ingestió o contacte dèrmic
- primers auxilis
- ecotoxicitat
3. Han de formar l’usuari del producte sobre:
- comportament i característiques
- correcta utilització (emmagatzamatge, manipulació, eliminació, etc.);
- controls d’exposició
- mitjans de protecció (individual o col·lectiva) en el cas d’una emer-gència
- actuacions que cal dur a terme en el cas d’un accident: extintorsadequats, control de vessaments, etc.

## Informació de la FDS

Conté un total de 16 punts:

1. Identificació del preparat i de la societat que el comercialitza.
2. Identificació de perills.
3. Composició i informació sobre components.
4. Primers auxilis.
5. Mesures de lluita contra incendis.
6. Mesures que cal prendre en el cas de vessament accidental.
7. Manipulació i emmagatzematge.
8. Controls d’exposició i protecció personal.
9. Propietats físiques i químiques.
10. Estabilitat i reactivitat.
11. Informació toxicològica.
12. Informacions ecològiques.
13. Consideracions sobre l’eliminació.
14. Informació relativa al transport.
15. Informació reglamentària.
16. Altres informacions.


## Exemples de FDS

- [FDS de l'Acetona feta per *Carl Roth GmbH*](https://www.carlroth.com/medias/SDB-7328-ES-ES.pdf?context=bWFzdGVyfHNlY3VyaXR5RGF0YXNoZWV0c3wyNzU1NzB8YXBwbGljYXRpb24vcGRmfHNlY3VyaXR5RGF0YXNoZWV0cy9oNjgvaDdjLzg5NTA5NzQxMTk5NjYucGRmfGJiODU0N2E4ZDI0NDY0MGQ3MmM0MjZiMWM2ZWFhNDg2ZWM5OGE2OWJmOTdlZjUyODkzNTJlNDU0NTA4YTYxNTQ) 