---
title: "Diluent"
tags: [materials de restauracio, diluent]
summary: "És una substància líquida que serveix per rebaixar, disminuir, o aprimar una concentració per facilitar la seva
aplicació. L'aigua pot actuar com a dissolvent o diluent i molts altres productes."
sidebar: mydoc_sidebar
permalink: diluent.html
---

## Exemples

La trementina dissol resina però pot aprimar una concentració diluint-la.


