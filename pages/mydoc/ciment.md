---
title: "Ciment"
tags: [ciment]
summary: "És un dels materials de construcció més importants que hi ha, si bé gairebé mai s'empra sol. El ciment (del llatí caementum) és un conglomerant hidràulic artificial de naturalesa inorgànica (conglomerant hidràulic vol dir que endureix dins l'aigua)."
sidebar: mydoc_sidebar
permalink: ciment.html
---  

## Història

El primer ciment patentat ha donat nom al material com Portland.
[+Wiki](https://ca.wikipedia.org/wiki/Ciment#Hist%C3%B2ria)

## Barreja del ciment

Barrejat amb aigua i àrids forma el formigó. Si els àrids de la barreja són molt fins (sorra), hom anomena la barreja morter.

Per analogia, en geologia s'anomena ciment al material d'unió d'algunes roques sedimentàries, principalment conglomerats. Aquest material sol ser [carbonat de calci](https://ca.wikipedia.org/wiki/Carbonat_de_calci).




