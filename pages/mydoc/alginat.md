---
title: "Alginat"
tags: [materials plàstics]
toc: true
summary: "Són polisacàrids naturals. Les varietats comercials d'alginat s'extreuen d'alga, incloent-hi l'alga gegant Macrocystis pyrifera, Ascophyllum nodosum i diversos tipus de Laminaria. També és produït per dos gèneres bacterians Pseudomonas i Azotobacter, que tenen un paper essencial en el desentrellar la seva ruta biosintètica."
sidebar: mydoc_sidebar
permalink: alginat.html
---

Els alginats són [polisacàrids](https://en.wikipedia.org/wiki/Alginic_acid) (com el [midó](https://ca.wikipedia.org/wiki/Mid%C3%B3), el [glicogen](https://ca.wikipedia.org/wiki/Glicogen), la [quitina](https://ca.wikipedia.org/wiki/Quitina), etc) obtinguts a partir [algues marines brunes](https://ca.wikipedia.org/wiki/Alga_bruna), de naturalesa polielectrolítica i amb comportament [col·loïdal](https://ca.wikipedia.org/wiki/Col%C2%B7loide) que els fa aptes per a poder espesseir, gelificar, emulsionar, estabilitzar, formar pel·lícules protectores (films). Les algues contenen grans quantitats d'alginat. Aquestes sals corresponen a polímers orgànics derivats de l'[àcid algínic](https://www.enciclopedia.cat/ec-gec-0076694.xml).


