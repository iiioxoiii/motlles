---
title: "Introducció als equips de protecció individual (EPI)"
tags: [riscos laborals]
summary: "Els EPIS són els «Equips de Protecció Individual»"
sidebar: riscos_laborals_sidebar
permalink: epis.html
---

## Criteris d'utilització

El criteri universal d’utilització dels EPI (Equips de Protecció Individual) és que s’hauran d’implantar quan els riscos no es puguin evitar o no es puguin limitar prou per mitjans tècnics de protecció col·lectiva o mitjançant mesures, mètodes o procediments d’organització del treball. Per tant, la utilització dels EPI és una mesura excepcional a la qual tan sòls es recorrerà quan s’hagin esgotat totes les vies alternatives que s’han d’intentar implementar prioritàriament amb l’objectiu d’eliminar el risc. 

## Quan utilitzar-los?

- Quan les mesures de prevenció i protecció col·lectiva o organitzatives aplicades **siguin insuficients**.
- Quan les mesures de prevenció i protecció col·lectiva o organitzatives **siguin tècnicament inviables**.
- Quan les mesures de prevenció i protecció col·lectiva que siguin oportunes no es puguin implantar immediatament i **s’hagi de recórrer provisionalment aa questes mesures.**
- Quan s’hagin de fer operacions puntuals en una situació esporàdica que nojustifiqui la implantació de mesures permanents.•Es produeixin situacions d’emergència, rescat o autosalvament. En aquest cas, s’han d’utilitzar sempre.

En els supòsits excepcionals que s’hagi d’utilitzar els EPI com a darrera mesura per eliminar les conseqüències de la situació de risc o disminuir-ne la gravetat, la Llei de prevenció de riscos en l’article [29 i l’RD 773/1997](https://www.boe.es/buscar/act.php?id=BOE-A-1997-12735), en l’article 10, obliga els treballadors a utilitzar correctament els mitjans i equips de protecció facilitats per l’empresariat, d’acord amb les instruccions que n’hagi rebut.

## Selecció dels EPIS

La selecció s'ha de desenvolupar d’acord amb la seqüència que es presenta en la taula:

Seqüència d’actuació | Paràmetres de decisió
--- | ---
Determinació del tipus d’equip que s’ha d’utilitzar | Vies d’entrada del contaminant
Fixació de les característiques tècniques de l’equip que s’ha d’utilitzar | Nivell de risc i informació del fabricant
Adquisició de l’equip | Adequació a l’entorn laboral 

### Determinació de l’equip que s’ha d’utilitzar

El tipus d’equip de protecció és determinat per la via d’entrada del contaminant al cos

Via d’entrada | Equip que cal utilitzar
--- | --- 
Inhalació | Equip de protecció de les vies respiratòries
Dèrmica | Segons l’estat del contaminant i la part del cosexposada
Contacte ocular | Ulleres de protecció


Si en el desenvolupament de l’activitat laboral poden coexistir diverses vies d’entrada, caldrà utilitzar diversos equips simultàniament o bé la utilització d’unequip multirisc

Taula d'exemple de protecció simultània a diversos riscos:

Vies d’entrada | Solucions possibles
--- | ---
Inhalació i contacte ocular | Combinació d’equips o Mascareta de protecció i ulleresde protecció o Màscara de protecció

{% include note.html content="Un dels punts clau és formar, informar i motivar el treballador per tal que en l’exercici diari de la seva feina utilitzi els EPI. Aquesta tasca de motivació recauen el tècnic de prevenció encarregat de fer la formació a l’empresa." %}


## Característiques tècniques dels EPIS

Una vegada decidit l’equip o combinació dels equips que s’han d’utilitzar, cal determinar quines seran les seves característiques tècniques perquè respongui amb efectivitat als nivells de risc avaluats al lloc de treball.L’element bàsic de referència per al desenvolupament d’aquesta tasca és **la informació facilitada pels fabricants** dels equips en els fulletons informatius, i enprincipi es preseleccionen aquells equips que ofereixin uns nivells de rendiment adequats al nivell de risc. 

A continuació, s’indiquen els diferents paràmetres tècnics que s’han de considerar per als diferents tipus d’equips

## EPIS de les vies respiratòries

Cal definir el tipus d’equip que s’ha d’utilitzar. Hi ha diferents possibilitats segons les condicions ambientals i l'oxigen ambiental. Amb els valors màxims d'exposició (VLA) es pot determinar el grau de protecció necessari pel EPI.

{% include note.html content="Per triar el EPI cal saber els valors de concentració a l'ambient i el valor màxim d'exposició. Si amb el valor de protecció nominal (FPN) del EPI el càlcul surt més alt que el valor màxim d'exposició, el EPI no és adequat" %}

#### Tipus d'equip (Oxigen>17% a l'ambient i concentració inferior a 100 vegades el VLA-ED o 10.000 ppm)

- Equips filtrants
- Mascaretes autofiltrants
- Filtre més adaptador facial (màscara o mascareta).

#### Tipus d'equip (Oxigen<17% a l'ambient i concentració inferior a 100 vegades el VLA-ED o 10.000 ppm)

- Equips aïllants
- Semiautònoms
- Autònoms

## Guants de protecció

El nivell de protecció del guant depèn del producte químic específic, i el fabricant
ha de fer referència en el full informatiu a quin és el grau de protecció del guant
davant els productes químics pels quals ha estat dissenyat.Pictograma que porten els guantspels riscos químics.

Normalment, el fabricant utilitza un pictograma que indica que el guant és adequatper a la seva utilització davant el risc químic, acompanyat d’una sèrie d’índexs queindiquen les característiques de rendiment del guant.

### Informació que ha d’incloure el fulletó 
- **Marcatge CE**.
- **Nom, marca registrada** o un altre mitjà d’identificació del fabricant orepresentant autoritzat.
- **Denominació del guant**: Nom comercial o codi que permeti que l’usuariidentifiqui el producte amb la gamma del fabricant o el seu representantautoritzat.
- **Talla**.
- **Data de caducitat**: Si les prestacions protectores poden ser afectades significativament per l’envelliment.
- **Pictograma** adequat al risc cobert pel guant: Eisc mecànic, per fred, per impacte, per calor i foc, per electricitat, per biològic, etc.
- **Paràmetre relatiu a la penetració**. La penetració indica la capacitat del pro-
ducte químic per passar a través dels forats, costures o altres imperfeccions del material.
- **Permeació**. Indica la capacitat del producte químic per travessar el guanta escala molecular.  Realment és el paràmetre que defineix el grau de protecció del guant.

Taula de permeació:

Temps de transpiració | Índex de protecció
--- | --- 
>10 minuts | 1 
>30 minuts | 2 
>60 minuts | 3
>120 minuts | 4
>240 minuts | 5
>480 minuts | 6


## Roba de protecció

{include note.html content="L’elecció d’un tipus de peça de roba o d’un altre depèn de la part de cos exposada i de la forma de presentació del contaminant." %}

La protecció davant riscos químics presenta la particularitat que els materials amb què estan fets els vestits són específics per al compost químic davant el qual esbusca protecció. El nivell de protecció es defineix per dos criteris: index de protecció i index de protecció

Roba de protecció davant el risc químic

### Índex de protecció dels materials

Es definit per cada parella, formada pel material de la roba/producte químic.El nivell de protecció es defineix mitjançant una escala amb sis índexs de protecció: des de l’1, que significa la protecció mínima, fins al 6, que significa la protecció màxima. Aquests “índexs de protecció” es determinen en funció d’un paràmetre d’assaig denominat “temps de pas”, el qual indica el temps que el producte químic triga a travessar el material.

### Índex de protecció dels vestits
A part del material de què està fet el vestit de protecció, s’estableix una classificació dels mateixos vestits segons la resistència a la permeació.

- **Tipus 1.**Són constituïts per materials no transpirables i amb resistència a la “permeació”:
–- **Tipus 1a.** Porten l’equip de protecció respiratòria dins del vestit.
-– **Tipus 1b.** Porten l’equip de protecció respiratòria a l’exterior delvestit.
-– **Tipus 1c.** Van connectats a una línia d’aire respirable.
- **Tipus 2.**Són com els 1c, però les costures no són estanques. Són formats per materials no transpirables i amb resistència a la permeació.
- **Tipus 3.**Tenen connexions hermètiques a productes químics líquids en forma de raig a pressió. Són formats per materials no transpirables amb resistència a la permeació.
- **Tipus 4.**Tenen connexions hermètiques a productes químics líquidsen forma d’esprai. El material pot ser transpirable o no, però ha d’oferir resistència a la permeació.
- **Tipus 5.**Tenen connexions hermètiques a productes químics en formade partícules sòlides. Són confeccionats per materials transpirables i el nivell de prestació es mesura per la resistència a la penetració de partícules sòlides.
- **Tipus 6.**Ofereixen protecció limitada davant petits esquitxos de productes químics líquids. Són confeccionats per materials transpirables i el nivell de prestació es mesura per la resistència a la penetració de líquids.

## Ulleres de protecció

Les ulleres tenen com a objectiu protegir els ulls dels treballadors. Per resultar eficaces davant agents químics contaminats, l’element que defineix el protector que s’ha d’utilitzar és la forma de la presentació del contaminant. 

El marcatge de la muntura indica quin és el camp d’ús del protector.

Símbol | Camp d’ús 
--- | ---
Sense símbol | Ús bàsic
3 | Líquids 
4 | Partícules gruixudes de pols
5 | Gasos, partícules fines de pols 
8 | Arc de “curtcircuit” elèctric
9 | Metalls fosos i sòlids calents

{% include note.html content="Les ulleres de protecció amb símbol 3 i 5 són les específiques per protecció contra agents químics" %}

## Selecció

L’equip de protecció individual que se seleccioni ha de complir la legislació de seguretat del producte [RD 1407/1992](https://www.boe.es/buscar/act.php?id=BOE-A-1992-28644). En la pràctica, l’equip ha de disposar del marcatge [«CE»](https://ca.wikipedia.org/wiki/Marca_CE). Amb la col·locació d’aquest marcatge, el fabricant declara que l’EPI s’ajusta a les exigències essencials de sanitat i seguretat definides en el RD1407/92 i modificat pel [RD159/1995](https://www.boe.es/buscar/doc.php?id=BOE-A-1995-5920).

{% include note.html content="La marca **CE** és indispensable en qualsevol EPI que es comercialitzi a la Comunitat Europea." %} 

S’ha de seleccionar l’equip que ofereixi un nivell d’adaptació tant a l’usuari com al desenvolupament habitual de les tasques dutes a terme al lloc de treball. Per això resulta essencial tenir l’opinió dels treballadors sobre lesdiferents solucions possibles. L’execució de proves in situ és un element indispensable per prendre la decisió final de selecció d’un EPI.

## Utilització

Gran part de l’eficàcia d’un equip davant del risc depèn del fet que la utilització i el manteniment siguin correctes. 

### Pautes d’utilització

1. Utilitzar l’equip per als usos previstos, seguint les instruccions del fabricant
2. Col·locar-se i ajustar-se adequadament l’equip d’acord amb les instruccions del fabricant i la formació i informació rebuda a aquest respecte.
3. Utilitzar l’equip mentre s’estigui exposat al risc i tenir presents les limitacions de l’equip indicades en el fulletó informatiu del fabricant.

## EPIs Protecció Respiratòria

{% include note.html content="En exposicions a agents químics, la via d’entrada més important és la respiratòria, per això és molt important utilitzar correctament els equips de protecció de les vies respiratòries." %}

### Pautes d'utilització

- Els equips de protecció de les vies respiratòries estan dissenyats de tal manera que només es poden utilitzar per espais de temps relativament curts. Per regla general, **no s’hi ha de treballar més de dues hores seguides**; en el cas d’equips lleugers o d’execució de treballs lleugers amb interrupcions entre les diferents tasques, l’equip es pot utilitzar durant un període més perllongat. 

- **Abans d’utilitzar un filtre, és necessari comprovar-ne la data de caducitat** impresa i el seu perfecte estat de conservació, segons la informació del fabricant.•Abans de començar a utilitzar els equips els treballadors han de serinstruïts per una persona qualificada i responsable sobre l’ús d’aquests aparells dins l’empresa. Aquest entrenament també ha de contenir les normes de comportament en cas d’emergència.
- **És important que l’empresa disposi d’un sistema de control periòdic** per verificar que els equips es troben en bon estat i que s’ajusten correctament als usuaris. Així, s’han de controlar especialment l’estat de les vàlvules d’inhalació i exhalació de l’adaptador facial, l’estat deles ampolles dels equips de respiració autònoms i tots els elements d’unió entre les diferents parts de l’aparell.
- És necessari vetllar perquè **els aparells no s’emmagatzemin a llocs exposats a temperatures elevades i ambients humits** abans de la seva utilització, d’acord amb la informació del fabricant

## Guants de protecció

Per a aquells agents químics que es manipulin manualment, es fa necessària la utilització de guants que protegeixin les mans dels efectes que poden produir. 

### Pautes d'utilizació

- S’haurà d’establir **un calendari per a la substitució periòdica dels guants** amb la finalitat de garantir que es canviïn abans de ser permeats pels productes químics.
- S’ha de posar atenció a una higiene de les mans adequada i aplicar-s’hi crema protectora si és necessari.•Els guants s’hauran de netejar seguint les indicacions del fabricant.
{% include important.html content="La utilització de guants contaminats pot ser extremadament perillosa,a causa de l’acumulació de contaminant en el material component del guant." %}

## Roba de Protecció

La via d’entrada dèrmica dels agents químics té una importància relativa comparada amb la respiratòria. Tanmateix, per a aquells agents químics amb capacitat de penetració per via dèrmica,és molt important protegir correctament tota la superfície corporal.

### Pautes d'utilizació

- En **els vestits** de protecció **per a treballs amb maquinària, els finals de la màniga i camal s’han de poder ajustar bé al cos i els botons i butxaques han de quedar coberts**.
- **Els vestits** de protecció **contra substàncies químiques requereixen materials de protecció específics** davant el compost del qual s’han deprotegir. En tot cas, s’han de seguir les indicacions del fabricant.
- **Els vestits de protecció sotmesos a fortes agressions d’agents químics** estan dissenyats de manera que les persones entrenades els pugin utilitzar **durant un màxim de 30 minuts**. **Els vestits sotmesos a agressions menys agressives** es poden portar **durant tota la jornada de treball**.
- Pel que fa al desgast i la conservació de la funció protectora **és necessari assegurar-se** que les peces de roba **no pateixen cap alteració durant tot el temps que siguin en ús**. Per aquesta raó, s’ha d’examinar la roba de protecció a intervals regulars per comprovar el seu perfecte estat de conservació, les reparacions necessàries i la neteja correcta.S’ha de planificar una reposició correcta de les peces.
- Per mantenir durant el màxim de temps possible la funció protectora de les peces de protecció i evitar riscos per a la salut de l’usuari és necessari seguir al peu de la lletra les instruccions de rentat i conservació proporcionades pel fabricant per garantir-ne una protecció invariable.
- En el cas de rentat i neteja de tèxtils que no portin tractament permanent contra els efectes nocius, és necessari que posteriorment se’n faci el tractament protector en un establiment especialitzat.
- En la reparació de peces de protecció només s’han d’utilitzar materials que tinguin les mateixes propietats.

## Ulleres de protecció. 

La protecció dels ulls dels treballadors exposats aagents químics es fa amb ulleres. 

### Pautes d'utilizació

- Els protectors oculars de qualitat òptica baixa només s’han d’utilitzar esporàdicament.
- Les condicions ambientals de calor i humitat afavoreixen que els oculars s’entelin, però no són úniques. Un esforç continuat i postures incòmodes durant el treball també provoquen la sudoració de l’usuari i, per tant, l’entelament de les ulleres. Aquest és un problema molt difícil de solucionar, encara que es pot minimitzar amb una selecció adequada de la muntura, del material dels oculars i de les proteccions addicionals.
- La manca o deteriorament de la visibilitat pels oculars és un origen de risc en la majoria dels casos. Per aquest motiu, aconseguir que aquesta condició es compleixi és fonamental. Per aconseguir-ho, cal netejar aquests elements diàriament seguint les instruccions del fabricant.
- Per aconseguir-ne una bona conservació, els equips s’han de desar,quan no s’utilitzin, nets i secs als seus estoigs corresponents. Si estreuen per breus moments, s’han de col·locar no deixar-los amb els oculars cap per avall, per evitar-ne ratllades.
- Per prevenir malalties de la pell, els protectors s’han de desinfectar periòdicament, seguint les indicacions donades pel fabricant per tal que el tractament no afecti les característiques i prestacions dels diferents elements.

## Normes reguladores

Entre totes les normes legals que regulen l’ús d’un EPI, és especialment important l’[RD 773/1997](https://www.boe.es/buscar/doc.php?id=BOE-A-1997-12735)sobre disposicions mínimes de seguretat i salut relatives a la utilització pels treballadors d’equips de protecció individual. D’aquest Reialdecret, hi ha publicada una [guia tècnica](https://www.insst.es/documents/94886/203536/Gu%C3%ADa+t%C3%A9cnica+para+la+evaluaci%C3%B3n+y+prevenci%C3%B3n+de+los+riesgos+para+la+utilizaci%C3%B3n+por+los+trabajadores+en+el+trabajo+de+equipos+de+protecci%C3%B3n+individual/c4878c11-26a0-4108-80fd-3ecbef0aee38) per part de l’INSHT.

Altres normes importants són les següents:
- La Llei de prevenció de riscos laborals. [Llei 31/1995, de 8 de novembre Publicada en el BOE núm. 269, de 10 de novembre.](https://www.boe.es/buscar/act.php?id=BOE-A-1995-24292)
- El Reglament dels serveis de prevenció [Reial decret 39/1997, de 17 de gener](https://www.boe.es/buscar/act.php?id=BOE-A-1997-1853)
- El Reglament sobre la utilització d’equips de protecció individual [Reial decret 773/1997, de 30 de maig](https://www.boe.es/buscar/act.php?id=BOE-A-1997-12735).
- El Reglament sobre comercialització d’equips de protecció individual [Reial decret 1047/1992, de 20 de novembre. Modificat pel Reial decret159/1995, de 2 de febrer](https://www.boe.es/buscar/doc.php?id=BOE-A-1992-28644).
- Reglament sobre notificació de substàncies noves i classificació, envasament i etiquetatge de substàncies perilloses [Reial decret 363/1995, de 10 de març](https://www.boe.es/buscar/doc.php?id=BOE-A-1995-13535)
- Reial decret relatiu a l’aproximació de les legislacions dels estats membre sobre màquines. [Reial decret 1435/1992, de 27 de novembre](https://www.boe.es/buscar/doc.php?id=BOE-A-1992-27456)

