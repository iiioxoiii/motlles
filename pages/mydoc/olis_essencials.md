---
title: "Olis essencials"
tags: [olis]
summary: "Són olis produïts per la destil·lació de balsams, plantes o petroli"
sidebar: mydoc_sidebar
permalink: olis_essencials.html
---

És poden classificar per l'origen: vegetal o mineral.

## Olis vegetals

Són dissolvents orgànics. Dissolen resines, ceres, serveixen per fer vernissos, per a retirar-los, per fer repints i retocs.
 
- Són subproductes dels bàlsams per destil·lació.
- Són líquids d’origen vegetals.
- S'obtenen de fulles, flors, de tiges i de branques (rementina, essència d’espígol, de romaní, clau, etc.)
- Són líquids transparent d’olor aromàtic que evaporen més o menys ràpidament i assequen per oxidació. 
- Són dissolvents orgànics. Es fan servir sempre en la neteja d’eines pinzells i estris.
- No serveixen d’aglutinants ni d’adhesius.

### Essència de Trementina

Existeixen diferents tipus de trementina i qualitats comercials. En general s’obtenen d’arbres com el pi comú o el pi marítim. Es pot obtenir del tronc o de les fulles.

Es coneix des de el segle XVII. Té olor aromàtica i en la seva obtenció s’obté un subproducte que és la colofònia. L’essència de trementina és inestable per l’acció convinada de l’aire i la llum. S’oxida fàcilment i tendeix a resinar-se.
 
La trementina normal (la que venen a les drogueries només es pot fer servir per netejar o per diluir pintures o vernissos domèstiques. La millor trementina és la rectificada o doblement rectificada que es la que es fa servir en restauració. Una taca de trementina a sobre de trementina d’amunt d’un foli no ha de deixar marca un cop evaporada. Aquesta trementina rectificada s’obté després d’un procés de refinat.

- Ha de ser un líquid totalment transparent.
- Té l’inconvenient que s’oxida en contacte amb les metal·litzacions.
- Serveix per dissoldre resines i ceres.
- És tòxica per contacte, per inhalació i és inflamable. No és miscible en aigua.

{% include important.html content="La trementina provoca moltes al·lèrgies. Actualment s'està substituint pel White Spirit d’origen mineral."%}
 
#### Aplicacions 

- Aprimar olis i pintures.
- Dissoldre resines i ceres.
- Mesclada amb la pintura a l’oli accelera el assecat.
- El seu abús provoca clivellat.
 
## Essències Minerals

Són d’origen fòssil. S’obtenen per destil·lació del petroli. No són miscibles en aigua. Els hidrocarburs són compostos senzills de carboni i es troben en el petroli. Per diferents fraccions o temperatures s’obtenen diferents productes: els alifàtics, els aromàtics i cíclics
 
### White Spirit
 
És un compost carbònic alifàtic. S’anomena aiguarràs mineral o èter de petroli. Ha substituït a la trementina perquè és molt estable no s’oxida ni es resina. És un líquid transparent com l’aigua i molt volàtil. És un dissolvent orgànic d’origen fòssil.
- No és miscible en aigua però sí amb etanol i acetona.
- Dissol ceres i algunes resines.
- Sempre s’utilitza per a neteges de metal·litzacions perquè no ataca als metalls.
- Existeixen diferents versions de White Spirit. El més econòmic és el aiguarràs símil. Té una forta olor però també el venen sense olor.
- Actua sempre com a dissolvent i diluent. És tòxic per contacte i inhalació.

{% include important.html content="És tòxic per contacte i inhalació." %}

### Essència de petroli
 
És un hidrocarbur alifàtic. Es coneix des de el segle XVI. Petroli vol dir oli de pedra (petra oleum). S’obté del petroli per destil·lació a diferents pressions. Del petroli s’obtenen diferents productes després de ser refinat. S’obté la benzina, el fuel, el gasoil, la vaselina, la parafina, la ceresina i molts d’altres dissolvents.
- S’ha fet servir antigament per dissoldre resines per fer vernissos. També com a diluent de pintures a l’oli.
- Avui es fa servir sobre tot per fer neteges d’estris i pinzells.
- No és miscible en aigua i és tòxic per contacte i inhalació.

{% include important.html content="És tòxic per contacte i inhalació." %}


