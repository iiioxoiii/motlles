---
title: "Olis"
tags: [olis]
summary: "Líquids usats per molts propòsits com consolidants naturals per polimerització al assecat"
sidebar: mydoc_sidebar
permalink: olis.html
---


Estan documentat de de l'antiguitat principalment en el món de la gastronomia, la medicina, la cosmètica i usos artístics. Les primeres fons els situen entre el segle V i el VI d.C.

Artísticament en parla el tractat de Luca al s.XVII i el “De diversis artibus” s.XI-XII de Theophilus Prevere. A partir del s. XII els olis ja estan totalment documentats.



## Composició
Estan formats per glicèrids dels àcids grassos. (àcid oleic, palmític, esteàric, linoleic).

## Classificació 

1. Naturals
2. Artificials
3. Orgànics vegetals
4. Orgànics animals. 
5. Orgànics minerals ó olis esencials.


## Propietats

- Poden ser assecants, semi-secants i no assecants.
- Canvien la seva viscositat en funció de la temperatura: a més temperatura més solvents i a poca temperatura poden arribar a solidificar-se.
- Són higrofugants.
- Reaccionen amb contacte amb l'oxigen. A l'oxidació es produeix polimerització que crea una pel·lícula elàstica de l'exterior a l'interior.
- Es pot accelerar afegint productes secants que contenen polo, colorants.

## Aplicacions

Com vernissos, aglutinants i adhesius

{% include important.html content="El seu abús en pintura provoca clivellat." %}
 
## Olis secants

Són el més emprats en les tècniques artístiques.

### Oli de llinosa

- És el més emprat en la pintura a l'oli. S'obté de les llavors d'una planta anomenada [*linum usitatissimum*](https://ca.wikipedia.org/wiki/Lli). Es coneix el seu cultiu des de fa 10000 anys a Babilònia. L'oli surt de les llavors premsades. Un cop obtingut el líquid es filtrat i refinat per treure les impureses.
- El color és groc clar.
- Engrogueix amb el temps.
- El procés d'assecat pot trigar anys.
- L'abús és origen de molts problemes.
- S´utilitza com aglutinant però s'ha fet servir com a vernís de pàtines, com adhesiu de metal·litzacions, com a poliment en restauració, i per a fer re-entelats.

### Oli d'anous

- S'obté del premsat de les nous madures que provenen de la [noguera](https://ca.wikipedia.org/wiki/Noguer_com%C3%BA) *(juglans regia*. Està documentat des de el segle V d.C. S'ha emprat molt a Itàlia (el feia servir Leonardo), Holanda i Alemanya.
- És més fluid i clar que el llinosa
- Engrogueix menys que el de llinosa.
- És més assecant que el de cascall.
 
### Oli de cascall

- S'obté de les llavors moltes i premsades de la planta de [l'opi](https://ca.wikipedia.org/wiki/Oli_de_cascall), rosella (o radebac).Es de flor blanca. 
- S'ha fet servir des de el s. XVII  sobretot a Holanda.
- És el més transparent i fluid. Aquesta fluïdesa el fa idoni per eliminar el rastre del pinzell.
- Engrogueix menys que el llinosa.
- Asseca lentament.
 
### Oli de gira-sol

- S'obté de les llavors de la planta del gira-sol. 
- S'ha fet servir sobretot a Rússia des de el s. XVIII i en èpoques de guerra i post guerra com a substitut de l'oli de llinosa.
- Les pipes blanques donen un oli molt transparent que és el millor de gira-sol.
- Hi han més de 50 tipus de pipes
- Engrogueix poc.
 
## Olis semi-secants

### Oli de coto

- Fet de [cànem](https://ca.wikipedia.org/wiki/C%C3%A0nem), de [sèsam](https://ca.wikipedia.org/wiki/S%C3%A8sam).
- Triguen molt a assecar.
- Gairebé no es fan servir.
 
## Olis no assecants

### Oli de Ricí

- S'extreu de les les tres llavors que te cada fruit de la planta de [Ricí](https://ca.wikipedia.org/wiki/Ric%C3%AD) *(ricinus communis)*. També és coneguda amb els noms de **cagamuja**, **enfiter**, **figuera infernal** o **fesolera de llum**, entre altres.La planta creix pel mediterrani.
- No engrogueixen.
- No asseca mai.
- No serveix com aglutinant.
- Es fa servir en la indústria de pintures.
- Té aplicacions en el camp de la conservació: per donar elasticitat i per fustes resseques.
- També en petites quantitats per donar més elasticitat als vernissos.
- Es pot diluir amb etanol.
