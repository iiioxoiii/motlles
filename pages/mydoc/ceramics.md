---
title: "Ceràmics"
tags: []
summary: "Els materials ceràmics cristalitzen de manera similar als metalls amb àtoms diferents"
sidebar: mydoc_sidebar
permalink: ceramics.html
---

Varien molt i van des de components simples a d'altres amb moltes fases enllaçades.
A més hi ha moltes diferències entre els enllaços que converteixen els materials
ceràmics en durs i fràgils amb baixa tenacitat i ductilitat.

## Composició

Els materials ceràmics són materials inorgànics no metàl·lics, estan compostos
per materials metàl·lics i no metàl·lics, enllaçats amb enllaços iònics i/o
covalents.

### Estructura dels compostos ceràmics senzills

Tenen una estructura similar a la dels metalls però els ceràmics tenen més d'un component
a la xarxa. (ex. BCC i hexagonal compacta)

### Estructura dels silicats

Els silicats estàn conformats per enllaços covalents. Hi ha molta quantitat de silicats a la natura i per
això es fan servir molt: un bon preu, disponibilitat i bones propietats.

El silici SiO2 és la matèria príma de molts materials ceràmics. L'estructura que forma és tetraèdrica (cristobalita) amb el
silici i l'oxígen.

(falta imatge cristall de Silici)

### Estructura del vidre

És un dels materials ceràmics més importants. La capacitat de ser transparent, ser dur i
resistent a la corrossió fa que sigui una opció molt bona per la industria malgrat s'estigui
substituïnt per certs polímers.

És un material innorgànic de fusió que s'ha refredat sense que hagi cristalitzat. Per la seva
fabricació es parteix de materials inorgànics que eleven la seva temperatura fins la seva
fusió per després refredar-lo fins un estat rígid, però sense que es produeixi cristalització.

La cristalització és un procés d'ordenacio repetitiu i la solidificació del vidre és una malla caòtica.

## Tipologies industrials

### Tradicionals

Són argiles, porcellanes i vidres.

### Específics per enginyeria

Òxids d'alumnini, carburs de silici o nitrurs de silici

## Propietats

- Normalment són bons aïllants elèctrics i tèrmics.
- Tenen una temperaturad de fusió alta.
- Bona resistència als atacs químics.



