---
title: "Plastilines"
tags: [materials plàstics, polímers]
summary: "És un material plàstic que es fa servir per modelar, de colors i compost de sals de càlci, vaselina i altres components alifàtics, principalment d'àcid esteàtic."
sidebar: mydoc_sidebar
permalink: plastilines.html
---

El material el va inventar l'industrial alemany [Franz Kolb](https://es.wikipedia.org/wiki/Franz_Kolb) al 1880, com una sol·lució pels artistes que modelaven amb fang.
[+Wikipedia](https://es.wikipedia.org/wiki/Plastilina)