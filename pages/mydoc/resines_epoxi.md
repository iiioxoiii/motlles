---
title: "Resines epoxi"
tags: [materials plàstics, polímers, resines]
summary: "Una Resina Epoxi o poliepòxid és un polímer orgànic termoestable que s'endureix quan es barreja amb un agent catalitzador."
sidebar: mydoc_sidebar
permalink: resines_epoxi.html
---

Les resines epoxi més freqüents són producte d'una reacció entre [l'epiclorohidrina](https://ca.wikipedia.org/wiki/Epiclorohidrina) i [el bisfenol-a](https://ca.wikipedia.org/wiki/Bisfenol_A).

{% include important.html content="Són molt tòxiques i s'haurien de fer servir només en casos excepcionals." %}

## Tipus

És poden trobar fabricades per molts fabricants. 

## Propietats

- Poca resistència als raig UV.
- Gran resistècia a càrregues.
- Poca elongació fins a la ruptura.
- Alta resistència a agents físics i químics.
- Alta resistència a la temperatura.
- Tendència a absovir l'humitat.
- Bona adhesió a l'alumnini, acer i molts pàstics.
- Es contrauen amb el temps.
- Normalment són més dures que els altres materials que participen en les unions.
- Hi ha un ARALDIT epoxi especial fusta que és una mica flexible per adaptar-se als canvis dimensionals de la fusta.

## Polimerització

Generalment la resina polimeritza amb la mescla de dos components. Però existeixen resines epoxi d'un component i que endureixen per aplicació de calor. Algunes d'aquestes resines d'un component tenen la capacitat d'absorvir olis. 

## Restauració

En retaules, pintura sobre talla, motllures, marcs, etc.


## El Cianocrilat

{% include important.html content="El cianorilat és molt crític d'utilitzar. Hi han que no són gens viscosos i és molt fàcil tenir accidents. Són irreversibles. La seva combustió genera cianur." %}

El [cianocrilat](https://ca.wikipedia.org/wiki/Cianoacrilat) s'endureix per l'absorció d'humitat ambiental. És el conegut com Loctite o Superglue.

## Riscos amb el treball de resines EPOXI

Hi han riscos en les següens fases:

- En les resines sense tractar
- Els els agents enduridors.
- En els plastificants, diluients i dissolvents.
- Les càrregues o «fillers» i pigments.
- En les resines tractades.

### Riscos al contacte amb la pell de:

- Les resines no curades.
- Els productes de curat.
- Dissolvents per augmentar la fluidesa o la neteja. 

### Riscos en vies respiratòries per l'exposició:

- Volàtils o emanacions que es produeixen durant la mescla.
- Vapors despresos per dissolvents organics.
- Pols de labors de mecanitzat.
- Pols de les càrregues afeguides a la resina, silíci, caolín, etc.

### Riscos pels ulls en els casos:

- Treballs amb compostos expoxi amb agents irritants i sensibilitzadors actius.
- Treballs de mecanitzat en els que es puguin produir projeccions.
- Esquitxades i projeccions de producctes líquits.


<div class="row">
        <div class="col-lg-12">
            <h2 class="page-header">Riscos amb el treball de resines EPOXI</h2>
        </div>
        <div class="col-md-4">
            <div class="media">
                <div class="pull-left">
                    <span class="fa-stack fa-2x">
                          <i class="fa fa-circle fa-stack-2x text-primary"></i>
                          <i class="fa fa-tree fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Service One</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo itaque ipsum sit harum.</p>
                </div>
            </div>
            <div class="media">
                <div class="pull-left">
                    <span class="fa-stack fa-2x">
                          <i class="fa fa-circle fa-stack-2x text-primary"></i>
                          <i class="fa fa-car fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Service Two</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo itaque ipsum sit harum.</p>
                </div>
            </div>
            <div class="media">
                <div class="pull-left">
                    <span class="fa-stack fa-2x">
                          <i class="fa fa-circle fa-stack-2x text-primary"></i>
                          <i class="fa fa-support fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Service Three</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo itaque ipsum sit harum.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="media">
                <div class="pull-left">
                    <span class="fa-stack fa-2x">
                          <i class="fa fa-circle fa-stack-2x text-primary"></i>
                          <i class="fa fa-database fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Service Four</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo itaque ipsum sit harum.</p>
                </div>
            </div>
            <div class="media">
                <div class="pull-left">
                    <span class="fa-stack fa-2x">
                          <i class="fa fa-circle fa-stack-2x text-primary"></i>
                          <i class="fa fa-bomb fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Service Five</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo itaque ipsum sit harum.</p>
                </div>
            </div>
            <div class="media">
                <div class="pull-left">
                    <span class="fa-stack fa-2x">
                          <i class="fa fa-circle fa-stack-2x text-primary"></i>
                          <i class="fa fa-bank fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Service Six</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo itaque ipsum sit harum.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="media">
                <div class="pull-left">
                    <span class="fa-stack fa-2x">
                          <i class="fa fa-circle fa-stack-2x text-primary"></i>
                          <i class="fa fa-paper-plane fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Service Seven</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo itaque ipsum sit harum.</p>
                </div>
            </div>
            <div class="media">
                <div class="pull-left">
                    <span class="fa-stack fa-2x">
                          <i class="fa fa-circle fa-stack-2x text-primary"></i>
                          <i class="fa fa-space-shuttle fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Service Eight</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo itaque ipsum sit harum.</p>
                </div>
            </div>
            <div class="media">
                <div class="pull-left">
                    <span class="fa-stack fa-2x">
                          <i class="fa fa-circle fa-stack-2x text-primary"></i>
                          <i class="fa fa-recycle fa-stack-1x fa-inverse"></i>
                    </span>
                </div>
                <div class="media-body">
                    <h4 class="media-heading">Service Nine</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Illo itaque ipsum sit harum.</p>
                </div>
            </div>
        </div>
    </div>


