---
title: "Ceres sintètiques"
tags: [ceres, ceres sintètiques]
summary: "La seva fabricació en basa en el polietileglicol i hi han molts tipus però també hi han d'altres."
sidebar: mydoc_sidebar
permalink: ceres_sintetiques.html
---
 
## Cera PEG (polietileglicol)

- Són hidrosolubles i higroscòpiques i incolores.
- Es presenten en forma líquida més o menys viscoses i també en forma sòlida, escates o pols.
- Es fan servir especialment pel tractament i consolidació de fustes mullades, per submersió.
- Tenen el punt de solidificació de 4º a 58º segons el tipus.
 
## Ceres BASF

Són ceres de polietilè en forma de pols blanca que fonen a 100º.
- Dissolen en white spirit, trementina, toluè i xilè.
- En restauració sempre es dissolen en white spirit.