---
title: "Resines de Polièster"
tags: [materials plàstics, polímers, resines]
summary: "Són materials termoestables, transparents"
sidebar: mydoc_sidebar
permalink: resines_de_poliester.html
---

Són resines líquides termoestables consisteixen en una sèrie de precursors líquits o semilíquits, que han de curar-se per aconseguir l'estat sòlid, per reaccions químiques, térmiques (altes temperatures), o per radiaciones (UV, gamma, electrons o microones). Un cop curades, tenen gran quantitat d'enllaços creuats i no poden tornar-se a fondre sense sufrir una greu degradació.


## Propietats

Les resines endurides son sòlids transparents de poca resistència a la tracció i a l'impacte. Les resines poliestériques poden ser reforçades amb l'adicció de fibra de vidre.

## Polimerització

Per la polimerització s'ha de fer servir un catalitzador, generalment un [peròxid orgànic](https://ca.wikipedia.org/wiki/Per%C3%B2xid_org%C3%A0nic). L'enduriment de la resina consisteix en la unió del poliàcid i l'alcohol a través de mol·lècules d'un monòmer insaturat (generalment [l'estiré](https://ca.wikipedia.org/wiki/Estir%C3%A8) ).

[+ info](https://www.cosmos.com.mx/wiki/resinas-poliester-poliestericas-cq7p.html)

## Tipus

- Resines alquidíliques: Base de la majoria de las pintures olisolubles.
- Polièsters no saturats: Resines termoestables usades com pols per modelar.
- Tereftalats de polietilè: Usats como fibres textils.
- Policarbonats aromàtics: Suport de forros metàl·lics i matrius per motlles de fosa.

### MELINEX

Són llàmines fines de resina de poliester de diferents gruixos resistent a les temperatures altes a l’aire i a la humitat. Serveixen de protecció per obres d’art en el seu emmagatzematge. També com a aïllant de suports. Per reforçar teles de fibra de vidre. Per l’aplicació d’adhesius termoplàstics en calent per poder lliscar la espàtula calenta.
