---
title: "Cera d'Abella"
tags: [materials de restauracio, cera, cera animal ]
summary: "És una substància secretada que prové de les abelles obreres per fabricar bresca. Hi han dos tipus: La verge i la blanca. Són les més emprades en restauració"
sidebar: mydoc_sidebar
permalink: cera_abella.html
---
 
## Cera d'abella verge
És la que s'obté directament de la bresca. És de color groguenc quant és fresca. Conté impureses. Decolora al sol. És aromàtica (la que més). Es presenta en barres.
- Fon als 64ºC
- Es soluble en trementina calenta, xilé i tolué.
- Es més adhesiva que la cera blanca i més elàstica.
- Aplicacions: Protecció de fustes, poliments i acabats de mobles. ex. 1 part en pes de cera x 2 parts de trementina.
  
## Cera d'abella blanca

També s'anomena grum. És la mateixa que la verge però decolorada i sense impureses.

- És més dura que la verge i es presenta en forma de grumolls o perletes blanques.
- Es fa servir per metal·litzar vernissos (treure brillantor), per protecció de metalls i consolidacions.
 
