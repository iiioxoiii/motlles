---
title: "Bibliografia"
tags: []
summary: ""
sidebar: mydoc_sidebar
permalink: bibliografia.html
---

## Arquitectura

### Materials

«Construir la arquitectura Del material en bruto al edificio. Un manual Construir la arquitectura»
AUTOR Andrea Deplazes
EDITORIAL Gustavo Gili
LUGAR DE PUBLICACIÓN Barcelona
AÑO 2010
PÁGINAS 555

«Piedra natural.Tipos de piedra, detalles, ejemplos»
AUTOR Theodor Hugues, Ludwig Steiger, Johann Weber
Editorial GG
Colección DETAIL Praxis

«Materiales traslúcidos. Vidrio, plástico, metal»
Frank Kaltenbach, Ioanna Symeoridou (tr.)
Detalles del libro
Editorial	Editorial Gustavo Gili, S.L.
Edición	1ª ed., 1ª imp.(14/01/2008)
Páginas	112
Dimensiones	30x21 cm
Idioma	Español
ISBN	9788425221842
ISBN-10	8425221846
Encuadernación	Rústica

«Membrane Structures»
AUTOR Klaus-Michael Koch
EDITORIAL Prestel Publishers
LUGAR DE PUBLICACIÓN Múnich
AÑO 2004
PÁGINAS 263

«ENLUCIDOS, REVOCOS, PINTURAS Y RECUBRIMIENTOS DETALLES, PRODUCTOS, EJEMPLOS»
REICHEL, ALEXANDER / HOCHBERG, ANETTE /  
Editorial: GUSTAVO GILI
Año de edición: 2011
ISBN: 978-84-252-2186-6
Encuadernación: Rústica

