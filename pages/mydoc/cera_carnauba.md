---
title: "Carnauba"
tags: [cera, cera vegetal]
summary: "És una cera vegetal que s'obté d'una palmera brasilenya"
sidebar: mydoc_sidebar
permalink: cera_carnauba.html
---

## Història 

Fou introduïda a Europa al segle XIX i s'obté de les  fulles d'una palmera del Brasil que s'anomena carandaí. 

## Presentació 

Es presenta en forma d'escates. El seu color va del groguenc al gris fosc depenent de la varietat de l'arbre.


## Característiques

- És una cera extremadament dura i brillant.
- Dissol en trementina.
- Es fa servir sobretot per afegir duresa en altres ceres més toves.
- Malgrat el punt de fusió tant elevat es pot barrejar amb la cera d'abella.
- És més opaca que la cera d'abella.
- S'utilitza sobre tot per poliments i acabats en fusta (mobles).
- També es fa servir en farmàcia i gastronomia.
- El seu punt de fusió va dels 82 als 88ºC. 

### Recepta
 
1 part de carnauba + 2 parts de cera d'abella blanca + 6 parts de trementina. Cal dissoldre cada una apart i adjuntar-les després un cop líquides remenant.
 
