---
title: "Inflamable"
tags: [riscos laborals, etiquetat]
summary: "El símbol que identifica el perill d'inflamabilitat de gasos, aerosols, líquids, sòlids. Identifica reaccions produïdes per un iniciador o per reacció espontània."
sidebar: riscos_laborals_sidebar
permalink: perill_inflamable.html
---

{% include precaution.html content="S'ha d'evitar el calentament" %}

![Pictograma Inflamable](http://dimasur.es/476-thickbox_default/etiqueta-pictograma-clase-3-liquidos-inflamables.jpg)


# Divisions 

A cada divisió s’associa una frase H (d’indicació de perill) diferent. Les frases han de figurar en un dels idiomes oficials al país on es comercialitza la substància o barreja i que estigui publicat al reglament.

Divisió | Frase | Paraula
--- | --- | --- |
Gas inflamable Cat.1 | H220: Gas extremadamente inflamable | Peligro
Gas inflamable Cat.2 | H221: Gas inflamable | Atención
Aerosol inflamable Cat.1 | H222: Aerosol extremandamente inflamable | Peligo
Aerosol inflamable Cat.2 | H223: Aerosol inflanmable | Atención
Líquid inflamable Cat.1 | H224: Líquido i vapor extremadamente inflamable | Peligro
Líquid inflamable Cat.2 | H225: Líquido i vapor altamente inflamable | Peligro
Líquid inflamable Cat.3 | H226: Líquido i vapor inflamable | Atención |
Sòlid inflamable Cat. 1 | H228: Sòlid inflamable | Peligro
Sòlid inflamable Cat. 2 | H228: Sòlid inflamable | Atención
Substàncies autoreactives Tipus A | H240: Calentamiento puede causar explosión | Peligro
Substàncies autoreactives Tipus B| H241: Calentamiento puede causar fuego o explosión | Peligro
Substàncies autoreactives Tipus C i D| H242: Calentamiento puede causar fuego | Peligro
Substàncies autoreactives Tipus Ei F| H242: Calentamiento puede causar fuego | Atención
Substàncies autoreactives Tipus G | - | - |
Líquids pirolítics Cat. 1 | H250: Se inflama espontáneamente en contacto con el aire | Peligro
Sòlids pirolítics Cat. 1 | H250: Se inflama espontáneamente en contacto con el aire | Peligro
Substàncies i mescles que experimenten esclafament espontàni Cat 1 | H251: Se calienta espontáneamente, puede inflamarse. | Peligro
Substàncies i mescles que experimenten esclafament espontàni Cat 1 | H252: Se calienta espontáneamente en grandes cantidades, puede inflamarse. | Atención
Substàncies i mescles que, en contacte amb l'aigua desprenen gasos inflamables Cat 1 | H260: En contacto amb l'aigua despren gasos inflamables que poden inflamar-se espontàniament | Peligro
Substàncies i mescles que, en contacte amb l'aigua desprenen gasos inflamables Cat 2 | H261: En contacte amb l'aigua desprenen gasos inflamables. | Peligro
Substàncies i mescles que, en contacte amb l'aigua desprenen gasos inflamables Cat 3 | H261: En contacte amb l'aigua desprenen gasos inflamables. | Atención

## Napo i el Perill Inflamable

<div class="row text-center">
  <iframe width="660" height="415" src="https://https://youtu.be/cicwpN01Xc4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

</div>