---
title: "Teixits sintètics i minerals"
tags: []
summary: "Són aquelles fibres d'origen mineral o sintètiques que poden ser transformades en filats i en conseqüència ser teixides, segons diversos sistemes de tramat o lligat."
sidebar: mydoc_sidebar
permalink: teixits_minerals.html
---

Són les d'origen inorgànic com és la fibra de vidre els fils metàl·lics i l'amiant o les sintètiques (artificials) orgàniques derivades de materials organics.

## Propietats Generals

- Resisteix l’aigua
- En general no es cremen
- S’estoven a 500ºC
- Son teles dures i resistents
- Resisteixen l’acció dels dissolvents, la intempèrie
- Es poden tenyir.
- No són atacades per els microorganismes
- Són teles aïllants de la calor i la electricitat

## Aplicacions en restauració

- Per rascar amb el «Llapis de fibra de vidre»
- Suports per a pintura mural arrencada
- Reentelatges
- Reforç de trencaments de metalls
- Reintegracións de suports de múltiples aplicacions
- També existeixen les massilles de fibra de vidre
- Industrialment té moltes aplicacions: aïllant, fabricació d’avions, vaixells, cotxes, etc.

### Fils metàl·lics

Existeixen metalls tous i dúctils que poden ser laminats o estirats en forma de fil que poden ser utilitzats per teixits, teles, brodats o cosits. A part dels metalls nobles (or, plata) També s’ha fet servir el coure i el llautó com aliatges. Aquests fils s’han fet servir per decoracions o brodats de vestuari de ritual i també per a pendons (estandartes). Els primers fils eren totalment metàl·lics, després van comprovar que era més lleuger folrar fils metàl·lics amb fils de seda o cotó recoberts en forma d’espiral. També hi ha la llana d’acer.

### Fibra de vidre

Les matèries primes són la sílice, carbonat de calci, borat de calci i el caolí. Per fusió a altes temperatures s’obté el vidre fluid que posteriorment pot ser filat. El punt de fusió sol ser entre 1200 i 1300 ºC. D’aquesta fibra de vidre s’obté la tela de fibra de vidre molt utilitzada en restauració.

### Fibra de carboni

La fibra de carboni és un material compost, fabricat a partir d'una matriu de polímer consolidats amb fibra de carboni. Cada fibra està feta de milers de fibres de carboni d'un diàmetre d'entre 5 i 8 micròmetres [µm]. Al tractar-se d'un material compost, en la majoria dels casos -aproximadament un 75% - s'utilitzen polímers termoestables. El polímer és habitualment resina epoxi, de tipus termoestable tot i que d'altres polímers, com el polièster o el viniléster també s'usen com a base per a la fibra de carboni encara que estan caient en desús.

Les característiques d'aquest material són:

- Com en altres materials compostos, s'uneixen les característiques de la matriu amb les de les fibres per aconseguir un teixit d'alta resistència, lleuger i força elàstic.
- La matriu polimèrica garanteix que sigui un bon aïllant tèrmic i el tractament realitzat en la resina epoxi assegura que tingui propietats ignífugues.
- És molt resistent als canvis de temperatura, conservant la seva forma.
- Baixa densitat, en comparació amb altres materials com l'acer.
- Resistent a agents externs.
- Elevat cost de producció: el cost de la fibra de carboni és 10 vegades més cara que l'acer.

[+viki](https://ca.wikipedia.org/wiki/Fibra_de_carboni)

Les raons de l'elevat preu dels materials realitzats amb fibra de carboni es deu a diversos factors:
 
## Fibres sintètiques

Les fibres sintètiques són el resultat de la recerca científica per tal de voler millorar les fibres naturals que proporcionen algunes plantes i animals. En general les fibres sintètiques es creen forçant, normalment per extrusió les fibres. Anteriorment al descobriment de les fibres sintètiques es feien fibres de la cel·lulosa vegetal. Les fibres sintètiques s'obtenen de tres fonts principals: la cel·lulosa, el petroli i els minerals.

Actualment les fibres sintètiques representen la meitat de les fibres que s'utilitzen. Dominen el mercat el nylon, polièsters, acrílics i poliolefines. Aquests quatre tipus de fibres representen el 98% en volum de la producció de fibres sintètiques, el polièster sol representa el 60%.


[+Viki](https://ca.wikipedia.org/wiki/Fibra_sint%C3%A8tica)

[+Wiki](https://es.wikipedia.org/wiki/Fibra_artificial)


