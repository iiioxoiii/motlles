---
title: "Resines acríliques"
tags: [materials plàstics, polímers, resines, termoplàstics]
summary: "Anomenades acríliques o resines d’acrilat, són componentes termoplàstics o termoestables"
sidebar: mydoc_sidebar
permalink: resines_acriliques.html
---

Són substàncies derivades de [l'àcid acrílic](https://ca.wikipedia.org/wiki/%C3%80cid_acr%C3%ADlic), [àcid metacrílic](https://ca.wikipedia.org/wiki/%C3%80cid_metacr%C3%ADlic) o altres compostos relacionats. 

Es presenten en forma de disperssió aqüosa o en forma sòlida per a dissoldre en dissolvents orgànics.

{% include important.html content="Són resines tòxiques i cal prende totes les precaucions." %}

## Característiques

- Es fan servir com aglutinants, vernissos, consolidacions, adhesius, emprimacions i capes de protecció.
- Són els productes sintètics més emprats en restauració. 
- Fan films transparents.
- No engrogueixen.
- Són flexibles.
- En general són brillants.  
- No són atacades pels microorganismes


## Usos

- Com components habituals en algunes pintures.

- Usos sanitaris com els [composits dentals](https://ca.wikipedia.org/wiki/Comp%C3%B2sit_dental) [endurits per llum UV](https://ca.wikipedia.org/wiki/Curat_per_UV).

- En la fabricació del **metacrilat** a partir del [polidroxietilmetacrilat](https://es.wikipedia.org/wiki/Polimetilmetacrilato)

## Restauració

Tenen aplicació en tots el camps de la restauració.


### Dedacryl X 122

És un polibutilmetacrilat. És un adhesiu que es caracteritza pel seu baix pes molecular. Pot fer-se servir com adhesiu en capes pictòriques en concentracions baixes (solucions al 5-10% en white spirit) i no introsueix tensions noves a la película pictòrica. 

- És disol en xilè. 
- El Synocryl 9122 X (Cary Valley) presenta la mateixa composició i les mateixes característiques.

C Scicilone.«Restauración de la pintura contemporánea». ed. Nerea

### Paraloid B-72

És una resina acrílica que va ser creada per Rhom and Haas per ser una substància de suport per la tinta flexogràfica anomenada ACRILOID. Avui en dia el B-72 és fa servir habitualment en tasques de restauració com adhesiu i en molts processos.

#### Presentació

Es presenta en forma sòlida i dissol en toluè, xilè, etanol, dimetilformamida, benzè, acetona,... 

#### Característiques

És la seva solubilitat en diferents disolvents orgànics, la seva resversibilitat i la seva capacitat de no esgrogeeir.  

#### Aplicacions 

Capes de protecció per a metallls, fixació de capes pictòriques, consolidació de fusta, reversible sempre amb el mateix dissolvent que s'ha dissolt.

### PARALOID B-44

És una resina molt dura i només soluble amb toluè + metílcelosolbe. 

#### Presentació

Es presenta en forma sòlida. 
Soluble en toluè+metilcelosolbe. 

#### Aplicacions

- És la millor resina acrílica per a capes de protecció de metalls. 
- És la resina que millor aïlla l'objecte de l'entorn, tant interior com exterior.


### PARALOID B-67

- Es presenta en forma sòlida. 
- Soluble en White Spirit i Etanol. 
- Es pot fer servir en alta concentració amb càrregues com serradures per a reintegrar fissures i orificis en fusta.

### PLEXIGUM N-80 

Es presenta en forma sòlida. 
- Soluble en toluè, xilè, tricloretilé, etanol. 
És semblant al PARALOID-B44 per a protecció de metalls.

### PRIMAL AC-33 

- Aquesta resina es presenta en dispersió aquosa. 
- Un cop seca no es reversible en aigua si no en toluè o xilè. És un consolidant molt important en arqueologia. Idoni per peces que al extreure de terra i que presenten humitats.


### PLEXTOL B500

- Es fa servir de consolidador pictòrica
- Molt usat al nord d'Europa i menys al sud.
- Es presenta en dispersió aquosa.
- Derivat de la copolimerització del metil metacrilat amb e etilacrilat.
-Al 49-50% de contingut sòlid es fa servir també en restauració pictòrica per amorteir axecaments i bonys.

### PLEXISOL P550

- Es fa servir de consolidador
- És una solució acrílica copolímera de baix pes molecular.
- És soluble en ésters, cetones, disolvents aromàtics i de manera limitada, en alcohol.
- Es pot fer servir en concentracions de 5 a 25% en white spirit per consolidar capa pictòrica.
-Com adhesiu en teles d'alta penetració a temperatures per sota dels 50C

### LASCAUX. 
Té una amplia gama de productes i consolidants fets amb resines acríliques.



[+Wikipedia](https://ca.wikipedia.org/wiki/Resina_acr%C3%ADlica#cite_note-1)