---
title: "Perill per la salut: Sensibilitzants, carcinògens, mutagens, tòxics per la reproducció, tòxics en organs"
tags: [riscos laborals, etiquetat]
summary: "És un grup que agrupa molts productes i mescles tòxiques: Sensibilitzants, carcinògens, mutagens, tòxics per la reproducció, tòxics en organs"
sidebar: riscos_laborals_sidebar
permalink: perill_per_la_salut.html
---

{% include precaution.html content="Perill de toxicitats de divesos tipus" %}

![Pictograma toxicitat diversa](https://www.upc.edu/prevencio/ca/seguretat-higiene/arxius/pictogrames-pq/ghs08_perill-per-a-la-salut.png)

El pictograma es fa servir per molts tipus de toxicitats


## Sensibilització respiratòria o cutània

Els **productes sensibilitzants** són aquells que per inhalació o penetració cutània poden ocasionar una reacció d’hipersensibilitat, de manera queuna exposició posterior a aquest producte doni lloc a efectes negatius característics.

{% include note.html content="Quan es treballa amb sensibilitzants, pràcticament no hi ha cap concentraciósegura, l’única mesura preventiva és evitar el contacte dels individus sensibilitzats amb aquests productes."%}

| Frase | Paraula
| --- | --- |
| H334: Puede provocar síntomas de alergia o asma o dificultades respiratorias en caso deinhalación | Peligro

## Mutàgens en cèl·lules germinals

Els termes “mutagènic” i “mutàgen” s’utilitzen per a designar aquells agents que augmenten la freqüència de mutació en les poblacions cel·lulars, als organismes o en tots dos.Existeixen dues categories.

Hi han dos categories: La 1 pertany als productes que estpa demostrat que són mutàtens i la 2 pertanys als productes sospitosos.

| Categories | Frase | Paraula
| --- | --- | --- |
| cat 1a i 1b | H340: Puede provocar defectos genéticos | Peligro
| 2 | H341: Se sospecha que provoca defectosgenéticos | Atención


## Carcinògens

Els productes cancerígenssón aquells que poden produir càncer o augmentar-ne la freqüència.

Existeixen dues categories, en funció de les proves existents.  Lac ategoria 1 inclou les substàncies de les quals se sap o es considera que indueixen càncer.Dintre de la categoria 1, es distingeixen la categoria 1A i la categoria 1B, en funció dels estudis o assajos disponibles. Els de la categoria 1A existeixen provesen humans, per exemple a partir d’estudis epidemiològics en humans. Els de lacategoria 1B hi ha resultats positius en proves en animals. A la categoria 2 pertanyen les substàncies que són motiu de preocupació perquè poden provocar càncer, en funció de proves i assajos existents, que no són prou convincents com per a incloure’ls a les categories 1A o a la 1B.

| Categories | Frase | Paraula
| --- | --- | --- |
| cat 1a i 1b | H350: Puede provocar cáncer | Peligro
| 2 | H351: Se sospecha que provoca cáncer | Atención

## Tòxics per a la reproducció

Els productes tòxics per a la reproducciósón aquells que poden produir efectes negatius no hereditaris en el desenvolupament dels éssers humans o augmentar la seva freqüència, o afectar de manera negativa la funció o la capacitat reproductora.

Tenim dos tipus de productes tòxics per la reproducció, en funció dels efectes queprodueixen:
- Efectes adversos sobre la funció sexuali la fertilitat.
- Efectes adversos sobre el desenvolupament dels descendents.

| Categories | Frase | Paraula
| --- | --- | --- |
| cat 1a i 1b | H360: Puede perjudicar a la fertilidad o dañar elfeto | Peligro
| 2 | H341: Se sospecha que puede perjudicar a lafertilidad o dañar el feto| Atención

## Toxicitat específica en determinats òrgans (STOT). Exposició única

S’entén per toxicitat específica en determinats òrgans (exposició única) la toxicitatno letal que es produeix en determinats òrgans després d’una única exposicióa una substància o barreja. 

Aquests efectes poden afectar el funcionament o la morfologia d’un teixit o òrgan,o poden provocar alteracions importants de la bioquímica o l’hematologia del’organisme.

| Categories | Frase | Paraula
| --- | --- | --- |
| cat 1| H370: Perjudica a determinados órganos | Peligro
| cat 2 | H371: Perjudica a determinados órganos | Atención
| cat 3 | H335: Puede irritar a las vías respiratorias. Puede provocar somnolencia y vértigo | Atención

## Toxicitat específica en determinats òrgans (STOT). Exposicions repetides

S’entén per toxicitat específica en determinats òrgans (exposicions repetides) la toxicitat no letal que es produeix en determinats òrgans després de diverses exposicions a una substància o barreja. 

A la indicació de perill de les categories 1 i 2 cal indicar la via d’exposició si s’ha demostrat que el perill no té lloc per cap altre via. També s’han d’indicar els òrgans afectats, si es coneixen.

| Categories | Frase | Paraula
| --- | --- | --- |
| cat 1| H372: Perjudica a determinados órganos por exposición prolongada o repetida | Peligro
| cat 2 | H373: Perjudica a determinados órganos porexposición prolongada o repetida | Atención

## Perill per aspiració

Per «aspiració» s’entén l’entrada d’una substància o d’una mescla, líquida o sòlida,directament per la boca o el nas, o indirectament per regurgitació, a la tràquea oen les vies respiratòries inferiors.

{% include note.html content="La toxicitat per aspiració pot comportar greus efectes aguts com ara pneumònia química, lesions pulmonars més o menys importants i fins i tot la mort per aspiració."%}

| Frase | Paraula
| --- | --- |
| H304: Puede ser mortal en caso de ingestión ypenetración por las vías respiratorias | Peligro



