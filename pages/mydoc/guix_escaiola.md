---
title: "Guix"
tags: [escaiola]
summary: "L'escaiola és un producte industrial que s'obté del guix natural"
sidebar: mydoc_sidebar
permalink: guix_escaiola.html
---

L'escaiola és un guix d'alta qualitat i gra molt fi, amb puresa major.
La font d'obtenció és [l'alabastre](https://ca.wikipedia.org/wiki/Alabastre_(mineral))

## Escaiola de guix

Es fa servir barrejant aigua amb sulfat de calci hemidratat:CaSO4·1/2H2O. Es produeix escalfant el guix a uns 150 °C.
2 CaSO4·2H2O → 2 CaSO4·0.5H2O + 3 H2O (alliberat com vapor). A París, Montmartre, hi ha un gran jaciment de guix.

## Escaiola de calç

Aquesta és una barreja d'hidròxid de calci i sorra (o altres d'inerts). El diòxid de carboni de l'atmosfera causa la transformació de l'hidròxid de calci en carbonat de calci.

Per produir escaiola de calç les pedres de carbonat de calci són escalfades per produir òxid de calci (cal viva) i quan s'afegeix aigua es torna hidròxid de calci (cal apagada) que es ven en forma de pols blanca. S'ha d'afegir més aigua per usar l'escaiola de calç en forma de pasta.

L'escaiola de calç es fa servir en els autèntics frescos (pictòrics). Els pigments, s'apliquen diluïts en aigua quan l'escaiola encara està fresca.

## Escaiola de ciment

L'escaiola de ciment és una barreja d'escaiola, sorra, ciment Portland i aigua. S'aplica en la construcció en interiors i exteriors per obtenir una superfície suau. Diverses escaioles basades en ciment es fan servir en productes contra el foc. Normalment es fa servir vermiculita com agregat lleuger de pes.



