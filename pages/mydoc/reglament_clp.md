---
title: "Reglament CLP"
tags: [riscos laborals]
summary: "És la legislació bàsica sobre classificació, etiquetatge i envasat de productes químics. CLP són sigles que provenen de l’anglès: C, Classification (Classificació); L, Labelling(Etiquetatge); P, Packaging(Envasat)"
sidebar: riscos_laborals_sidebar
permalink: reglament_clp.html
---

Aquest reglament és una adaptació del sistema GHS de Nacions Unides (GHS són les sigles que provenen de l’anglès GHS: Globally Harmonized System, Sistema Globalment Harmonitzat) que té com a objectiu unificar la classificació i etiquetatge dels productes químics a nivell mundial, ja que s’havien trobat diferències en l’etiquetatge d’una mateixa substància en funció del país que realitzava la classificació.

Fins fa poc, la mateixa substància podria ser no perillosa a la Xina,nociva per ingestió a Europa i molt tòxica per ingestió als Estats Units. 

{% include note.html content="L’objectiu és que tots els països acabin adoptant el GHS i una substància tingui la mateixa classificació i etiquetatge independentment del país de procedència" %}

## Legislació 

El Reglament (CE) n.º 1272/2008 («Reglament CLP») alinea la legislació anterior de la UE amb el SGA (Sistema Globalm Armonitzat de Classificació i Etiquetat de Productes Químics), un sistema de les Nacions Unides per identificar productes químics perillosos i informar a les empreses/persones usuaries sobre aquests perill. També hi han enllaços a la legislació REACH. El [Reglament CLP](http://eur-lex.europa.eu/JOHtml.do?uri=OJ%3AL%3A2008%3A353%3ASOM%3AEN%3AHTML) va entrar en vigor el 20 de gener de 2009 i va substituir a les Directives sobre classificació i etiquetat de substàncies perilloses (67/548/CEE) i sobre preparats perillosos (1999/45/CE). Amdues directives va ser derogades el 1 de juny de 2015.

- [Reglament (CE) n.º 1272/2008](http://eur-lex.europa.eu/legal-content/EN/ALL/;ELX_SESSIONID=rhdpJK6bQWJRP0fjDyyQQMtlbv3B2py8nJDYSyfgLsbLpmLfP8Xq!1095794860?uri=OJ:L:2008:353:TOC) 
- [Legislació REACH](https://osha.europa.eu/es/themes/dangerous-substances/reach)

El [SGA](http://www.unece.org/trans/danger/publi/ghs/ghs_welcome_e.html) ha sigut adoptat per molts paisos de tot el món i ara es fa servir també com base per la reglamentació del transport internacional i nacional de mercaderies perilloses.

{% include important.html content="Els perills dels productes químics es comuniques a través d'indicacions i pictogrames normalitzats en les etiquetes i les fulles de dades de seguretat." %}

Els nous pictogrames enmarcats en vermelll reemplacen els coneguts símbols de perill taronges.

![Pictogrames CLP](https://casalista.com/wp-content/uploads/2018/09/2-que-significan-los-simbolos-de-peligro-de-productos-quimicos.jpg)

<iframe width="660" height="415" src="https://www.youtube-nocookie.com/embed/sC5IIStp3GA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
