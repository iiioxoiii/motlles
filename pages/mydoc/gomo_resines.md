---
title: "Gomo Resines"
tags: [ resines naturals ]
summary: "Són mescles naturals de goma i resina que amb l'aigua forma una emulsió. "
sidebar: mydoc_sidebar
permalink: gomo_resines.html
---

S'anomenen també bàlsams. Poden contenir olis essencials o no

## Bàlsam elemí

S'obté d'un arbre tropical anomenat cabanarium. És d'aspecte viscós i lletós i fa olor a fonoll. Primer es va fer servir per a la medicina.
- S'ha fet servir com adhesiu de fibres de cotó.
- Es pot mesclar amb olis, resines, i ceres.
- És soluble amb etanol i hidrocarburs aromàtics.
 



