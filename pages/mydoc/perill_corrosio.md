---
title: "Corrosió"
tags: [riscos laborals, etiquetat]
summary: "Elsproductes corrosius són aquells que en entrar en contacte amb teixits vius provoquen la seva destrucció."
sidebar: riscos_laborals_sidebar
permalink: perill_corrosio.html
---

{% include precaution.html content="Perill per substàncies i barreges corrosives" %}

![Pictograma Corrosió](https://images-na.ssl-images-amazon.com/images/I/618TblVprQL._SL1200_.jpg)

## Corrosius a teixits 

- Corrosió cutània és l’aparició d’una lesió irreversible a la pell, a través de l’epidermis que arriba a la dermis, com a conseqüència de l’aplicació d’una substància d’assaig durant un període de fins a 4 hores. 
- Les reaccions corrosives es caracteritzen per úlceres, sagnat, i després d’un període d’observació de 14 dies, per decoloració deguda al blanqueig de la pell, zones completes d’alopècia i cicatrius.

| Frase | Paraula
| --- | --- |
H314: «Provoca quemaduras graves en la piel y lesiones oculares graves» | Peligro |
H318: «rovoca lesiones oculares graves» | Peligro |

### Lessió Ocular

La lesió ocular greu és un dany als teixits de l’ull o deteriorament físic importantde la visió, com a conseqüència de l’aplicació d’una substància d’assaig a la superfície anterior de l’ull, no completament reversible als 21 dies següents de l’aplicació.

### Irritació Ocular

Irritació ocular és la producció d’alteracions oculars com a conseqüència de l’aplicació d’una substància d’assaig en la superfície anterior de l’ull, totalment reversible als 21 dies següents a l’aplicació.

## Corrosius de Metalls

Una substància o barreja és corrosiva per als metalls quan, per la seva acció química, poden fer malbé o fins i tot destruir-los. 

| Frase | Paraula
| --- | --- |
H290: «Puede ser corrosivo para los metales» | Atención |




