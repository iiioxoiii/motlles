---
title: "Engrut"
summary: "L'engrut és un material adhesiu que s'utilitza per enganxar cartells o paper pintat i realitzar obres d'artesania amb paper i cartró o paper maixé. L'engrut és un material barat, fàcil de fabricar i que es conserva molt de temps pel que és molt indicat per a les aplicacions casolanes."
sidebar: mydoc_sidebar
permalink: engrut.html
---
## Fabricació  

Per a la fabricació de l'engrut més senzill només s'empren l'aigua i la farina. La farina pot ser de blat, de sègol, d'arròs...

### Recepta de Juan Carlos Banacloy 

- 100gr de farina.
- 600ml d'aigua.
- 5gr de sulfat de coure.

«Ponemos a hervir tres unidades de agua mientras disolvemos una proporcion de harina en una de agua. Cuando llegue a ebullicion apagamos el fuego y vertemos la harina, un chorrito de cola de carpintero y una cucharadita de piedra lipi (azulete), es decir, sulfato de cobre, que actúa como fungidida y remolvemos la mezcla»

*Recepta donada a Àlgela Grau González per Juan Carlos Banacloy en una entrevista en desembre de 2017*

### Tècnica japonesa per Berta Blasi

- 1 part de midò d'arròs.
- 3 parts d'aigua.

«Es col·loca el NORIKOSHI (sedàs) dins del NORIBON (recipient de bambú) i, amb l’espàtula de bambú, es pressiona l’engrut per fer-lo passar pel tramat de pèl de cavall del sedàs fent-lo caure dins del noribon, (repetint el procés de tres a set vegades) així els grumolls resultants de la cocció del midó són filtrats donant lloc a una massa més pura.
El següent pas és amassar l’engrut tamisat amb la brotxa SHIGOKEBAKE, separant les partícules de l’engrut i trencant la consistència gelatinosa. Un cop amassat una mica l’engrut, s’hi afegeix una mica d’aigua i amb la shigokebale mateix s’emulsiona fins a trobar la textura adequada: més espessa per encolar o més líquida per laminar. S’ha d’anar amb compte en aquest procés i afegir l’aigua de mica en mica per tal que la massa la pugui absorbir, si no fos així es correria el risc de provocar aurèoles al paper al aplicar-hi la cola per consolidar»

[+info](https://bertablasi.com/).*Berta Blasi.Conservadora i Restauradora.*

### Recepta per enganxar etiquetes sobre vidre, metall o ceràmica

Es realitzava una barreja de midó de blat, goma aràbiga, sucre i aigua. Primer, es dissol la goma i el sucre en l'aigua. Després, s'hi afegix el midó mentre es bull cinc minuts.