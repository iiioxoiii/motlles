---
title: "Metall"
tags: [materials]
summary: "Les caracterísitiques dels metalls són qualitats o defectes depenent de la nostra necessitat. En tot cas són constants físiques."
sidebar: mydoc_sidebar
permalink: metall.html
---

En una classificació des de l'aspecte de fabricació mecànica es poden establir tres grups de propietats: les propietats físiques, les propietats químiques i  les propietats mecàniques.

### Propietats Físiques

#### Fusibilitat

Propietat que tenen els metalls en licuar-se (passar d'estat sòlid a líquid) amb l'acció del calor. És important tenir present aquesta propietat en les peces fetes per fosa o per dissenyar
peces per treballs a altes temperatures.

#### Calor específic

La quantitat de calor necessàri per augmentar la temperatura de la unitat de massa d'un cos des de 0 fins 1ºC. S'expresa en calories/gram. El seu valor és molt important per fer càlculs
de calor aportat per fondre o deformar cert material.

#### Temperatura de Fusió

La temperatura del metall a canviar d'estat sòlid a líquid.

#### Dilatabilitat

La propietat d'augment de volum per efecte de la calor. Es freqüent expressar propietat de dilatació amb un coheficient de dilatació lineal al elevar-se un grau celsius de temperatura.

#### Conductivitat tèrmica

La propietat de transmissió de calor a través de la seva massa.

#### Conductivitat elèctrica

És una propietat gairebé exclusiva dels metalls de desplaçament d'electrons per la seva massa. La inversa de la seva conductivitat és la resistència elèctrica. 

#### Cohesió

És la propietat de força d'atracció entre les molècules del material.

#### Porositat

La propietat que presenten alguns materials de permetre el pas de líquits i gasos pel seu interior.

#### Magnetisme

Propietat d'alguns materials d'atreure el ferro.

### Propietats Químiques

#### Oxidació

#### Corrosió

### Propietats mecàniques

#### Tenacitat

#### Elasticitat

- Plasticitat

- Ductilitat

- Fatiga

#### Resistencia a la rotura

- Tracció
- Compressió
- Torsió
- Cisallament
- Flexió
- Convinats
- Estricció
- Duresa
- Fragilitat
- Resiliencia
- Fluència
- Maquinabilitat
- Porositat  

## Estructura

La unió dels materials metàl·lics es pot trobar de dues maneres. 

### Estructura cristalina

Es conforma a partir d'una estructura ordenada i geomètrica a l'espai dels àtoms metàl·lics. La forma i la duresa en la unió d'aquesta estructura determinarà el comportament posterior
del material, pel que l'estudi d'aquestes estructures es el punt d'inici per aconseguir materials amb característiques concretes.

### Estructura granular

És distribució dels àtoms de manera desordenada. En els metalls una estructura granular es pot determinar en zones que poden oscilar entre els 0,02 als 0,2 mm. 
En línies generals, a més fi el gra millors propietats mecàniques. 

