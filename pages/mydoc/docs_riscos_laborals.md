---
title: "Documentació Riscos Laborals"
tags: []
summary: "Recursos imprimibles sobre Riscos Laborals" 
sidebar: mydoc_sidebar
permalink: docs_riscos_laborals.html
---
 


PDF - Anglès. «Chemicals at Work-a new labeling system»
Guidance to help employers and workers
to manage the transition to the new classification, labelling and packaging system.
[download](https://tienda.melcomposites.es/)

PDF CFGS - Prevenció de riscos professionals (IOC)
- Riscos químics i biològics ambientals.
- La higiene industrial. Els contaminants químics.
- Generació dels contaminants químics.
- Avaluació de l’exposició a contaminants químics.
- Quantificació d’agents químics.
- Quantificació d’agents químics: els sistemes de presa de mostres i anàlisi.
- Prevenció i protecció del risc químic.
- Riscos biològics.
- Prevenció i protecció de riscos biològics.

