---
title: "Ceres minerals"
tags: [ceres, ceres minerals]
summary: "Són d'origen fòssil de l'era terciària i s'obtenen de carbons, de torbes, de lignits i betums.  Són hidrocarburs sòlids i són ceres molt pesades i molt fosques."
sidebar: mydoc_sidebar
permalink: ceres_minerals.html
---
 
## Cera Montan

Es coneix des de el s.XIX i s'obté del lignit. Des de 1928 mitjançant un tractament químic s'ha obtingut una cera de molt bona qualitat HOECHTS que té el seu origen....

- És fosca però molt dura.
- Fon a 80ºC .
- Dissol en trementina, xilè i toluè.
 
## Cera ozocerita

S'extreu a llocs propers de jaciments petroliers. És de color fosc. S'anomena també cera de terra. Resisteix àcids i alcaids i mitjançant un tractament químic d'oxidació perd el color fosc i passa a tenir un color groguenc. Es converteix en ceresina.
 
## Cera ceresina

És groguenca i té el punt de fusió de 65 a 80ºC.
- Dissol en xilè i toluè.
- Es fa servir per a la conservació de la fusta i pot arribar a substituir la cera d'abelles.
 
## Parafina

S'obté de la destil·lació del petroli i és la cera més inert de totes les ceres vegetals i animals. És una barreja d'hidrocarburs. De la parafina líquida s'anomena vaselina.

- Fon dels 50º als  80º.
- És més transparent que la cera d'abella blanca apareix a meitat del segle XIX i avui en dia es fabrica sintèticament.
- La cera sintètica fon als 50º.
- Dissol en white spirit. Serveix per a capes de protecció de metalls (avui ja no es fa servir).
 
## Cera Microcristal·lina

Es presenta en forma de gotes blanques i és un derivat de la parafina. És fabrica des de 1935.

- És més adhesiva que la parafina però menys brillant.
- Fa pel·lícules molt elàstiques,flexibles i resistents. Aguanta bé les temperatures baixes.
- És transparent.
 
## COSMOLLOID 80H

- Dissol en toluè, xilè o white spirit. 
- Fon entre 65 i 85º depenent del tipus.
- Es fa servir per protegir metalls.
 
