---
title: "Termoplàstics (derivats de l'elilè)"
tags: [materials plàstics, ceres, polímers]
summary: "Els termoplàstics a temperatura ambient són plàstics rígids, que en augmentar la temperatura es tornen tous i mal·leables, retornant a un estat sòlid al refredar-se. Històricament, els termoplàstics eren les ceres."
sidebar: mydoc_sidebar
permalink: termoplastics.html
---

## Propietats

Són materials que mantenen les seves propietats malgrat s'escalfin i solidifiquin diverses vegades.

Aquests plàstics mantenen les seves propietats malgrat hagin estat escalfats i modelats diverses vegades i sense canviar les
seves propietats i a diferència dels plàstics termoestables que en augmentar la temperatura es cremen, resultant impossibles de tornar a modelar.

{% include important.html content="Són soldables." %}

## Estructura

En forma de polímers amorfs i semicristal·lints, és a dir, un polímer que te una regió cristal·lina i una amorfa. Es componen de macromolècules
lineals o ramificades, s'estoven amb l'escalfament de forma repetible fins la fusió i es solidifiquen per refredament.
 
## Tipus de termoplàstics

### Produits en sèrie

- El [polietilè](https://ca.wikipedia.org/wiki/Polietil%C3%A8)(PE)
- El [poliestirè](https://ca.wikipedia.org/wiki/Poliestir%C3%A8)(PS)
- El [polimetilmetacrilats](https://ca.wikipedia.org/wiki/Polimetilmetacrilat)(PMMA)
- El [clorur de polivinil dur o tou](https://ca.wikipedia.org/wiki/Clorur_de_polivinil)(PVC-R, PVC-P)
- El [tefló o politetrafluoretilè](https://ca.wikipedia.org/wiki/Politetrafluoretil%C3%A8)(PTFE)
- El [polipropilè](https://ca.wikipedia.org/wiki/Polipropil%C3%A8)(PP)
- El [policarbonat](https://ca.wikipedia.org/wiki/Policarbonat)(PC)
- El [politereftalat d'etilè](https://ca.wikipedia.org/wiki/Politereftalat_d%27etil%C3%A8)(PET)
- El [polimetracrilat de metil](https://ca.wikipedia.org/wiki/Polimetilmetacrilat)(PM-MA-vidre acrílic, plexiglàs)
- El [politetrafluoretilè](https://ca.wikipedia.org/wiki/Tefl%C3%B3)(PTFE-tefló)

### Tècnics

- Les[poliamides](https://ca.wikipedia.org/wiki/Poliamida) (PA)
- El [polioximetilé](https://es.wikipedia.org/wiki/Poliacetal) (POM)

### Polímers d'alt rendiment

- La [poliariletercetona](https://en.wikipedia.org/wiki/Polyaryletherketone)(PAEK)
 
## Les Ceres

Les ceres són considerats materials termoplàstics.

