---
title: "Alumini"
tags: [metall, escultura, construcció]
summary: "Element químic de símbol Al, del grup 13 de la taula periòdica, de número atòmic i3 i massa atòmica 26,981."
sidebar: metalls_sidebar
permalink: alumini.html
---

És un element grisós, molt tou (per això es fan aliatges) i lleuger.  Químicament és molt electropositiu pel que representa una reactivitat elevada.
En contacte amb l'oxigen atmosfèric reacciona suaument i es torna passiu. És estable en contacte amb l'aigua, però reacciona
suaument amb presència de bases i àcids no oxidants. Té 15 isòtops de vida mitjana coneguda, de ells un és estable, el Al-27.
S'obté bàsicament amb el procés Hall-Héroult.

L'alumini es fa servir de manera intensiva en l'obtenció d'aliatges especials, en la indústria alimentícia i elèctrica.

## Propietats

- Densitat 2,70gr/cm2
- Resistencia 20 kg/mm2
- Un punt de fusió baix (658ªC)
- Elevada conductivitat tèrmica
- Elevada capacitat d'oxidació

## Compostos

- Òxid d'alumini Al2O3: Sólid ceràmic blanc de caràcter anfòter de punts d'ebullició i fusió molt alts. Es troba en forma de corindó i bauxita a la natura. Es fa servir per aïlaments tèrmics i elèctrics, com abrasius, en cromatografia, en joieria i òptica.

- Hidròxid d'alunmini Al(OH)3: Alúmina hidratada. És un pol de color blanc que es descomposa amb alúmina i aigua al escalfar-lo. Es fa servir com absorvent en resines d'intercanvi iònic i en la indústria paperera, detergents i ceràmiques.

- Clorur d'alumini AlCl2: És de color blanc i força reactiu que es fa servir com catalitzador en la indústria petroquímica, en la farmacèutica i en la metalúrgica.

## Aliatges

Els aliatges d'alumni es divideixen en dos grups: forjables i fundicions. Els elements que es dissolen amb l'Al i amb diferents proporcions són el Fe, Si, Mn, Mg, Zn, Cu, Li. (Fig.3.24) 
El coure dona resistènia mecànica, duresa i facilita el mecanitzat. El magnèsi millora la ductilitat i resistència a impactes. El manganés augmenta la resistència i duresa. El silíci
rebaixa el punt de fusió i millorar la colabilitat. El zinc reforça duresa i resistència.

### Alumini forjable

Són aliatges que augmenten la resistència mecànica i permeten la fabricació de peces o components per procediments mecànics com la forja, laminació, trefilat.
Generalment admeten tractactaments tèrmics que modifiquen les seves propietats però hi han que no. Per això es poden dividir en dos grups.

- Aluminis forja bonificables: Aliatges d'alumnini que accepten tractaments tèrmics per augmentar resistència i duresa. 
- Aluminis forja no bonificables: Aliatges d'alumini que no accepten tractaments tèrmics. Només poden ser treballades en fred per augmentar la seva resistència. 

### Alumini per fosa

Són els que es fan servir per fer peces amb motlles. L'aliatge per aquest tipus de peces conté Si, Mg Zn i Cu.

Els procediments habituals són:

- Colada en sorra
- Fundició inyectada
- Fundició per gravetat (cast.coquilla).

## Usos

Es fa servir molt industrialment i ha substituit moltes peces que es fabricaven amb acer. Això es degut a que:

- L'alumini és un terç més lleuger que l'acer.
- És molt fàciment reciclable. En comparació amb l'obtenció de l'alumini primari, el reciclat costa un 95% menys d'energia.
- Facilitat de mecanitzat per arrancament de viruta.
- Material no tòxic.
- Bona resistència, però no tanta com l'acer.
- Bona mal·leabilitat.
- Bona capacitat de moldejat.

## Soldadura

Per fer una soldadura elèctrica amb electrodes revestits cal que els electrodes tinguin una capa de components especial per poder dissoldre els òxids que es formen a la superfície (òxid d'alumini o alumina) i que tenen una elevada temperatura de fusió (2050ºC) i que eviten la formació de nous òxids.

També s'acostuma a fer servir un arc elèctric de corrent alterna per solventar els òxits.

## Formes comercials

- Perfils comercials: Passamans, barres de secció quadrada, rodona, hexagonal i extrussions de perfils amb formes diverses.
- Tubs: Seccions quadrades, rodones, extruïts en formes diverses.
- Xapa: Llises, perforades o gravats.


