---
title: "Papir"
tags: [suport cal·ligràfic]
summary: "El papir més antic conegut està en blanc, i fou descobert a la tomba de Hemaka, el visir del faraó Den, de la Dinastia I, cap al 3000 aC, a la necròpoli de Saqqara. Gràcies als manuscrits sobre papir es coneixen molts textos de l'antiguitat, sobretot de l'antic Egipte, així com altres textos bíblics. Aleshores es feia servir una tinta elaborada a partir de negre de fum, cola, aigua i oli de cedre."
sidebar: mydoc_sidebar
permalink: papir.html
---
 
## Origen

S'obté d'una planta herbàcia perenne de la família de les ciperàcies. En concret de la **cyperus papirus**. Es tracta d'un jonc que creix en llocs humits i fangosos com són les ribes dels rius. La planta té de 3 a 5 metres d'alçada i creix a Egipte, Palestina, Síria i Sicília.

## Història

Abans del papir hi com a suport s'escriptura existien les tauletes de fang del sumeris on s'escribia per inscisió. També exisia el bambú del Xinesos i les fulles de palmera dels filipins. El papir apareix 3000 a.C. A l'antic Egipte. Era reservat per a usos religiosos metre que l'us privat es seguia fent servir com a suport pedres calcàrees. En època grega es va divulgar el seu us amb el cultiu sistemàtic.

## Obtenció 

Plini ja ho va explicar La planta era tallada per les tiges en sentit longitudinal. 

Damunt d'un suport de fusta s'anaven col·locant mullades i es premsaven amb una maça de fusta. Després es polia amb un tros d'ivori fins allisar-les totalment. Aquestes tires de tiges després eren col·locades de manera entrellaçada com un teixit. L'origen està en la cistelleria.La saba d'aquesta planta té un alt contingut en midó que actua d'adhesiu de les pròpies tires. Després s'emblanquinava i assecava mitjançant l'acció del Sol.  Mes tard es va afegir un aprest amb faria i aigua. Aquest adhesiu també s'emprava per adherir els fulls entre elles i al final s'hi posava un pal de fusta anomenat umbilicum. Els volum tenien 20 fulls. A un full s'anomena plàgula i tot plegat anava dins una capsa de ceràmica o fusta anomenada capsae.

## Caracterítiques

- El papir és un suport més delicat que el pergamí o el paper.
- És hidroscòpic. 
- S'escrivia de dreta a esquerra però seguint la direcció de la fibra amb pelets de canya sucats en tinta negre de sutge aglutinat amb gòma aràbiga i aigua.
- Els títols s'escrivien en vermell. Només es podia escriure per una cara. En comparació amb els suports anteriors oferia més superfície la qual cosa afegia rapidesa en l'escriptura, era més lleuger, era més fàcil de transportar i és atact pels microorganísmes.