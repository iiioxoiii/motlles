---
title: "Resines Naturals"
tags: [materials plàstics]
summary: "Són polímers naturals. És la part que queda quan s'ha evaporat els olis esencials."
sidebar: mydoc_sidebar
permalink: resines_naturals.html
---

Tenen el seu origen als bàlsams. Són sòlides i és la part que queda quant han evaporat els olis essencials. Són d'origen vegetal. Són polímers* naturals. 

## CARACTERÍSTIQUES

- Físicament és presenten en forma sòlida però tenen forma d'escates, làmines o palets, fragments irregulars grans o petits.
- El color va del groguenc al rogenc. Són semitransparents.
- Acostumen a portar impureses d'origen orgànic que s'han d'eliminar abans de fer-les servir. Una manera de treure les impureses és posar la resina dins una mitja i submergir-la amb dissolvent dins d'un recipient ceràmic durant 24 hores.
- No dissolen en aigua.
- Si s'apropen al foc és fonen i si es mosseguen es trenquen.
- Es mesclen en forma de dissolució
Aplicacions
- Fer vernissos, adhesius o aglutinants.
- Majoritàriament dissolen en dissolvents orgànics.
 
## Classificació de resines

### Vegetals i animals (resines toves)
- Màstec o Màstic
- Dammar
- Colofònia
- Gomalaca
- Sandàraca
 
### Fòssils (dures)
- copals
- ambre
- copal de manila
 
 
## Màstec o Màstic

S'obté de l'arbre de la pistàcia lentinscus o el llentiscle. Es troba sobretot a Grècia, Àfrica oriental i l'Índia, i a la franja mediterrània. És el més utilitzat a Espanya.

### Caracterítiques

- Es presenta en forma de llàgrimes de color groguenc clar.
- Dissol en trementina, alcohol etílic o etanol o white spirit.
- S'ha fet servir per treure vernissos, per fer massilles per subjectar vidres ó per mastegar.
- Fa capes molt elàstiques, molt brillants amb aspecte d'esmalt.
- Amb el temps engrogueix molt i es torna poc reversible.
- Amb presencia d'humitat es pot esbalair i agafar color propi.
- Pot ser que es torni blanquinós, blavós o verdós.
 
## Dammar

S'obté de diferents arbres tropicals que viuen a l'Índia, a les illes Sudaic (de l'Oceà Índic), a Indonèsia, a les illes de Java i Sumatra. Es fa servir des del s. XIX. Un arbre dona uns 5 kg. De resina a l'any.
El dammar de millor qualitat es el de Batavia (ciutat de Java). Es presenta en forma de grumolls, fragments, groguencs i d'aspecte enfarinat
- Es la resina que engrogueix menys i la més estable.
- És fàcilment reversible i és el vernís més emprat en restauració.
- Dissol en trementina i white spirit. El vernís adquireix un aspecte tèrbol però que no afecta el color.
- És fan fixatius (en esprai) per tècniques sobre paper.
- Com adhesiu en re-entelaments.
- Com a consolidat en fustes mullades.
- Ha substituït a màstec.
 
## Sandàraca

S'obté de diferents coníferes. Una d'elles és la tetraclinis articulata o thuya articulata . L'arbre es troba al Marroc, Algèria i a Austràlia. És l'alerç africà. S'ha fet servir molt des del s. XVI fins al XVII. Es fa servir en productes farmacèutics.

Es semblant al dammar. Es presenta en palets o llàgrimes de color groguenc al rogenc.
- És la resina més clara.
- Dissol en alcohol etílic o etanol, i acetona.
- S'ha emprat com a vernís per a metalls en les metal·litzacions barrejat amb oli de llinosa i colofònia.
- És molt dur
- A la llarga enfosqueix molt.
 
## Colofònia

El nom té el seu origen en la ciutat de Colofón on antigament s'obtenia. Té altres nom populars com la pega grega o la resina de violinista. És un subproducte del procés de destil·lació de la trementina. Es presenta en fragments punxeguts i vidriosos.

- No es fa servir però s'ha fet servir molt. El seu us en tècniques artístiques, fabricació de paper i en restauració ha sigut molt negativa.
- Molt usada per adulterar d'altres resines més cares.
- Per vernissos sobre fusta.
- Fixatius per suport de paper.
- En la indústria paperera com aglutinant i adhesiu.
- En reentelatges a la cera-resina.
- Es soluble en trementina,etanol i olis.
- Engrogueix molt.
- És molt barata.
 
## Goma Laca

S'obté d'un insecte paràsit que viu en forma de colònies damunt les branques de diferents arbres resinosos. Es coneix des de l'edat mitjana. L'insecte és un “hemipter” “cocca lacca” “kerria lacca” “laccifer lacca”. La femella de l'insecte col·loca a les branques joves els seus ous mitjançant la seva trompa a les tijes, xuclant la saba que transforma en una secreció resinosa que acaba d'embolcallar tota la branca formant-se una crosta que té per funció protegir les larves. És presenta en forma d'escates. No porta impureses i no cal filtrar-la.

### Característiques

- Només dissol en etanol.
- Hi han diferents tipus:
- Natural: Porta cera incorporada o no.
- Decolorada: Sense color però engrogueix.
- Orange: És de color ataronjat. Porta colorant natural del propi insecte
- No és tòxica
- No serveix d'aglutinant*.
- A partir dels 25 anys no és reversible.
- Es fa servir per fer vernissos per metal·litzacions.
- Com a impermeabilitzants de materials porosos (aïllaments de motlles en reproduccions).
- Per fer fixatius en tècniques sobre paper amb espai.
- En poliments per a fusta de mobles.
- És l'acabat més car i de millor qualitat.
- Aplicació a la monyeca
- Amb presència d'humitat es pot esbalair o entelar
- Amb el temps es fa fosca, engrogueix i es torna irreversible.
 
 
## Els copals

Són vegetal s'obtenen de diferents arbres i plantes lleguminoses (que tenen beines). Dins aquests copals hi han les resines fòssils que són les més dures. Són fragments que han quedat soterrats en els boscos enfonsats (boscos prehistòrics) i es troben en  zones pantanoses, mines de carbó, i a les platges dels mars del nord els dies de turmenta. La seva datació seria de 30 a 50 milions d'anys.

També provenen de Sierra Leone, Angola, i el Congo. Allà s'anomena Ivori negre.
Aquestes resines són tant dures que per fer-les servir només es poden desfer amb olis a altes temperatures.

Tenen el punt punt de fusió molt alt. D'aquí s'obté un vernís que és molt fosc de color i oliós que dona un acabat molt dur i brillant

- Donen aspecte brillant.
- Són molt dures.
- Es fonen amb olis molt calents.
- S'anomenen vernissos cuits
- Són irreversibles.
- Crivellen molt i avui no es fan servir.
 
## Copals nous

Són resines dures però no com els copals fòssils. Són els que es poden comprar i són fragments de resina que es troben a prop de les arrels de l'arbre viu. 

Provenen de l'India, Brasil i Mèxic
 
## Ambre (fosil)
 
El terme prové de la paraula àrab hambar i en llatí succinum (suc de planta). S’obté del Pinus Succifera. És una resina fòssil dura. Prové dels boscos esfondrats que existien 40 mil·lions d’anys al nord d’Europa en la zona d’Escandinàvia (mar del nord i Bàltic). 
 
Es troba a les platges després d’una tempesta. Són de color groguenc emblanquinats o rogencs emblanquinats. En parla Plini en Vell a la seva Història Natura.

- S’ha fet servir en joieria, orfebreria. Un cop polit té un color de mel. S’han fet servir per pipes de fumar, camafeus i suport d’obres d’art com cares, mans d’escultures. En tècniques pictòriques s’ha fet servir sobretot com a vernís.
- Té el seu punt de fusió de 150 a 400º barrejat amb oli de llinosa (cal filtrar impureses).
 
Sovint s’ha falsificat amb colofònia. Té l’inconvenient de totes les resines fòssils o copals:
- amb el temps s’enfosqueix i es clivella. (l’Ambre flota a l’aigua i es pot carregar electrostàticament).
 
## Copal de Manila
 
S’ha fet servir molt per vernís paper-cartró i joguines.



