---
title: "About"
sidebar: mydoc_sidebar
summary: false
permalink: about.html
tags: []
---

El meu nom és Amós i des del meu confinament he muntat aquest site **«motlles i escultura»** per donar recursos relatius a tècniques de motlles, escultura, alguna cosa de restauració i art contemporani.   

El site està fet amb [Jekyll](https://jekyllrb.com/) i està penjat al [GitLab](https://gitlab.com/iiioxoiii/motlles). Això significa que podeu descarregar-vos el site, modificar-lo i tornar-lo a pujar en una altra plataforma o servidor web. Potser, més endavant, penjaré algun post explicant més exactament què significa fer-ho d'aquesta manera. ;)

## Continguts

El site està organitzat de manera intuitiva perquè un escultor pugui trobar la informació ràpidament. Hi ha informació de **materials** que es fan servir en escultura però també en d'altres disciplines artístiques, una part específica sobre **ríscos laborals** i **productes químics**, **receptaris**, **bibliografia** i una secció en forma de blog que ocuparà la pàgina principal on es noficaran les novetats que s'aniran incorporant al site.

Com he apuntant, el site ha sigut fet a partir del confinament per la pandèmia del COVID-19, des de l'assignatura de Motlles i Reproduccions i per cicles formatius d'escultura i ceràmica de l'escola [Llotja](http://www.llotja.cat/llotja/p/1/737/0/Inici).

Ara el deixaré un dies aparcat però espero acabar-lo i mantenir-lo. 

## Disseny 

El disseny és del [Tom Johnson](https://iiioxoiii.gitlab.io/motlles/mydoc_about.html) i funciona amb [Bootstrap](https://getbootstrap.com/) i amb [Navgoco](https://github.com/tefra/navgoco) per la barra laterial. [+info](https://iiioxoiii.gitlab.io/motlles/tag_getting_started.html).

### Dues barres laterals

Hi ha un menu principal laterial des d'on es pot anar a qualsevol recurs. Però hi ha un segon menú que apareix a l'entrar dins l'apartat de **Riscos laborals>Reglament** i on hi ha informació útil per la identificació de riscos en els productes .

<img src="{{ "images/bola_menu.png" }}" alt="Menú Riscos Laborals"/>

## Contacte

A la barra superior hi ha un apartat *Feedback* que permet enviar-me els vostres suggeriments o avisar-me d'errors (que hi ha molts) que hagueu detectat. 

<img src="{{ "images/feedback.png" }}" alt="Feedback"/>

{% include links.html %}
