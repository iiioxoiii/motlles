---
title: "Escumes de Poliuretà"
tags: [materials plàstics, polímers, escuma]
summary: "L'escuma de poliuretà és un material plàstic porós format per una agregació de bombolles, conegut també pels noms col·loquials de gomaespuma a Espanya o gomapluma en alguns països sud-americans."
sidebar: mydoc_sidebar
permalink: escumes_de_poliureta.html
---

## Producció 

Es forma bàsicament per la reacció química de dos compostos, un [polialcohol](https://ca.wikipedia.org/wiki/Polialcohol) i un [isocianat](https://ca.wikipedia.org/wiki/Isocianat), encara que la seva formulació necessita i admet múltiples variants i additius. Aquesta reacció allibera diòxid de carboni, gas que va formant les bombolles.

## Tipus

Bàsicament, i segons el sistema de fabricació, es poden dividir els tipus d'escumes de poliuretà en dos tipus:

### Escumes en calent

Són les escumes que alliberen calor durant la seva reacció, fabricades en peces de grans dimensions, destinades a ser tallades posteriorment. Es fabriquen en un procés continu, mitjançant un dispositiu anomenat escumadora, que bàsicament és la unió de diverses màquines, de les quals la primera és un mesclador, que aporta i barreja els diferents compostos de la barreja, la segona és un sistema de cintes sense fi, que arrossega l'escuma durant el seu creixement, limitant el seu creixement per donar-li al bloc la forma desitjada, i la part final de l'escumadora és un dispositiu de tall, per tallar el bloc a la longitud desitjada. Generalment són les més barates, les més utilitzades i conegudes pel públic.

### Escumes en fred

Són aquelles que tot just alliberen calor a la reacció, s'utilitzen per a crear peces a partir de motlles, com farcits d'altres articles, com aïllants, etc. Es fabriquen mitjançant una escumadora senzilla que consisteix en un dispositiu mesclador. Normalment solen ser de més qualitat i durada que les escumes en calent, encara que el seu cost és bastant més gran.


[+Wiki](https://ca.wikipedia.org/wiki/Escuma_de_poliuretà)