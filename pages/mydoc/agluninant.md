---
title: "Aglutinant"
tags: [materials de restauracio, adhesiu, fixador]
summary: "L'aglutinant és una substància líquida mes o menys viscosa que serveix per unir partícules sòlides actuant com
adhesiu i fixat-les a un suport."
sidebar: mydoc_sidebar
permalink: aglutinant.html
---

## Característiques
Adhesius i aglutinants s'endureixen, s'assequen i uneixen i fixen les partícules.

## Tipus 
Aquós o gras depenent de la tècnica.

## Instruccions 

A vegades no es pot diferenciar un aglutinant d'un adhesiu. La diferència seria l'us. Un aglutinant pot actuar
d'adhesiu. 

## Envelliment

- Envelleixen per les condicions de l'entorn i el pas del temps.
- Perden les seves propietats adhesives i presenten problemes de conservació.
- Els (pigments)[/pigment.html] o partícules sòlides és poden alliberar en forma de polsim perdent així l'adherència.


