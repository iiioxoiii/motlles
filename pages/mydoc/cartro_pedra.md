---
title: "Catró Pedra"
tags: [ tirar de cartró, cabut, cotó, jute, cartapesta, carrossers, capgros, cabut, engrut, titella ]
summary: "El catró pedra és una material que es feia servir pel modelat de falles. La última fàbrica  que estava a Bunyol, va tancar al 2016. Estava fet amb pasta de paper reciclat amb cotó, filat de jute. El procés d’assecat era diferent al del cartró. El cartró es tirava amb engrut."
sidebar: mydoc_sidebar
permalink: cartro_pedra.html
---
 
La tècnica de fer servir una pasta de cartrò s'ha fet servir històricament per la fabricació de titelles, cabuts o tot tipus d'artesania i antigament es feia servir molt per fer joguines, sobretot nines i cavallets, o màscares de carnaval. [+ref](https://www.lasprovincias.es/fallas-valencia/nueva-oportunidad-ninots-20190618010444-ntvo.html)

La pasta de paper en el modelat també es coneix com **cartapesta** o **paper maixé** (papier mâché). És podria dir que el cartró pedra es diferencia per l'afegit a sobre de l'estructura de paper d'una capa suport amb càrrega de guix.

[fabricació cartapesta familia Baldani](https://www.youtube.com/watch?time_continue=244&v=JzN8y9HTUPE)  

 
