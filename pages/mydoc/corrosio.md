---
title: "Corrosió"
tags: [riscos laborals, etiquetat]
summary: ""
sidebar: riscos_laborals_sidebar
permalink: corrosio.html
---





    - title: Perill d'Explosió
      url: /perill_explosio.html
      output: web, pdf

    - title: Inflamable
      url: /inflamable.html
      output: web, pdf

    - title: Carburant
      url: /carburant.html
      output: web, pdf

    - title: Gasos Comprimits
      url: /gasos_comprimits.html
      output: web, pdf

    - title: Corrosió
      url: /corrosio.html
      output: web, pdf

    - title: Toxicitat
      url: /toxicitat.html
      output: web, pdf

    - title: Químic Nociu
      url: /quimic_nociu.html
      output: web, pdf

    - title: Contaminant
      url: /contaminant.html
      output: web, pdf

    - title: Perill per la Salud
      url: /perill_per_la_salud.html
      output: web, pdf