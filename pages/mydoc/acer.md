---
title: "Acer"
tags: []
summary: "L'acer és l'aliatge amb poca proporció de carboni"
sidebar: mydoc_sidebar
permalink: ferro.html
---

L'acer és un dels dos materials més importants del grup de «materials férrics». Es caracteritza per què la seva composició conté **entre 0,1 i el 2,11% de carboni(C)** 

## Obtenció

Es podria determinar que l'acer és comença a fabricar a partir del ferro de primera fusió. L'Arabi conté moltes impureses com el sofre, el fòsfor i altres elements que
el fan trencadadís. El procediment es coneix com «afinat» i és un procés de descarbunització i eliminació d'impureses i que dona lloc a acer líquid amb les característiques
perseguides


### Forn d'arc electric
https://www.youtube.com/watch?v=0bKbBJUR7hk

### 

## 

## Ferro Colat o Foses

És coneix com **ferro colat** o **ferro de fosa** aquella aliatge de ferro amb més d'un 5% de carboni.
És l'aliatge fèrric entre el 2,11% de carboni i fins un 6,67%.

- És el material més fàcil d'obtenir donades les elevades quantitats d'impureses que conté.
- Es coneix també com «ferro de segona fosa» per un dels processos que segueix en la seva fabricació (es fon 2 vegades).
- Es pot trobar en la majoria d'estructures antigues.

## Propietats

- Les foses són dures, trencadísses, poc maleables, i poc soldables perquè el material canvia d'estat sense fer-se tou.
- Assequible de fabricar per fondre's a temperatures més baixes i permetre figures més grosses i més complicades.
- Falta d'uniformitat i errors de modelat, però molt útil per fer peces sense mecanitzat posterior malgrat sigui apte per mecanitzar.
- En el mecanitzat les virutes es trenquen fàcilment (són de mida petita) i les eines duren més.
- Pot contenir més elements en l'aliatge per augmentar la duresa, millorar la resistència a la corrossió, etc.
- Es fa servir per radiadors, tubs a pressió, tarquetes, mobiliari urbà (papereres, fanals, bancs, etc).
- Es fan servir per les bancades de les màquines.

### Propietats de les Foses respecte l'Acer

- Les foses són més resistents al desgast.
- Les foses absorveixen millor les vibracions.
- Les foses tenen millors cualitats autolubricants.
- Les foses tenen més resistència a l'oxidació.
- És molt més barat perquè la fosa de ferro 

## Elaboració 

Si es fa un estudi en la seva elaboració poden distingir cinc grups

### Fosa de primera fusió (cast.arrabio) amb llingots de primera fusió

És el metall que surt de l'alt forn. En estat líquid en castellà s'anomena arrabio. El lingot obtingut es fa servir per obtenir acer en fundicions posteriors.

### Fosa de segona fusió

S'obté de fondre els lingots de la primera fusió en un altre forn (ex. cast. tipos cubilote) amb materia per eliminar impureses i obtenir altres característiques del metall. El metall
fos es fa servir en motlles per colada.

### Fosa mal·leable

- S'obté a partir de «foses blanques» amb tractaments de mal·leabilitat.
- El metall se sotmet a procesos de descarburització. S'obtenen foses de menor duresa
i fragilitat i major tenacitat, mal·leabilitat i allargament, excel·lents característiques de moldeabilitat (peces fetes amb motlles), resistència a la corrossió per
certes aplicacions i uniformitat, perquè totes són tractades térmicament.
- S'obtenen peces amb motlles.

Si es fa un estudi per composició podem distingir tres tipus:

### Fosa gris

Es caracteritza perquè **la major part del carboni** que conté es troba en forma de llàmines fines de grafit, degut al seu alt component amb grafit.
Tant el fòsfor (P) com el silici (Si) són elements que provoquen l'acumilació de carboni (grafiticants). Entre el 0.5% i el 3.5% pel Si, i entre el 0.8% i el 1.5%, pel P.
- La típica, barata i fràgil

El nom li ve pel color gris que presenten les rotures granulades de le peces conformades amb aquest sistema.

[wiki: Fosa gris](https://ca.wikipedia.org/wiki/Fosa_grisa)

### Fosa blanca

- Llimes i eines

### Fosa dúctil o nodular

- S'obté a partir de «fosa gris»
- És una segona fos afegint magnesi amb el que s'aconsegueix bona fluidesa i mal·leabilitat, més d'alta resistència, més tenacitat, millor conformació en calent i
trempabilitat.
- Les peces s'obtenen amb motlle. 




## Acer

L'Acer és l'aliatge de ferro i carboni amb una quantitat de carboni que va del 0,05 al 1,7%. Mecànicament, l'acer és un material siderúrgic que te una tensió de trencament mínima de 4500kg/cm2. L'acer és un metall blanc platejat, la densitat del qual es de 7700 kg/m3 i de temperatura de fusió entre 1450 i 1500 graus celsius. La resistència d'acers d'alta resistència pot assolir les 16T/cm2.
(veure entrada «acer» en apartat «aliatges»)

### Classificació per composició química

#### Acers al carboni

Els que tenen poques impureses. Hi ha el **extrasuau** amb menys del 0,2% de C, el **suau** entre el 0,3 i el 0,4 de C, el **semidur** entre el 0,4 i el 0,5%, el **dur** entre el 0,5% i el 0,6%, i el **extradur** amb un contingut superior al 0,6% de carboni.

{% include note.html content="Els acers suaus i extrasuaus es poden soldar i forjar fàcilment. Els suaus són els que es fan servir per fabricar els perfils i elements estructurals. Els semidurs, durs i extradurs necessiten tècniques especials de soldadura. Els acers extradurs es fan servir per fabricar eines i poden portar molibden o varadi per augmentar la duresa i resistència." %}

#### Acers d'aliatge

A més de ferro i carboni contenen altres elements químics que modifiquen les seves propietats, aportant una millora en les seves propietats de resistència i duresa. Es coneixen també com **acers especials**. Entre ells es poden destacar:

##### Acers amb aliatge amb crom i níquel, que contenen una major oxidabilitat i resistència a tracció i són químicament molt estables:

- Acer inoxidable al crom-níquel (0,07% de carboni, el 18% de crom, i entre el 8 i el 10% de níquel).
- Acer inoxidable al crom (0,07 de carboni i entre el 15 i 17% de crom)

##### Acers aliats amb manganés o varadi 

Amb els quals s'augmenta la seva duresa.

##### Acers aliats amb plom

Amb els quals es millora el modelat i docilitat.


### Classificació segons elaboració

#### Acer forjat

Es tracta d'aquell tipus d'acer que es moldeja en estat sólid i amb l'ajud d'esforços mecànics. En el procés d'elaboració dels acers hi ha el **desbastat** escalfant el material i sometent-lo a un esforç de compressió contínua i el **perfilat** mitjançant el premsat amb premses hidràuliques i el laminat per moldeig i tracció. El lingot passa per rodets on es aixafat i allargat. Amb els rodets també es dona la forma requerida en els **trens de laminació**. 

També hi ha la conformació per un procés **d'extrusió** on la boquilla dona forma al material (el sistema de fabricació dels tubs) o el **trefilat** en el qual l'acer es fa passar per una boquilla, estirant-lo i amb el qual sovint és el sistema d'obtenció de **filferro**.

#### Acers de fosa o colada. 

És aquell que s'obté per la solidificació en motlles. Hi ha l'inconvenient de presentar defectes de parts massises, porus, etc. Es poden solucionar aquests inconvenients en motlles a pressió.

## Ferros Universals

Es distribueixen normalment en mides de 6 metres però hi han disponibles en 8, 10, 12, 14, 15, 16 i 18 metres.

### Barretes circulars

De diferents tipus d'acers es poden trobar de 4, 5, 6, 8, 10, 12, 14 ,16, 20, 25, 32, 40 i 50 mm. Si son més primes de 5 mms'anomenen **filferro**. Si són de més de 5 mm és poden anomenar **barres**, **barrots** o **barrots rodons**.

Les barres que es fan servir en construcció amb el formigó tenen relleu i s'anomenen **coarrugats**.

### Seccions

S'anomenen **pletines** o **passamans** les que tenen el costat llarg entre 4 i 10 mm. Més petites de 4 mm s'anomenen **fleixos** i més grans de 10 i fins 50 s'anomenen **llandes**.
S'anomenen **quadrats** els de secció quadrada, **rodons** els de secció rodona, etc.

### Perfils

Es tracta dels perfils usats en estructures. Hi han els IPE, IPN, HEB, UPN. També es poden trobar en forma de «U» o en forma de «T»

### Passamans

Les pletines que 3 a 200 mm de secció.

### Angles

Angles de costats iguals i diferents:

### Xapes

D'amplada superior a 400 mm. Es parla de **xapa blanca** aquella que s'ha tractat amb algun mètode de protecció per l'oxidació o la **xapa negra** la que està desprovista de protecció a l'oxidació.

El **revestiment** de la **xapa blanca** pot ser amb estany o zinc.

Es poden trobar **xapes llises** però també perforades, embutides, estriades (amb rombos o requadres), alveolades, ondulades, etc.

Poden tenir espessors dels 0,6 mm als 20 mm. (mecànica).

### Tubs

De secció quadrada, rodona, rectangulars i amb formes complexes.

### Coarrugats

Són els ferros de construcció pel forjat del formigó. Es tracta barrots de secció rodona amb uns gravats per millorar l'adherencia al formigó.
Hi ha malla i armadura electrosoldada també formada pel mateix típus d'acer coarrugat.

## Ferros d'unió

### Reblons 

Són elements rodons d'acer, provistos d'un cos o canya i un cap. S'uneixen als perfils, indroduïnt-los per orificis coincidents, transformant a continuació l'extrem sortint de la canya en un segon cap, mitjançant forja calenta. Es tracta d'una solució que no es fa servir avui en dia, però es pot trobar en les grans construccións metàl·liques de finals del segle XIX i començaments del XX.

També s'anomenen **reblons** als que es fan servir amb una màquina de reblar d'alumnini i que són molt més habituals en la unió de metalls.

### Passadors i cargols

Són elements metàl·lics d'unió en els que els cos o canya disposa de rosca helicoidal.

### Claus

Són peces d'unió que es fan servir per percusió en materials més tous.

### Cables

Construïts per una corda parcial, o totalment metàl·lica, feta per torsió mútua entre dos o més cordons de filferro. La torsió es realitza sobre una ànima central que pot ser metàl·lica o d'un altre material.

### Cadenes

Peces d'unió composades per peces metàl·liques independents anomendades **baules, bagues o anelles**.

## Minerals ferrosos

### Òxids

La magnetita (amb un 72% de ferro), oligisto (70%), hematities (70%), hematities parda (60%), limonita (60%)

### Sulfurs

Pirita (47%)

### Carbonats

Siderita (48%), esferoidita (48%)

### Impureses

Les impureses que s'eliminen (redueixen) als alts forns són:

- Alúmina
- Òxid de Manganés
- Sosa
- Potassa

Les que s'eliminen parcialment i per tant influeixen en l'aliatge són:

- Compostos del manganés i la potassa
- Silicats
- Sulfurs

Les que es dissolen en part i li donen fragilitat:

- Fòsfor
- Sofre.

## Soldadura

## Compostos


