---
title: "Pergamí"
tags: [suport cal·ligràfic]
summary: "És un suport orgànic i prové de pells d'animals domèstics. El pergamí va substituïr al papir i el seu us es situa des de el V a.C. Fou el principal material sobre el que s'elaboraven documents a l'Europa Occidental fins a l'arribada i popularització del paper. 
"
sidebar: mydoc_sidebar
permalink: pergami.html
---

## Història

Segons Pili a la Història Natural (xiii.21), es pensava que l'inventor del pergamí fou Eumenes II que és del II a.C. Fill de Pèrgam. Segons la llegenda Eumenes volia fundar una biblioteca per fer la competència a la biblioteca d'Alexandria. Llavors necessitava molt papirs però Plotomeu II d'Egipte tenia el monopoli dels papirs i no li va voler enviar. D'aquí sembla que es va originar el pergamí. S'han descobert pergamins més antics al nord de Bagdat documentats al s. III a.C.. 

El pergamí va substituïr al papir i el seu us es situa des de el V a.C. Fins al s. V que fou desplaçat pel paper si bé van ser compatibles encara durant molts anys. Finalment va acabar sent relegat als relligats i per les cobertes. Els primers pergamins encara s'enrotllaven en forma de volum i després passar a la forma de còdex. Abans del segle XIII els pergamins es produïen als monastirs  on els monjos tenien granges. Fins al segle XVIII encara es feien servir per fer traces. 

## Origen

L'origen del seu nom és la ciutat de Pèrgam, on hi havia una gran producció de qualitat d'aquest material. El material, és d'origen orgànic animal i de constitució protèica a base de [col·lagen](https://ca.wikipedia.org/wiki/Col%C2%B7lagen) i fibres creuades de proteines. S'obté de pells de diferents animals domèstics com la vedella, la cabra, l'ovella, el porc, el cèrvol, el búfal, l'ase i el camell.

## Estructura de la pell

### L'epidermis

- És la part de fora de la pell. 
- Conté els pels i els seus arrels.
- És una capa flexible amb presència de cel·lules vives i mortes. 
- Aquesta part no s'aprofita per fer pergamí. 

### La dermis

És la capa situada a sota de l'epidermis i d'aquesta capa s'obté el pergamí. 

### La hipodermis

És la capa més profunda. És el teixit conjuntiu que protegeix la musculatura. Conté terminacions nervioses i les glàndules cebàcies i sudoríferes.

## Composició

En el manuscrit de la ciutat de Lucca (s.VIII-IX) i Teofil Presbiter expliquen les maneres de fabricar el pergamí.

«La pell es posava en remull 24 hores en aigua. Després es reta fins que l'aigua quedi neta. Després es torna a posar en remull amb aigua i òxid de calci. Després es remena cada dia durant 8 dies. Després s'extreu i se li treu el pel.  Es repeteix l'operació 8 dies més. Finalment es renta i s'esvandeïx fins que l'aigua quedi neta. Es torna a deixar dos dies en aigua neta, després séxtreu de l'aigua i després es tensa en un bastidor i es rebaixa mecànicament amb gabinets o fulles esmolades i es deixa dos dies sense que li toqui el Sol. Després es mulla i es puleix amb polsim de pedra tosca. Es torna a tensar i es deixa assecar. Es talla i es continua polint i el resultat és un suport irregular tant amb gruix i en superfície,»
 
## Caracterítiques 

- És molt resistent a les rotures. 
- No deixa traspassar la tinta és un material molt sensible als canvis de temperatura.
- L'escalfor l'encongeix i la humitat també.
- És menys atacat pels fong que el paper pel seu caràcter no àcid.
- És un material dur i resistent que permet per correccions.
- Es pot plegar, es pot cosir i ocupa menys espai que el papir enrotllat.
- Es pot escriure en qualsevol direcció.
- Es pot escriure també per ambdues cares.
- Sovint era reciclat. El pergamí reciclat s'anomena [palimsest](https://ca.wikipedia.org/wiki/Palimpsest).
 
Aplicacions: Suport d'escriptura, també de pintura. En la pintura de taula s'havia utilitzat folrant la fusta. Després es donaven capes de preparació i es pintava a sobre. També serveix per folrar portes amb efectes decoratius, les pantalles dels llums i també s'hi han pintats retrats i pel caràcter aspre, la tècnica més emprada és el pastel.
