---
title: "Dissolució"
tags: [materials de restauracio, dissolució, dissolvent]
summary: "Les dissolucions són una barreja de substàncies a nivell molecular."
sidebar: mydoc_sidebar
permalink: dissolucio.html
---

Cada substància perd les seves característiques individuals i es crea una nova.

En la formació d'una dissolució es desprèn o s'absorbeix calor (calor de dissolució) com en les reaccions químiques.
Una dissolució pot ser endotèrmica (requereix calor) o exotèrmica (desprèn calor).

ex. aigua + sal. L'aigua fa de dissolvent i la sal és el solut.

## Estat físic de les dissolucions

En les dissolucions s'ha de distingir entre dos constituents: el dissolvent, que en general és el component que intervé en més quantitat, i el solut (o soluts), que són els components que intervenen en menor proporció. Segons l'estat físic en el que es troben les solucions, es classifiquen en sòlides, liquides i gasosa. L'estat físic d'una dissolució és el mateix que té el disolvent, mentre que el solut pot estar en un estat diferent. Les dissolucions més comunes són les dissolucions líquides i, dins d'aquestes, les més importants són les aquoses.

[+ Wikipedia](https://ca.wikipedia.org/wiki/Dissoluci%C3%B3_est%C3%A0ndard)