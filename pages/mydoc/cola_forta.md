---
title: "Cola forta o de fuster"
tags: [ adhesius, cola animal, cola calenta ]
summary: "També s'anomena cola de penques. La cola de fuster s'obté de la ebullició o cocció dels tendrums de la vedella o el bou. Era tradicionalment la cola que feien servir els fusters per encolar fustes mobles marqueteries."
sidebar: mydoc_sidebar
permalink: cola_forta.html
---

## Característiques

Actualment en fusteria ha estat substituïda per les coles blanques que són d'acetat de polivinil. També s'ha fet servir per al re-ompli ment d'escletxes i llacunes de fusta, sobretot els buits que originaven l'extracció dels nusos de la fusta en que la cola s'aglutinava amb l'estopa (un residu del cànem).
 
En restauració també es fa servir en l'arrencament de pintura mural.

També és un ingredient important de la coletta (un adhesiu) i a l'hora de la pasta de farina o gatxa que es un adhesiu per teles (per pegats i folradura de teles malmeses (re-entelats).
 

