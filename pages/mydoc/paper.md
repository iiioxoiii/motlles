---
title: "paper"
tags: [suport cal·ligràfic]
summary: "És un suport orgànic fet de cel·lulosa. És tou, flexible, més o menys prim, en forma de làmina i compost principalment per fibres vegetals entrellaçades i assecades que es pot presentar en forma de full o rotllo continuu i que té diferents usos. Inclou aglutinants en forma de coles o gelatines, també compostos minerals sals, pigments, colorants i càrregues."
sidebar: mydoc_sidebar
permalink: paper.html
---

## Història

El paper té el seu origen amb la grega papirus però el material té el seu origen a la Xina. També és el cas d'una llegenda al segle II d.C. i que diu que TSai-Lun ministre d'agricultura va decidir substituir la seda natural per les fibre vegetals que fins llavors s'empraven com a suport pel paper. Seria amb l'expansió àrab s.VIII-IX que el paper va arribar fins a la Índia. Més tard va venir a la mediterrània i la seva arribada a Europa no seria fins el segle XI-XII a través del nord d'Itàlia i pel Sud a través d'Espanya. A Xàtiva és te constància del primer molí paperer al 1115-1153. 

Al princípi, el paper va ser considetat un suport menor i fet servir per documents inferiors. Finalment va substituir el pergamí degut al seu baix cost, si bé van conviure paper i pergamí molts anys.

Amb la revolució industrial es va consolidar com el suport mes emprat del món. El paper acolorit apareix al segle XV. Abans s'acoloria a mà. 

{% include note.html content="Recordar que el paper reciclat s'ha de fer servir per a usos molt efímers perquè es degrada i porta moltes impureses." %}
 
## Paper fet a mà

També s'anomena paper antic. Està fet amb tècniques artesanals i tradicionals i encara es fabrica per a usos artístics. 

### Composició 

- Cel·lulosa de lli, cotó i cànem en forma de teixit.

- S'obté de draps de lli cotó i cannam que subministrava el drapaire. El drapaire comprava draps i roba vella o els intercanviava, Després els venia als fabricants seleccionats per qualitats. El preu del paper, doncs, depenia del preu oscil·lant dels draps. Era un procés de reciclat..

### Fabricació

Hi han diferents maneres de fabricar paper antic. 

#### Recepta Doris

Els draps eren tallats en tires d'una amplada de de 5 o 6 cm. Després s'espolsaven damunt una reixeta amb la fí de treure la pols i cosos estranys, s'eliminen les costures i restes de fil afegits. Després aqueste tires es clasifiques per gruixos i colors, es renten i trituren amb aigua a la fi d'obtenir una pasta. Després es blanqueja amb cendres o llegiu, després s'esbandeix amb aigua neta al molí paperer situat normalment al costat d'un riu. Aquesta pasta es deixava fermentar fins pràcticament  la seva putrefacció durant un més. Es feia dins un recipient que s'anomena tina. Després es torna a moldre a la fi d'obtenir filaments llargs mitjançant un pentinat amb claus de ferro a l´hora que continua passant per aigua fins que quedava en suspensió. Després aquesta pasta es distribuïa en la forma. La forma és una reixeta amb fils de coure tancada amb un marc. Els fills podien ser de diferents tipus.El gruix del marc és el que donava el gruix del paper, i aquesta “forma” aprofitava el fabricant per posar la filigrana o marca d'aigua i que apareix per primera vegada al segle XIII a Itàlia. Aquest paper situat a la forma s'amuntegava o apilonava després d'escórrer l'aigua. Quant ja era sec, s'encolava. 

{% include note.html content="El paper fet a mà no porta cap aglutinant i era després quant s'encolava. Les coles eren coles de peix, cola de conill, o midó de blat o d'arros." %}. 

El paper sense encolar és el **paper assecant**. 

Finalment amb l'arribada de la impremta va augment la demanda de paper. Evidentment va augmentar la demanda de draps que ja eren insuficients i va fer que fos la principal matèria prima fos la fusta.

{% include important.html content="El paper fet a mà és més ressistent i engrogueix menys que el paper mecànic." %}


### Característiques

- Té forma irregular tant el el gruix com zones perimetals o vores del paper.
- L'encolat es feia al final de la fabricació en forma de fulls.
- Té un alt contingut amb cel·lulosa pura i no té tendència a l'acidesa.
- És lleuger, permet l'escriptura per ambdues cares, permet rectificar i accepta totes les tècniques, és elàstic, també atacat per micro organismes, evidentment és més feble que el pergamí.

### Aplicacions

- És el suport més versàtil per a totes les tècniques artístiques. 
- Avui en dia és car i es fabrica sobretot per a suport d'obres d'art.
 
## Paper mecànic o modern

### Història 

Apareix a principis del segle XVIII. Lluis Robert, obrer d'una fàbrica de paper a prop de París va presentar la patent de la màquina de fabricar paper al 1798. La màquina de fabricar paper dona lloc al rotllo continuu. A partir del segle XIX va augmentar encara més la demanda de paper amb la gran difusió de diaris i enciclopèdies producte de la revolució industrial.

### Composició

Es d'origen orgànic vegetal però s'obté de la cel·lulosa de la fusta dels arbres. Aquest arbres són majorment coníferes però també dels eucaliptus, els xops o pollancres. Són arbres de creixement ràpid. Abans d'arribar a trobar la cel.lulosa idònia s'havia provat de fer de canya, de iute, de cànnam, d'arrels, de fulles, d'altres materials re-aprofitats (les xarxes de pesca, les cordes, les lones dels vaixells).
 
### Característiques

El paper mecànic conté l'aglutinant a la pròpia pasta, al contrari del fet a mà que es encolat un cop sec. Aquest aglutinant era sobretot la [colofònia](https://es.wikipedia.org/wiki/Colofonia). El resultat és un paper que amb el temps engrogueix molt degut a l'acidesa que provoca la colofònia. Li afecta negativament la humitat, l'oxigen, la llum que porta a l'oxidació del paper (comporta el trencament de la mol·lecula de la cel·lulosa amb petites cadenes formant-se oxi-cel·lulosa en detriment del paper que es torna trencadís. Es fabricava en forma de rotllo que després es tallava. Actualment els aglutinants són disperssions de resines sintètiques. El paper mecànic també pot portar càrreges com el [caolí](https://ca.wikipedia.org/wiki/Caolinita), el [banc titani](https://ca.wikipedia.org/wiki/%C3%92xid_de_titani(IV)), el [blanc de bari](https://ca.wikipedia.org/wiki/Baritina), o guix a la fi d'aportar diferent característiques al paper. Les càrreges poden donar opacitat al paper, fer-lo impermeable a les tintes d'impressió o satinar-lo. Avui hi han fàbriques que fabriquen 1km de paper per minut. 

{% include note.html content="Hi ha paper mecànic de molt bona qualitat fins a molt dolenta." %}

Existeix el paper i blocs de pH neutre, hi ha paper de reserva alcalina per combatre l'acidesa d'un document.
 
