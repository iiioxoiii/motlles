---
title: "Resines viníliques"
tags: [materials termoplàstics, polímers, resines, termoplàstics]
summary: "Són polímers sintètics i es fan servir en les pintures plàstiques i els adhesius com la cola blanca."
sidebar: mydoc_sidebar
permalink: resines_viniliques.html
---

S'obtenen a partir de diversos monòmers amb enllaços dobles com el clorur de vinil, acetat de vinil, etc

{% include note.html content="Per definició, les resines viníliques es caracteritzen per la presencia del grup insaturat vinil en la seva composició; per tant, també estan incloses les resines acríliques i metacríliques , les resines de poliestiré, etc. 

Malgrat això, des d'un punt de vista pràctic, el terme viníliques es circumscriu als homoplímers (ex. clorur de polivinil) i heteropoímers (ex.clorur-acetat de polivinil)." %}


## Acetat de plovinil (PVA)

Es comercialitzen en forma de pols blanca lleugerament groguenca.

### Aplicacions
- Adhesius
- Consolidació de fusta, ceràmica, paper i cartró.
- En la fabricació de pintures, vernissos i adhesius.


És la resina vinílica més utilitzada en formulacions de pintura. S'obté polimeritzant acetat de vinil a causa d'un increment de temperatura a la qual és sotmès l'aceta de vinil, el PVA variarà les seves característiques. A causa de la duresa dels homopolímers PVA, les pintures PVA no tenen la capacitat de formar una película contínua. Conseqüentment es requereix un plastificant com a additiu per tal d'estovar les partícules, juntament amb els altres dispositius descrits anteriorment en l'apartat dedicat als dispersants acrílics. L'addicció de plastificants limita l'estavilitat d'aquest mitjà artístic, presentant-se la tendència a trencar-se i a ser menys permanent durant el pas del temps que no pas les resines acríliques. Cal esmentar, però, que pintures de PVA d'alta qualitat poden tenir resultats excel3lents si es comparen amb pintures acríliques de baixa qualitat.

En art contemporani:
- S'ha fet servir molt com aglutinant de tècniques pictòriques. 
- Per fer capes de preparació per teles rugoses.


#### RAYT
- És un adhesiu estandard en varies versions d'adhesiu
- Per fusta fer massilles amb càrregues
- Hi ha una específica per teixits i paper i cartró que és la cp1313.

#### LINECO

Es una marca amb amplia gama de productes. Hi ha un d'adhesiu de paper, cartró, pergamí. És de pH neutre reversible amb aigua i porta antisèptic incorporat que fa que no es fagi malbé.

#### MOWILITH-MOWITAl

Són sobretot consolidants i adhesius per capes pictòriques i consolidació de fusta. Existeixen en forma de dispersió aquosa i d'altres dissolen en dissolvents orgànics. Requerix una temperatura d'activació de 50C. És soluble en hidrocarburs aromàtics

#### RHODOPAS
Són adhesius i consolidants. Hi han molts.


## Alcohols polivinílics (PVAL) 

Són resines sintètiques solubles amb aigua que s'obtenen a partir de l'acetat de polivinil polimeritzat. Són adhesius poc potents, tòxics i es fan servir en el mon del paper i del tèxtil. Fan pelicules transparents i són estables a la llum. També es fan servir com additius dels acetats de polivinil.

### MOWIOL. 
És soluble en aigua. Es fa servir com a substitut del midó i de la caseina.

### POLYVIOL
És soluble en aigua

### RHODOVIOL
És soluble en aigua. Es fa servir com adhesiu tectil i consolidació de capes pictòriques.

### GELOWAL
Anomenat fa temps Gelvatol. Es fa servir en el resbers de consolidacions pictòriques. Manté força i elasticitat. No es molt resistent a la llum i es torna insoluble al envellir. Molt recomanat el seu us en ambients humits. No produeix variacions cromàtiques.

## Clorur de polivinil (PVC)
Ha estat emprada per la realització de pintures murals en exteriors. Tot i gaudir d'excel·lents propietats quant a resistència davant l'aigua, té poca estabilitat davant la llum i la calor, resultant-ne una película trencadissa, motiu pel qual requereix plastificants com a additius. Actualment és molt poc utilitzat. Aquest tipus de resines (heteropolímers) poden ser modificades amb un tercer component que intervé amb la polimerització i que defineixen les propitats del producte final

## Art i Restauració
Es van descobrir als anys 30 i es fan servir en restauració des dels anys 50.






[+info pintures](http://fabricarpinturas.com/blog/resinas-vinilicas.php)
*Codina Esteve, Rosa.«Procediments Pictòrics,(Textos docents; 189. Text-guia)».edicions Universitat de Barcelona. ISBN 84-8338-189-3*
