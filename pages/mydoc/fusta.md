---
title: "Fusta"
tags: []
summary: "És el suport de moltes obres d'art, tant de pintures, escultures retaules, mobles. És un dels materials mes antics utilitzats per l'home junt amb la pedra. La fusta es considera material noble. Després de ser treballada i trossejada s'aprofiten els residus, fins i tot els mes ínfims, com les serradures i els encenalls per la fabricació de tota mena de productes."
sidebar: mydoc_sidebar
permalink: fusta.html
---

## Caracteristiques

- És higroscòpica, que vol dir que es capaç d'absorbir o expulsar humitat segons les condicions ambientals.
- Pot doblar el seu volum si es submergeix.
- És anisòtropa que vol dir que es dilata en diferents direccions de diferent manera.

{% include note.html content="La fusta conservada en un ambient anaeròbic (sense presència d'oigen ni microorganismes pot mantenir-se intacta durant mil·lers d'anys." %}


## Composició 

- 50% de carboni, 40% oxigen, 6, 1 de nitrogen i un 1% de cendres (sodi, calci, potasi, fosfor i magnesi) .
- 50% de cel·lulosa, 30% de lignina i 20% d'hemil-cel·lulosa, matèries tàniques, colorants, albúmines, resines i greixos.
- Té aigua de constitució: és la part integrant de la matèria llenyosa, aigua de saturació és retinguda per les membranes o parets de la matèria llenyosa i l'aigua lliure és la que ompla les fibres llenyoses i és la que desapareix quant l'arbre es talla.
- La fusta apta per ser treballada hauria de tenir menys d'un 10% d'aigua. 
- Els arbres tallats a l'hivern contenen més aigua que els arbres tallats a la primavera.
- Quant la fusta perd aigua es contrau més en l'albura que en el cor o medul·la originant tensions que bomben, torcen i esquerden la fusta.
- La cel·lulosa es descompon per hidròlisis i es estable en sec

### Lignina

És una substància incrustada a la cel·lulosa i intervé en el procés de transformació de la cel·lulosa. 

- És de color fosc i li dona rigidesa a l'arbre.
- És la substancia que sobretot produeix acidesa al paper.

### Tannins

- Són molt abundants en el mon vegetal i ajuden a l'arbre en la defensa contra insectes i fongs. 
- Actua de biocida. Són estractes colorants de sabor estringent que també es fa servir a la industria tèxtil i en el curtit de pells.

### Albúmines

- Són el midó i el sucre. És la part de la fusta que atreu tots els insectes i els fongs.

### Les Resines i els Greixos

Li donen a la fusta protecció i resistència a la humitat. També li donen brillantor.
 
## Duresa

La fusta que creix lentament es més dura que els arbres que creixen ràpids

Fusta **molt dura**: vanús, el boix i l’alzina
Fusta **dura**: om, cirerer, roure, auró
Fusta **semidura**: la noguera, la perera, el castanyer i el faig
Fusta **tova**: avet, pi, bedoll,
Fusta **molt tova**: pi americà, el xop, el tell, salze
 
{% include note.html content="Quant mes vella és una fusta més gran és la seva duresa." %}
 
## Anatomia

### Escorça

És la part mes exterior. És la capa de protecció dels teixits interns i és la capa que li permet els intercanvis gasosos necessaris per la vida de l'arbre. 
- No permet que traspassi l'aigua i evita l'evaporació de l'aigua pròpia amb la incidència del Sol. 
- El protegeix del fred i la calor.
- Es renova constantment.

### Líber

Conté els vasos que transporten l'aliment sintetitzat per les fulles. Després passa a ser escorça.

### Cambiu

És la capa cel·lular o estrat subtil de teixit responsabel de la producció de fusta nova cap a l'interior i cap a l'exterior. Té la funció del creixement que comporta l'augment de la circumferència del tronc i de les branques durant l període de desenvolupament vegetatiu que en els climes temperats correspon a primavera estiu. El cammium forma els cercles anuals o de creixement que conformaran la veta. La dendrocronologia és la ciència que estudia la metereologia pels anells i canvis meteorològics de temps passats.

### Arbeca

Fusta jove que amb el temps es converteix en duramen.Saturada de sàvia, per la qual cosa contè molta aigua. Té la funció de conduir les sals minerals de les arrels fins les fulles. Es diferencia del duramen pel seu color més clar.

### Duramen

Està format per cèl·lules mortes que sostenen l'estructura de l'arbre i emmagatzemen substancies de reserva. Aquesta part del tronc és la de la fusta que es fa servir per a la construcció i elaboració d'objectes. A mesura que l'arbre creix l'albeca es torna durament. És on es troba més lignina. El duramen és la fusta dura i consistent.

### Medul·la

També se li diu cor de la fusta. Forma part del duramen i és la part central del tronc. Pot ser dura, tova, o esponjosa, segons el tipus d’arbre.
 
## Les imperfeccions de la fusta

### Els nusos

Són els teixits que es formen en la part on les branques s’uneixen al tronc. Disminueixen el valor de la fusta.  Aquesta es fa difícil de treballar amb els nusos. Dónen lloc a esquerdes. Trenquen el dibuix de la beta.

## Fusta corbada

Provè dels arbres en què els troncs no han crescut rectes. Això passa per culpa del vent o proximitat del tronc amb roques. Si creix inclitat la medul·la pot inclinar-se i la fusta obtinguda perd elasticitat i resistència.

 
## Prefabricats de fusta

### Aglomerat

- És l'estratificació mitjançant motlles i premses amb un alt poder de premsat a altes temperatures d'una barreja de coles i resines amb aquests residus.
- El resultat és un producte resistent als canvis atmosfèrics, no són atacats pels insectes sempre que les coles no siguin naturals. 
- Són plafons estables resistents a la flexió i es contrauen o dilaten molt poc.

### Contraxapat

- Són taulons compostes per fines fulloles de fusta obtingudes per l'asserrament amb fulles especials que encolades i sobrepujades formen taulers. Estan formats per 3, 5 o 7 peces amb un gruix que va de 5 mm a 50 mm. 
- El contraxapat és flexible i elàstic i lleuger. Per fer-lo s'empren fustes no gaire dures com el bedoll, del faig, i xop.

### Tablex

És un tauló de superfície llisa i pulimentada que pot imitar òpticament la fusta. S'obté dels encenalls de la fusta amb fibres de palla aglutinades amb resines sintètiques.
- El Tablex no s'estella, no es podreix, és flexible i resisteix els atacs biològics.
- És resistent a l'alcohol i als àcids dèbils.
 