---
title: "Laca Pigment"
tags: [materials de restauracio, laca, color, pigment]
summary: "És un pigment de segona categoria. És un pigment que té el seu origen en un colorant."
sidebar: mydoc_sidebar
permalink: laca_pigment.html
---

## Obtenció

- S'obté de la base d'un colorant en un procés de lacat. 

- Una laca és un pigment fet precipitant o fixant un colorant sobre una base inert, quan el pigment pur no té cos o té massa potència de tinció (normalment pigments orgànics).

Els succedanis poden ser dissolts d'alguna manera.

El procés consisteix en dotar de cos propi al colorant amb un suport anomenat substrat d'origen inorgànic que sovint és hidròxid d'alumini o hidrat xxxxx que són càrregues incolores i per precipitació agafen el color del colorant quedant insolubles.

Els pigments que no són permanents són anomenats **fugitius**. 

