---
title: "Balsams"
tags: [balsams]
summary: "Antigament tenia aplicacions en la medicina per guarir ferides."
sidebar: mydoc_sidebar
permalink: balsams.html
---

Els bàlsams adopten el nom del lloc geogràfic d'origen. 

Químicament es composen de resines (reines) àcids aromàtics, alcohols i èsters. Els bàlsams són exsudacions que es produeixen a les coníferes quant es produeix un tall o incisió. 

Tenen aspecte viscós, com una tèrbola i contenen una mascla de resines i olis essencials.

## Caracterítiques

- Tenen olor aromàtica
- Són insolubles en aigua.
Subproductes
- les resines (sòlides). Obtingudes per evaporació
- els olis (líquits). Obtinguts per destil·lació.
Aplicacions
- Com additiu de la pintura a l'oli per donar més brillantor i alentir l'assecatge.
- Per donar relleu a la porcellana.
 
## Bàlsam de Copaiba

S'obté de l'exsudació de diferents coníferes que creixen a l'Àfrica, l'Índia i sud Amèrica. Conté un 40% d'oli essencial i un 60% de resina. Va arribar a Europa al segle XVIII per a us medicinal i no va ser fins al s.XIX que es va fer servir per tècniques pictòriques.
- A la llarga enfosqueix la pintura.
- Accelera l'assecatge. !!!!
- En restauració s'ha fet servir en entelats en petites proporcions.
 
## Bàlsam de Trementina de Venècia

S'obté de l'arbre larix decidua. És un tipus d'alerç que es troba al centre d'Europa. El nom li ve del port de Venècia com a distribuïdor  de mercaderies. S'ha fet servir del s. XVII al XVIII.
- Serveix per donar volum a les pinzellades i elasticitat a la capa pictòrica.
- S'han fet servir per fer vernissos.
 
## Bàlsam de Canadà
S'obté de l'arbre pirrus balsamea i de l' albies balsamea. Són els avets del Canadà. Aquest arbres provenen del Canadà i Nord Amèrica.
- És el més transparent del bàlsams.
- Allarga l'assecatge de la pintura a l'oli i dona fluïdesa al traç.
- Pot actuar d'adhesiu.

## Bàlsam de trementina d'Estrasburg

S'obté de l'arbre l'albies alba. És un arbre que creix al centre i sud d'Europa. Conté el 80% d'àcids resinosos i resines, i un 20% d'àcids essencials.
- Històricament s'ha fet servir com a dissolvent per resines
- Per la fabricació de vernissos greixosos.
