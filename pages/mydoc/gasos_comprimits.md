---
title: "Gasos Comprimits"
tags: [riscos laborals, etiquetat]
summary: "Els gasos a pressió són gasos que es troben en un recipient a una pressió de 200kPa (indicador) o superior, o que estan liquats o bé liqüats i refrigerats."
sidebar: riscos_laborals_sidebar
permalink: gasos_comprimits.html
---

{% include precaution.html content="Perill d'explossió per escalfament i cremades o lessions criogèniques" %}

![Pictograma Gas Comprimit](https://www.asvstubbe.it/wp-content/uploads/2017/04/GHS-Gas-compressi.jpg)

Tenen associada la paraula d’advertència: **Atención** i poden tenir dues indicacions de perill diferent **(H280 o H281)**, en funció de si es tracta d’un gas comprimit, liqüat, liqüat-refrigerat o dissolt.

Gasos normalment segurs poden tornar-se perillosos si estan pressuritzats.


| Gas | Frase |
| --- | --- |
| Comprimit | H280 «Contiene gas a presión; peligro deexplosión en caso de calentamiento.» |
| Liqüat | H280 «Contiene gas a presión; peligro deexplosión en caso de calentamiento.» |
| Dissolt | H280 «Contiene gas a presión; peligro deexplosión en caso de calentamiento.» |
| Liqüat refrigerat | H281:Contiene gas refrigerado; puede provocarquemaduras o lesiones criogénicas.|


{% include note.html content="Protegir les bombones de la llum del sol i fer servir EPIS adequats." %}


## Clasificación de gasos 
(segons el ADR 2009)

### Gas comprimit
Gas que quan s'embala a pressió pel seu transport, és totalment gasos a a -50 ºC; en aquesta categoria hi han tot els gasos que tinguin una temperatura crítica menor o igual a -50 ºC

### Gas liqüat
Gas que quan s'embala a pressió pel seu transport és parcialment líquid a temperatures superiors a -50 ºC.

### Gas dissolt
Gas que quan s'embala a pressió pel seu transport es troba dissolt en un dissolvent en fase líquida.

### Gas liquat refrigerat
Gas que quan s'embala a pressió pel seu transport és parcialment líquid a causa de la seva baixa temperatura

## Napo i els Gasos Comprimits

<div class="row">
<iframe width="660" height="415" src="https://www.youtube-nocookie.com/embed/GWQUI_cjO6A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>


