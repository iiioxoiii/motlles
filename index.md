---
tags: []
toc: false
sidebar: index_sidebar
permalink: index.html
summary: false
---

<div class="home">
    <div class="post-list">
        {% for post in site.posts limit:10 %}
    <h1 class="post-title">{{ post.title }}</h1>
    <!-- <h1><a class="post-link" href="{{ post.url | remove: "/" }}">{{ post.title }}</a></h1> -->
        <span class="post-meta">{{ post.date | date: "%b %-d, %Y" }} /
            {% for tag in post.tags %}
                <a href="{{ "tag_" | append: tag | append: ".html"}}">{{tag}}</a>{% unless forloop.last %}, {% endunless%}
                {% endfor %}</span>
        <!-- <p>{% if page.summary %} {{ page.summary | strip_html | strip_newlines | truncate: 160 }} {% else %} {{ post.content | truncatewords: 50 | strip_html }} {% endif %}</p> -->
        <div>
            {{ post.content }}
        </div>

        <hr>
        {% endfor %}

        

        <p><a href="feed.xml" class="btn btn-primary navbar-btn cursorNorm" role="button">RSS Subscripció{{tag}}</a></p>

<hr/>
        <p>Mira posts anteriors a  <a href="news_archive.html">Arxiu de Notícies</a>. </p>

    </div>
</div>