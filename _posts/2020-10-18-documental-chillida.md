---
title: "Chillida: El arte y los sueños"
published: true
sidebar: mydoc_sidebar
layout: post
permalink: mydoc_pages.html
summary: false
tags: [Chillida, documental, escultor, ferro]
---


El film mostra la figura de l'escultor Eduardo Chillida des d'una perspectiva familiar i en el moment de construcció del museu Chillida Leco. Es poden escoltar testimonis, reflexions del propi escultor i veure detalls durant la producció d'algunes de les seves espectaculars formes en ferro en els alts forns.

**Eduardo Chillida** va nèixer al 1924 a Donosti. La seva dedicació a l'escultura es va propiciar després d'una lessió en un partit de futbol defenent la porteria de la Reial Societat. La seva formació inicial en arquitectura no va completar-se per passar-se al dibuix. Finalment va decidir-se per l'escultura i després de formar-se en aquesta disciplina va mudar-se als anys 50 a París on va coincidir amb altres artistes com **Picasso** o [Eusebio Sempere](https://ca.wikipedia.org/wiki/Eusebio_Sempere_Juan) 


<div>
<div style="padding:73.17% 0 0 0;position:relative;">
	<iframe src="https://player.vimeo.com/video/155003925" style="position:absolute;top:0;left:0;width:100%;height:100%;" frameborder="0" allow="autoplay; fullscreen" allowfullscreen>
	</iframe>
</div>
<script src="https://player.vimeo.com/api/player.js"></script>
<p><a href="https://vimeo.com/155003925">CHILLIDA: EL ARTE Y LOS SUEÑOS</a> from <a href="https://vimeo.com/user48469984">susana chillida</a> on <a href="https://vimeo.com">Vimeo</a>.</p>
</div>


Fitxa técnica
Any : 1998
Format : Betacam digital, Betacam SP y DVD
Duració: 54 min.
Llengua: espanyol
Subtítols: inglés, francés, italiano, euskera.
Direcció i producció : Susana Chillida.
Música: Alberto Iglesias