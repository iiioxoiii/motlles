---
title: «Nemausus 1» de Jean Nouvel
published: true
sidebar: mydoc_sidebar
layout: post
permalink: mydoc_pages.html
summary: false
tags: []
---

Capítol d'una série en la que s'examina «Nemausus 1», un conjunt de vivendes socials construïdes pel [Jean Nouvel](https://ca.wikipedia.org/wiki/Jean_Nouvel) entre 1985 i 1987. L'objectiu de l'arquitecte era aprofitar l'espai i poder ampliar la superfície útil dels habitatges. Per fer-ho va fer servir ~el mòdul~ i va construir el conjunt el més recte i més simple possible.

Destaquen els elements de metàl·lics com els **tancaments**, els **acabats de la façana**, els **balcons**, les **baranes** i les **escales interiors**.


<iframe width="560" height="315" src="https://www.youtube.com/embed/QDScQfIdM5g" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>