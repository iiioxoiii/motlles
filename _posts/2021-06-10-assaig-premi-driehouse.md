---
title: "Assaig crític: La Forja segons Santiago Martínez Otero"
published: true
sidebar: mydoc_sidebar
summary: false
permalink: assaig_forja.html
tags: []
---


En Santiago Martínez Otero afirma «un emplomat ben fet és garantia d'eternitat». La contundència i claretat de la frase ens pot servir per conformarnos el taranà del protagonisa i abordar
questions relacionades amb la pràctica mil·lenària de la manipulació de l'acer, la restauració, la conservació o els oficis al segle XXI. 

Ens vol dir que l’antic ofici de ferrer-forjador va trobar la pedra filosofal de l’eternitat pels elements metàl·lics? O ens està intentant dir que els mètodes que aleshores s’usaven,
de moment, no tenen res a envejar a tot l'innegable avanç actual respecte a la constitució dels metalls i noves tècniques de tractament d’aquests?

En *Chago*, tal i com tothom el coneix, és un senyor que ha dedicat la seva vida a recuperar el coneixement antic del seu ofici. Actualment ja sap que un emplomat és bàsic per no fer
malbé la fàbrica (la pedra que el sosté). Ha après a través de llargues entrevistes als avis de la zona, com les tècniques dels emboetats amb reblons o xavetes, donen a les estructures la flexibilitat necessària per afrontar
les contraccions i dilatacions dels metalls. O que un empavonat en oli dóna un acabat de molta alta durabilitat sense pràcticament manteniment.

És clar que la ciència i la tecnologia en el darrer segle ha fet uns enormes avenços. La profunditat dels coneixements a nivell molecular de la constitució
dels acers quasi que no es pot comparar: ja sabem l’estructura cristal·lina de l’austenita, la ferrita, la bainita, la martensita,... Sabem i quasi controlem els
mecanismes de la difusió del carboni o altres elements dins de les xarxes per tal de millorar les propietats dels acers. La varietat de nous tractaments tèrmics
és brutal, la cimentació, el galvanitzat, per citar-ne algun dels molts que disposem. Ara sols caldrà que passi algun segle per poder valorar aquests
nous mètodes i comparar-los amb els que en Chago proposa. En tot cas el que aquest senyor ens diu clarament és que les restauracions del patrimoni arquitectònic,
fet a la “vieja usanza” i amb coneixement de causa, tenen uns preus que no poden competir en els pressupostos de les licitacions actuals.
Ens trobem en situacions, per exemple, en que un nou metal·lista pot usar la soldadura, que si bé és una tècnica d’unió dels metalls molt eficaç, interessant i econòmica,
per determinats usos pot no ser la més adient, i a mig-llarg termini pot esguerrar la intervenció restaurativa.

Per acabar també és important contextualitzar els premis Richard H. Driehaus de les Arts de la Construcció iniciats el 2017. Aquests són uns guardons que es concedeixen a Espanya
als mestres més destacats dels diferents oficis de la construcció tradicional; oficis que actualment estan a la vora de l'extinció. La seva creació és doncs una clara contribució
a pal·liar la situació actual de pèrdua d’un coneixement i unes tècniques que permetin preservar el patrimoni històric a llarg termini, a la vegada que un interès per donar continuïtat a
aquests oficis. Darrera d’aquest intent en la conservació del nostre patrimoni hi ha també un evident interès cultural i turístic.Tenint en compte que una de les
bases de l’economia espanyola és el turisme, conservar tots aquests coneixements té sentit i lògica. I tampoc és d’estranyar, per tant, que el Chago en aquest concret audiovisual, realci les seves praxis en contraposició als
materials i mètodes actuals.

El document audiovisual d'agraïment per aquest premi 2021 és doncs un emotiu i clar clam a l'antic ofici de forjador. Al llarg del document ell ens transmet el seu amor i respecte
a les tècniques i materials emprats des de fa segles, així com a tot el seu col·lectiu professional.

**Belén Mercader Prats**
***Alumne de 1r de Forja Artística a La Llotja de Sant Andreu***

<iframe width="560" height="315" src="https://www.youtube.com/embed/7DlWle0UW1o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
