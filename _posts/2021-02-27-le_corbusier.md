---
title: «Convent de la Tourette» de Le Corbusier
published: true
sidebar: mydoc_sidebar
layout: post
permalink: mydoc_pages.html
summary: false
tags: []
---

Documental sobre el Convent de la Tourette de [Le Corbusier](https://ca.wikipedia.org/wiki/Le_Corbusier). El projecte va ser encarregat per l'Ordre Dominicana a finals dels 
anys 50 del segle XX i construit a Lyon. El convent està format per volums de formigó i pensat des del funcionalisme extrem que caracteritza l'arquitecte.

M'agradaria esmentar la solució en els tancaments de vidre d'una de les façanes on hi va intervenir el compositor [IIannis Xenakis](https://ca.wikipedia.org/wiki/Iannis_Xenakis) que en aquells moments treballava com arquitecte i començava
a composar la peça musical [Metastasis](https://www.youtube.com/watch?v=SZazYFchLRI). Xenaquis va presentar diverses propostes basades en la peça musical fent servir 
els intervals musicals i Le Corbusier va sintetitzar la feina anomenant-los «panells ondulatoris» 


<iframe width="560" height="315" src="https://www.youtube.com/embed/HoUj8IktrSY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
