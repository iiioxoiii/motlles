---
title: Buidat de motlles
published: true
sidebar: mydoc_sidebar
layout: post
permalink: mydoc_pages.html
summary: false
tags: []
---

La tècnica dels motlles ha canviat molt des de l'aparició de nous materials durant el segle passat i això permés el desenvolupament de sistemes de de reproducció molt més ràpids i més fàcils. 

Per contra, aquestes millores han sigut quantificades pels fabricants i distribuidors dels nous materials i això ha fet que els preus d'aquests productes siguin molt més elevats que els productes que s'havien fet servir fins aleshores. 

En tots els anys d'experiencia en la docència i com alumne en estudis artístics no he observat gaire interés pel coneixement dels materials més enllà d'aspectes concrets sobrevinguts per l'experiència o la moda del moment. En la tria s'hi expresava el gust per treballar amb aquella matèria, la tècnica, els diners, etc. Però poques vegades ha passat que la tria de l'alumne o el professor es determinava a partir d'aspectes físics o químics de la materia.

L'obra d'art se'ns representa de maneres infinites i sembla que per aquesta raó, s'entri en alguna mena de contradicció en la presa de decisició sobre la tria del material, fins al punt no valorar els perills que suposa fer servir un o altre producte per l'artista, el grau de perdurabilitat de l'obra, o la petjada ecològica en l'obtenció del material.

## Buidat d'un motlle d'escaiola

<iframe width="560" height="315" src="https://www.youtube.com/embed/-N_4HRZMvXI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Buidat d'un motlle de silicona

<iframe width="560" height="315" src="https://www.youtube.com/embed/-ZZZRFkrJ3o" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

{% include links.html %}


