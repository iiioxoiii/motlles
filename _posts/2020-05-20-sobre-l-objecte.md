---
title: "Sobre l'Objecte (I)"
published: true
sidebar: mydoc_sidebar
layout: post
permalink: mydoc_pages.html
summary: false
tags: []
---

Un terme que m'agrada fer servir per assenyalar l'escultura i que és comú a moltes altres coses és definir-la com un objecte. Segons l'etimologia de la paraula, l'objecte es refereix a aquella cosa que podíem desprendre'ns. Es pot pensar en una cosa de poc valor o llancívola. Actualment crec que el sentint no a canviat gaire. L'expressió «mereix ser objecte d'estudi» centra l'atenció en alguna cosa que mereix una atenció especial en relació a la resta.


Una de les persones que va reflexionar sobre els objectes va ser [Roland Barthes](https://es.wikipedia.org/wiki/Roland_Barthes). Aquest semiòleg venia a dir (i ara vaig a simplificar molt) que els objectes els reconeixem de maneres molt complexes. No només són coses que podem percebre amb els nostres sentits, si no que els objectes també els construïm amb significats simbòlics. 

El mercat, la política, la publicitat, el disseny, la moda, la religió, etc. construeixen els seus objectes amb «atributs» simbòlics i culturals. Són molt complexos. També molts artistes han integrat sistemàticament aquesta manera de veure els objectes tot creant objectes deterministes, no gaire diferentss a cualsevol altre. 

Per tant, sobre la construcció de l'objecte, sobretot des de l'àmbit artístic i en aquests moments d'homogeneizació cultural, crec que s'hauria fomentar la dissuasió de significació i permetre que l'objecte dissident tingués la possibilitat d'esdevenir extraordinari.


[«Semántica del objeto». Roland Barthes](https://static1.squarespace.com/static/58d6b5ff86e6c087a92f8f89/t/58e16d7dbe65941eab135566/1491168637136/Barthes%2C+Roland+-+Semantica+del+objeto.pdf) (pdf)


<img src="{{ "images/josep-kosuth.png" }}" alt="Four colors Four Words"/>

Four Colors Four Words (Orange-Violet-Green-Blue). Joseph Kosuth (B. 1945)
